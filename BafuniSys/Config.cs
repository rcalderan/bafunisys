﻿/*
 * Created by SharpDevelop.
 * User: richa
 * Date: 21/06/2017
 * Time: 15:48
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data.SQLite;

namespace BafuniSys
{
	/// <summary>
	/// Description of Backup.
	/// </summary>
	public class Config
	{
		
		public bool BackupOnClose{ get;set;}
		public string BackupPath{ get;set;}
		public bool Conf1{ get;set;}
		public bool Conf2{ get;set;}
		public bool Conf3{ get;set;}
		
		
		public static Config Load(SQLiteConnection con)
		{
			try{
				using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from config", con);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    adp.Fill(dt);
                    Config conf = new Config();
                    if (dt.Rows.Count != 0)
                    {
                        conf.BackupOnClose = (bool)dt.Rows[0]["bkp_on_close"];
                        conf.Conf1 = (bool)dt.Rows[0]["conf1"];
                        conf.Conf2 = (bool)dt.Rows[0]["conf2"];
                        conf.Conf3 = (bool)dt.Rows[0]["conf3"];
                        conf.BackupPath = dt.Rows[0]["bkp_src"].ToString();
                        con.Close();
                        return conf;
                    }
                    else
                    {
                        con.Close();
                        if (Config.Save(false,"C:\\",false,false,false,con))
                        {
                        	adp = new System.Data.SQLite.SQLiteDataAdapter("select * from config", con);
                        	dt = new System.Data.DataTable();
                        	adp.Fill(dt);
                        	if (dt.Rows.Count != 0)
                        	{
                        		conf.BackupOnClose = (bool)dt.Rows[0]["bkp_on_close"];
                        		conf.Conf1 = (bool)dt.Rows[0]["conf1"];
                        		conf.Conf2 = (bool)dt.Rows[0]["conf2"];
                        		conf.Conf3 = (bool)dt.Rows[0]["conf3"];
                        		conf.BackupPath = dt.Rows[0]["bkp_src"].ToString();
                        		con.Close();
                        		return conf;
                        	}
                        }
                        con.Close();
                        return null;
                    }
                }
				
			}catch{if (con.State != System.Data.ConnectionState.Closed)
					con.Close();
				return null;
				
			}
		}
			
		public static bool IniciarBackup( string destino, SQLiteConnection conn){
			try
			{
				string s;
				foreach (string bkpPath in System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory(),"backup*")){
					s= bkpPath;
					System.IO.File.Delete(bkpPath);
				}
				int lastIndexOf = destino.LastIndexOf('\\');
				
				DateTime noww = DateTime.Now;
				string filename = "backup-"+noww.ToString("yyyy-MM-dd-HH-mm-ss");
				using(var source = new System.Data.SQLite.SQLiteConnection(conn.ConnectionString))
					using(var destination = new System.Data.SQLite.SQLiteConnection("Data Source="+filename+"; Version=3;"))
				{
					source.Open();
					destination.Open();
					source.BackupDatabase(destination, "main", "main", -1, null, 0);
					if (lastIndexOf+1-destino.Length!=0){
						System.IO.File.Copy(filename,destino+"\\"+filename);
					}
					else{
						System.IO.File.Copy(filename,destino+filename);
					}
				}
				return true;
			}
			catch{
				return false;
			}
		}
		
		
		
		public static bool Save(bool onclose, string path, bool conf1, bool conf2, bool conf3, SQLiteConnection conn)
		{
			try{
                using (var command = new System.Data.SQLite.SQLiteCommand(conn))
                {
                	conn.Open();
                    SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from config", conn);
                    System.Data.DataTable dt = new System.Data.DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count == 0){
                    	command.CommandText = "INSERT INTO config (bkp_on_close,bkp_src,conf1,conf2,conf3) VALUES (" +
                        "@onclose,@src,@conf1,@conf2,@conf3)";
                    	command.Parameters.AddWithValue("@onclose", onclose);
                    	command.Parameters.AddWithValue("@src", path);
                    	command.Parameters.AddWithValue("@conf1", conf1);
                    	command.Parameters.AddWithValue("@conf2", conf2);
                    	command.Parameters.AddWithValue("@conf3", conf3);
                    	if (0 < command.ExecuteNonQuery())
                    	{
                    		conn.Close();
                    	 	return true;
                    	}
                    	else{
                    		conn.Close();
                    		return false;
                    	}
                    }else
                    {
                    	command.CommandText = "UPDATE config SET bkp_on_close=@onclose, bkp_src=@src, conf1=@conf1,conf2=@conf2,conf3=@conf3";                      
                    	command.Parameters.AddWithValue("@onclose", onclose);
                    	command.Parameters.AddWithValue("@src", path);
                    	command.Parameters.AddWithValue("@conf1", conf1);
                    	command.Parameters.AddWithValue("@conf2", conf2);
                    	command.Parameters.AddWithValue("@conf3", conf3);
                    	if (0 < command.ExecuteNonQuery())
                    	{
                    		conn.Close();
                    	 	return true;
                    	}
                    	else{
                    		conn.Close();
                    		return false;
                    	}
                    }
                }
				
				
			}catch{
				if (conn.State != System.Data.ConnectionState.Closed)
					conn.Close();
				return false;
			}
			
		}
	}
}
