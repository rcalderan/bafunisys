﻿namespace BafuniSys
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
        	this.menuStrip1 = new System.Windows.Forms.MenuStrip();
        	this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.efetuarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.orçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.pesquisarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.relatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.pendendênciasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.segurançaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.backupRápidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.configuraçõesDeBackupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.cad1Gb = new System.Windows.Forms.GroupBox();
        	this.cad1AtualizarclienteBt = new System.Windows.Forms.Button();
        	this.cad1ExcluirBt = new System.Windows.Forms.Button();
        	this.cad1TipoCb = new System.Windows.Forms.ComboBox();
        	this.cad1Dg = new System.Windows.Forms.DataGridView();
        	this.cad1Lst = new System.Windows.Forms.ListBox();
        	this.label10 = new System.Windows.Forms.Label();
        	this.label14 = new System.Windows.Forms.Label();
        	this.cad1ObsTb = new System.Windows.Forms.TextBox();
        	this.cad1CpfTb = new System.Windows.Forms.MaskedTextBox();
        	this.cad1CodigoMtb = new System.Windows.Forms.MaskedTextBox();
        	this.label13 = new System.Windows.Forms.Label();
        	this.label12 = new System.Windows.Forms.Label();
        	this.label29 = new System.Windows.Forms.Label();
        	this.cad1CpfCnpjLb = new System.Windows.Forms.Label();
        	this.cad1VaiLb = new System.Windows.Forms.Label();
        	this.cad1VemLb = new System.Windows.Forms.Label();
        	this.cad1IdLb = new System.Windows.Forms.Label();
        	this.label16 = new System.Windows.Forms.Label();
        	this.label15 = new System.Windows.Forms.Label();
        	this.label1 = new System.Windows.Forms.Label();
        	this.cad1Fone2Tb = new System.Windows.Forms.TextBox();
        	this.cad1Fone1Tb = new System.Windows.Forms.TextBox();
        	this.cad1EmailTb = new System.Windows.Forms.TextBox();
        	this.cad1NomeTb = new System.Windows.Forms.TextBox();
        	this.cad1SalvarBt = new System.Windows.Forms.Button();
        	this.cad1LBt = new System.Windows.Forms.Button();
        	this.cad1XBt = new System.Windows.Forms.Button();
        	this.cad1EndPn = new System.Windows.Forms.Panel();
        	this.cad1EndAtualizarBt = new System.Windows.Forms.Button();
        	this.cad1NewEndBt = new System.Windows.Forms.Button();
        	this.cad1TipologCb = new System.Windows.Forms.ComboBox();
        	this.panel6 = new System.Windows.Forms.Panel();
        	this.cad1ExcluirEndBt = new System.Windows.Forms.Button();
        	this.cad1EnderecosCb = new System.Windows.Forms.ComboBox();
        	this.cad1EndLb = new System.Windows.Forms.Label();
        	this.label7 = new System.Windows.Forms.Label();
        	this.label37 = new System.Windows.Forms.Label();
        	this.label8 = new System.Windows.Forms.Label();
        	this.cad1EnderecoTb = new System.Windows.Forms.TextBox();
        	this.cad1NumeroTb = new System.Windows.Forms.TextBox();
        	this.label9 = new System.Windows.Forms.Label();
        	this.label3 = new System.Windows.Forms.Label();
        	this.label2 = new System.Windows.Forms.Label();
        	this.cad1CepMtb = new System.Windows.Forms.MaskedTextBox();
        	this.label6 = new System.Windows.Forms.Label();
        	this.cad1BairroTb = new System.Windows.Forms.TextBox();
        	this.cad1UfTb = new System.Windows.Forms.TextBox();
        	this.label5 = new System.Windows.Forms.Label();
        	this.cad1ComplementoTb = new System.Windows.Forms.TextBox();
        	this.cad1CidadeTb = new System.Windows.Forms.TextBox();
        	this.label11 = new System.Windows.Forms.Label();
        	this.statusStrip1 = new System.Windows.Forms.StatusStrip();
        	this.toolStripMsg = new System.Windows.Forms.ToolStripStatusLabel();
        	this.orc1Gb = new System.Windows.Forms.GroupBox();
        	this.panel4 = new System.Windows.Forms.Panel();
        	this.orc1PagouLb = new System.Windows.Forms.Label();
        	this.orc1ParcelasLv = new System.Windows.Forms.ListView();
        	this.valorr = new System.Windows.Forms.ColumnHeader();
        	this.data = new System.Windows.Forms.ColumnHeader();
        	this.forma = new System.Windows.Forms.ColumnHeader();
        	this.orc1TotalLb = new System.Windows.Forms.Label();
        	this.label38 = new System.Windows.Forms.Label();
        	this.label36 = new System.Windows.Forms.Label();
        	this.orc1ParPn = new System.Windows.Forms.Panel();
        	this.orc1F0Cb = new System.Windows.Forms.ComboBox();
        	this.orc1ExcluiParBt = new System.Windows.Forms.Button();
        	this.orc1AddParBt = new System.Windows.Forms.Button();
        	this.orc1SomaParBt = new System.Windows.Forms.Button();
        	this.orc1P0Dtp = new System.Windows.Forms.DateTimePicker();
        	this.label35 = new System.Windows.Forms.Label();
        	this.orc1P0Tb = new System.Windows.Forms.TextBox();
        	this.label18 = new System.Windows.Forms.Label();
        	this.orc1ParIdLb = new System.Windows.Forms.Label();
        	this.label34 = new System.Windows.Forms.Label();
        	this.label23 = new System.Windows.Forms.Label();
        	this.orc1RestaLb = new System.Windows.Forms.Label();
        	this.label27 = new System.Windows.Forms.Label();
        	this.label28 = new System.Windows.Forms.Label();
        	this.panel2 = new System.Windows.Forms.Panel();
        	this.orc1ExcluirOrcBt = new System.Windows.Forms.Button();
        	this.orc1StatusCb = new System.Windows.Forms.ComboBox();
        	this.orc1PrazoDtp = new System.Windows.Forms.DateTimePicker();
        	this.orc1CodigoMtb = new System.Windows.Forms.MaskedTextBox();
        	this.panel3 = new System.Windows.Forms.Panel();
        	this.label26 = new System.Windows.Forms.Label();
        	this.label25 = new System.Windows.Forms.Label();
        	this.orc1AddIndexLb = new System.Windows.Forms.Label();
        	this.orc1AddDescTb = new System.Windows.Forms.TextBox();
        	this.orc1AddValorTb = new System.Windows.Forms.TextBox();
        	this.orc1Dg = new System.Windows.Forms.DataGridView();
        	this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.label20 = new System.Windows.Forms.Label();
        	this.label31 = new System.Windows.Forms.Label();
        	this.orc1IdLb = new System.Windows.Forms.Label();
        	this.label22333 = new System.Windows.Forms.Label();
        	this.label22 = new System.Windows.Forms.Label();
        	this.label30 = new System.Windows.Forms.Label();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.orc1EndCb = new System.Windows.Forms.ComboBox();
        	this.orc1CpfMtb = new System.Windows.Forms.MaskedTextBox();
        	this.orc1Lst = new System.Windows.Forms.ListBox();
        	this.label24 = new System.Windows.Forms.Label();
        	this.label17 = new System.Windows.Forms.Label();
        	this.orc1CcliLb = new System.Windows.Forms.Label();
        	this.orc1NomeTb = new System.Windows.Forms.TextBox();
        	this.orc1CpfLb = new System.Windows.Forms.Label();
        	this.label19 = new System.Windows.Forms.Label();
        	this.orc1SalvarBt = new System.Windows.Forms.Button();
        	this.orcXbt = new System.Windows.Forms.Button();
        	this.orcLBt = new System.Windows.Forms.Button();
        	this.rel1Gb = new System.Windows.Forms.GroupBox();
        	this.rel1AteDtp = new System.Windows.Forms.DateTimePicker();
        	this.rel1DeDtp = new System.Windows.Forms.DateTimePicker();
        	this.rel1ImprimeBt = new System.Windows.Forms.Button();
        	this.rel1RelataBt = new System.Windows.Forms.Button();
        	this.rel1Tb = new System.Windows.Forms.TextBox();
        	this.rel1XBt = new System.Windows.Forms.Button();
        	this.rel1LBt = new System.Windows.Forms.Button();
        	this.pesGb = new System.Windows.Forms.GroupBox();
        	this.pes1Lst = new System.Windows.Forms.ListBox();
        	this.button1 = new System.Windows.Forms.Button();
        	this.pes1CliTb = new System.Windows.Forms.TextBox();
        	this.button2 = new System.Windows.Forms.Button();
        	this.button3 = new System.Windows.Forms.Button();
        	this.label33 = new System.Windows.Forms.Label();
        	this.label4 = new System.Windows.Forms.Label();
        	this.rel2Gb = new System.Windows.Forms.GroupBox();
        	this.groupBox1 = new System.Windows.Forms.GroupBox();
        	this.groupBox2 = new System.Windows.Forms.GroupBox();
        	this.rel1StatusPendenteCh = new System.Windows.Forms.CheckBox();
        	this.rel2XBt = new System.Windows.Forms.Button();
        	this.rel2Dg = new System.Windows.Forms.DataGridView();
        	this.bkpGb = new System.Windows.Forms.GroupBox();
        	this.bkpInstrucoesTb = new System.Windows.Forms.TextBox();
        	this.bkpOnCloseCh = new System.Windows.Forms.CheckBox();
        	this.bkpAlterarBt = new System.Windows.Forms.Button();
        	this.bkpPachLb = new System.Windows.Forms.Label();
        	this.label21 = new System.Windows.Forms.Label();
        	this.bkpXBt = new System.Windows.Forms.Button();
        	this.menuStrip1.SuspendLayout();
        	this.cad1Gb.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.cad1Dg)).BeginInit();
        	this.cad1EndPn.SuspendLayout();
        	this.panel6.SuspendLayout();
        	this.statusStrip1.SuspendLayout();
        	this.orc1Gb.SuspendLayout();
        	this.panel4.SuspendLayout();
        	this.orc1ParPn.SuspendLayout();
        	this.panel2.SuspendLayout();
        	this.panel3.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.orc1Dg)).BeginInit();
        	this.panel1.SuspendLayout();
        	this.rel1Gb.SuspendLayout();
        	this.pesGb.SuspendLayout();
        	this.rel2Gb.SuspendLayout();
        	this.groupBox1.SuspendLayout();
        	this.groupBox2.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.rel2Dg)).BeginInit();
        	this.bkpGb.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// menuStrip1
        	// 
        	this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
        	this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.cadastroToolStripMenuItem,
			this.efetuarToolStripMenuItem,
			this.pesquisarToolStripMenuItem,
			this.relatóriosToolStripMenuItem,
			this.segurançaToolStripMenuItem,
			this.sobreToolStripMenuItem});
        	this.menuStrip1.Location = new System.Drawing.Point(0, 0);
        	this.menuStrip1.Name = "menuStrip1";
        	this.menuStrip1.Size = new System.Drawing.Size(1203, 29);
        	this.menuStrip1.TabIndex = 0;
        	this.menuStrip1.Text = "menuStrip1";
        	// 
        	// cadastroToolStripMenuItem
        	// 
        	this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.clienteToolStripMenuItem});
        	this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
        	this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(84, 25);
        	this.cadastroToolStripMenuItem.Text = "Cadastro";
        	// 
        	// clienteToolStripMenuItem
        	// 
        	this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
        	this.clienteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
        	this.clienteToolStripMenuItem.Size = new System.Drawing.Size(155, 26);
        	this.clienteToolStripMenuItem.Text = "Cliente";
        	this.clienteToolStripMenuItem.Click += new System.EventHandler(this.clienteToolStripMenuItem_Click);
        	// 
        	// efetuarToolStripMenuItem
        	// 
        	this.efetuarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.orçamentoToolStripMenuItem});
        	this.efetuarToolStripMenuItem.Name = "efetuarToolStripMenuItem";
        	this.efetuarToolStripMenuItem.Size = new System.Drawing.Size(71, 25);
        	this.efetuarToolStripMenuItem.Text = "Efetuar";
        	// 
        	// orçamentoToolStripMenuItem
        	// 
        	this.orçamentoToolStripMenuItem.Name = "orçamentoToolStripMenuItem";
        	this.orçamentoToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
        	this.orçamentoToolStripMenuItem.Size = new System.Drawing.Size(185, 26);
        	this.orçamentoToolStripMenuItem.Text = "Orçamento";
        	this.orçamentoToolStripMenuItem.Click += new System.EventHandler(this.orçamentoToolStripMenuItem_Click);
        	// 
        	// pesquisarToolStripMenuItem
        	// 
        	this.pesquisarToolStripMenuItem.Name = "pesquisarToolStripMenuItem";
        	this.pesquisarToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
        	this.pesquisarToolStripMenuItem.Size = new System.Drawing.Size(89, 25);
        	this.pesquisarToolStripMenuItem.Text = "Pesquisar";
        	this.pesquisarToolStripMenuItem.Visible = false;
        	this.pesquisarToolStripMenuItem.Click += new System.EventHandler(this.pesquisarToolStripMenuItem_Click);
        	// 
        	// relatóriosToolStripMenuItem
        	// 
        	this.relatóriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.pendendênciasToolStripMenuItem});
        	this.relatóriosToolStripMenuItem.Name = "relatóriosToolStripMenuItem";
        	this.relatóriosToolStripMenuItem.Size = new System.Drawing.Size(92, 25);
        	this.relatóriosToolStripMenuItem.Text = "Relatórios";
        	// 
        	// pendendênciasToolStripMenuItem
        	// 
        	this.pendendênciasToolStripMenuItem.Name = "pendendênciasToolStripMenuItem";
        	this.pendendênciasToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
        	this.pendendênciasToolStripMenuItem.Text = "Pendendências";
        	this.pendendênciasToolStripMenuItem.Click += new System.EventHandler(this.PendendênciasToolStripMenuItemClick);
        	// 
        	// segurançaToolStripMenuItem
        	// 
        	this.segurançaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.backupRápidoToolStripMenuItem,
			this.backupToolStripMenuItem,
			this.configuraçõesDeBackupToolStripMenuItem});
        	this.segurançaToolStripMenuItem.Name = "segurançaToolStripMenuItem";
        	this.segurançaToolStripMenuItem.Size = new System.Drawing.Size(95, 25);
        	this.segurançaToolStripMenuItem.Text = "Segurança";
        	// 
        	// backupRápidoToolStripMenuItem
        	// 
        	this.backupRápidoToolStripMenuItem.Name = "backupRápidoToolStripMenuItem";
        	this.backupRápidoToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
        	this.backupRápidoToolStripMenuItem.Text = "Backup Rápido";
        	this.backupRápidoToolStripMenuItem.Click += new System.EventHandler(this.BackupRápidoToolStripMenuItemClick);
        	// 
        	// backupToolStripMenuItem
        	// 
        	this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
        	this.backupToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
        	this.backupToolStripMenuItem.Text = "Backup (Escolher destino)";
        	this.backupToolStripMenuItem.Click += new System.EventHandler(this.BackupToolStripMenuItemClick);
        	// 
        	// configuraçõesDeBackupToolStripMenuItem
        	// 
        	this.configuraçõesDeBackupToolStripMenuItem.Name = "configuraçõesDeBackupToolStripMenuItem";
        	this.configuraçõesDeBackupToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
        	this.configuraçõesDeBackupToolStripMenuItem.Text = "Configurações de Backup";
        	this.configuraçõesDeBackupToolStripMenuItem.Click += new System.EventHandler(this.ConfiguraçõesDeBackupToolStripMenuItemClick);
        	// 
        	// sobreToolStripMenuItem
        	// 
        	this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
        	this.sobreToolStripMenuItem.Size = new System.Drawing.Size(63, 25);
        	this.sobreToolStripMenuItem.Text = "Sobre";
        	this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
        	// 
        	// cad1Gb
        	// 
        	this.cad1Gb.Controls.Add(this.cad1AtualizarclienteBt);
        	this.cad1Gb.Controls.Add(this.cad1ExcluirBt);
        	this.cad1Gb.Controls.Add(this.cad1TipoCb);
        	this.cad1Gb.Controls.Add(this.cad1Dg);
        	this.cad1Gb.Controls.Add(this.cad1Lst);
        	this.cad1Gb.Controls.Add(this.label10);
        	this.cad1Gb.Controls.Add(this.label14);
        	this.cad1Gb.Controls.Add(this.cad1ObsTb);
        	this.cad1Gb.Controls.Add(this.cad1CpfTb);
        	this.cad1Gb.Controls.Add(this.cad1CodigoMtb);
        	this.cad1Gb.Controls.Add(this.label13);
        	this.cad1Gb.Controls.Add(this.label12);
        	this.cad1Gb.Controls.Add(this.label29);
        	this.cad1Gb.Controls.Add(this.cad1CpfCnpjLb);
        	this.cad1Gb.Controls.Add(this.cad1VaiLb);
        	this.cad1Gb.Controls.Add(this.cad1VemLb);
        	this.cad1Gb.Controls.Add(this.cad1IdLb);
        	this.cad1Gb.Controls.Add(this.label16);
        	this.cad1Gb.Controls.Add(this.label15);
        	this.cad1Gb.Controls.Add(this.label1);
        	this.cad1Gb.Controls.Add(this.cad1Fone2Tb);
        	this.cad1Gb.Controls.Add(this.cad1Fone1Tb);
        	this.cad1Gb.Controls.Add(this.cad1EmailTb);
        	this.cad1Gb.Controls.Add(this.cad1NomeTb);
        	this.cad1Gb.Controls.Add(this.cad1SalvarBt);
        	this.cad1Gb.Controls.Add(this.cad1LBt);
        	this.cad1Gb.Controls.Add(this.cad1XBt);
        	this.cad1Gb.Controls.Add(this.cad1EndPn);
        	this.cad1Gb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.cad1Gb.Location = new System.Drawing.Point(23, 35);
        	this.cad1Gb.Name = "cad1Gb";
        	this.cad1Gb.Size = new System.Drawing.Size(1057, 612);
        	this.cad1Gb.TabIndex = 0;
        	this.cad1Gb.TabStop = false;
        	this.cad1Gb.Text = "Cliente";
        	this.cad1Gb.Visible = false;
        	this.cad1Gb.VisibleChanged += new System.EventHandler(this.Cad1GbVisibleChanged);
        	this.cad1Gb.Enter += new System.EventHandler(this.Cad1GbEnter);
        	// 
        	// cad1AtualizarclienteBt
        	// 
        	this.cad1AtualizarclienteBt.Location = new System.Drawing.Point(20, 522);
        	this.cad1AtualizarclienteBt.Name = "cad1AtualizarclienteBt";
        	this.cad1AtualizarclienteBt.Size = new System.Drawing.Size(404, 33);
        	this.cad1AtualizarclienteBt.TabIndex = 23;
        	this.cad1AtualizarclienteBt.Text = "Atualizar Cliente";
        	this.cad1AtualizarclienteBt.UseVisualStyleBackColor = true;
        	this.cad1AtualizarclienteBt.Visible = false;
        	this.cad1AtualizarclienteBt.Click += new System.EventHandler(this.Cad1AtualizarclienteBtClick);
        	// 
        	// cad1ExcluirBt
        	// 
        	this.cad1ExcluirBt.BackColor = System.Drawing.Color.Crimson;
        	this.cad1ExcluirBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.cad1ExcluirBt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
        	this.cad1ExcluirBt.Location = new System.Drawing.Point(968, 585);
        	this.cad1ExcluirBt.Name = "cad1ExcluirBt";
        	this.cad1ExcluirBt.Size = new System.Drawing.Size(83, 21);
        	this.cad1ExcluirBt.TabIndex = 22;
        	this.cad1ExcluirBt.Text = "Excluir Cliente";
        	this.cad1ExcluirBt.UseVisualStyleBackColor = false;
        	this.cad1ExcluirBt.Visible = false;
        	this.cad1ExcluirBt.Click += new System.EventHandler(this.Cad1ExcluirBtClick);
        	// 
        	// cad1TipoCb
        	// 
        	this.cad1TipoCb.FormattingEnabled = true;
        	this.cad1TipoCb.Items.AddRange(new object[] {
			"Fisica",
			"Juridica"});
        	this.cad1TipoCb.Location = new System.Drawing.Point(19, 177);
        	this.cad1TipoCb.Name = "cad1TipoCb";
        	this.cad1TipoCb.Size = new System.Drawing.Size(121, 28);
        	this.cad1TipoCb.TabIndex = 20;
        	this.cad1TipoCb.Text = "Fisica";
        	this.cad1TipoCb.SelectedIndexChanged += new System.EventHandler(this.Cad1TipoCbSelectedIndexChanged);
        	// 
        	// cad1Dg
        	// 
        	this.cad1Dg.AllowUserToAddRows = false;
        	this.cad1Dg.AllowUserToDeleteRows = false;
        	this.cad1Dg.AllowUserToResizeColumns = false;
        	this.cad1Dg.AllowUserToResizeRows = false;
        	this.cad1Dg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
        	this.cad1Dg.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
        	this.cad1Dg.BackgroundColor = System.Drawing.SystemColors.Control;
        	this.cad1Dg.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.cad1Dg.CausesValidation = false;
        	this.cad1Dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.cad1Dg.ColumnHeadersVisible = false;
        	this.cad1Dg.Enabled = false;
        	this.cad1Dg.Location = new System.Drawing.Point(747, 463);
        	this.cad1Dg.MultiSelect = false;
        	this.cad1Dg.Name = "cad1Dg";
        	this.cad1Dg.ReadOnly = true;
        	this.cad1Dg.RowHeadersVisible = false;
        	this.cad1Dg.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        	this.cad1Dg.ScrollBars = System.Windows.Forms.ScrollBars.None;
        	this.cad1Dg.ShowCellErrors = false;
        	this.cad1Dg.ShowCellToolTips = false;
        	this.cad1Dg.ShowEditingIcon = false;
        	this.cad1Dg.ShowRowErrors = false;
        	this.cad1Dg.Size = new System.Drawing.Size(240, 103);
        	this.cad1Dg.TabIndex = 19;
        	// 
        	// cad1Lst
        	// 
        	this.cad1Lst.FormattingEnabled = true;
        	this.cad1Lst.ItemHeight = 20;
        	this.cad1Lst.Location = new System.Drawing.Point(451, 476);
        	this.cad1Lst.Name = "cad1Lst";
        	this.cad1Lst.Size = new System.Drawing.Size(229, 84);
        	this.cad1Lst.TabIndex = 18;
        	this.cad1Lst.Visible = false;
        	this.cad1Lst.SelectedIndexChanged += new System.EventHandler(this.cad1Lst_SelectedIndexChanged);
        	// 
        	// label10
        	// 
        	this.label10.AutoSize = true;
        	this.label10.Location = new System.Drawing.Point(15, 340);
        	this.label10.Name = "label10";
        	this.label10.Size = new System.Drawing.Size(102, 20);
        	this.label10.TabIndex = 7;
        	this.label10.Text = "Observações";
        	// 
        	// label14
        	// 
        	this.label14.AutoSize = true;
        	this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label14.Location = new System.Drawing.Point(451, 458);
        	this.label14.Name = "label14";
        	this.label14.Size = new System.Drawing.Size(110, 15);
        	this.label14.TabIndex = 7;
        	this.label14.Text = "Orçamentos Feitos";
        	// 
        	// cad1ObsTb
        	// 
        	this.cad1ObsTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1ObsTb.Location = new System.Drawing.Point(19, 363);
        	this.cad1ObsTb.MaxLength = 300;
        	this.cad1ObsTb.Multiline = true;
        	this.cad1ObsTb.Name = "cad1ObsTb";
        	this.cad1ObsTb.ScrollBars = System.Windows.Forms.ScrollBars.Both;
        	this.cad1ObsTb.Size = new System.Drawing.Size(404, 153);
        	this.cad1ObsTb.TabIndex = 6;
        	this.cad1ObsTb.Leave += new System.EventHandler(this.Cad1ObsTbLeave);
        	// 
        	// cad1CpfTb
        	// 
        	this.cad1CpfTb.Location = new System.Drawing.Point(243, 182);
        	this.cad1CpfTb.Mask = "000,000,000-00";
        	this.cad1CpfTb.Name = "cad1CpfTb";
        	this.cad1CpfTb.Size = new System.Drawing.Size(180, 26);
        	this.cad1CpfTb.TabIndex = 1;
        	this.cad1CpfTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	this.cad1CpfTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1CpfTb_KeyDown);
        	// 
        	// cad1CodigoMtb
        	// 
        	this.cad1CodigoMtb.Location = new System.Drawing.Point(20, 41);
        	this.cad1CodigoMtb.Mask = "00000000";
        	this.cad1CodigoMtb.Name = "cad1CodigoMtb";
        	this.cad1CodigoMtb.Size = new System.Drawing.Size(89, 26);
        	this.cad1CodigoMtb.TabIndex = 0;
        	this.cad1CodigoMtb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	this.cad1CodigoMtb.Visible = false;
        	this.cad1CodigoMtb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1CodigoMtb_KeyDown);
        	// 
        	// label13
        	// 
        	this.label13.AutoSize = true;
        	this.label13.Location = new System.Drawing.Point(239, 220);
        	this.label13.Name = "label13";
        	this.label13.Size = new System.Drawing.Size(38, 20);
        	this.label13.TabIndex = 4;
        	this.label13.Text = "Fixo";
        	// 
        	// label12
        	// 
        	this.label12.AutoSize = true;
        	this.label12.Location = new System.Drawing.Point(15, 220);
        	this.label12.Name = "label12";
        	this.label12.Size = new System.Drawing.Size(58, 20);
        	this.label12.TabIndex = 4;
        	this.label12.Text = "Celular";
        	// 
        	// label29
        	// 
        	this.label29.AutoSize = true;
        	this.label29.Location = new System.Drawing.Point(19, 154);
        	this.label29.Name = "label29";
        	this.label29.Size = new System.Drawing.Size(62, 20);
        	this.label29.TabIndex = 4;
        	this.label29.Text = "Pessoa";
        	// 
        	// cad1CpfCnpjLb
        	// 
        	this.cad1CpfCnpjLb.AutoSize = true;
        	this.cad1CpfCnpjLb.Location = new System.Drawing.Point(243, 159);
        	this.cad1CpfCnpjLb.Name = "cad1CpfCnpjLb";
        	this.cad1CpfCnpjLb.Size = new System.Drawing.Size(40, 20);
        	this.cad1CpfCnpjLb.TabIndex = 4;
        	this.cad1CpfCnpjLb.Text = "CPF";
        	// 
        	// cad1VaiLb
        	// 
        	this.cad1VaiLb.AutoSize = true;
        	this.cad1VaiLb.Location = new System.Drawing.Point(91, 74);
        	this.cad1VaiLb.Name = "cad1VaiLb";
        	this.cad1VaiLb.Size = new System.Drawing.Size(18, 20);
        	this.cad1VaiLb.TabIndex = 4;
        	this.cad1VaiLb.Text = ">";
        	this.cad1VaiLb.Visible = false;
        	this.cad1VaiLb.Click += new System.EventHandler(this.cad1VaiLb_Click);
        	// 
        	// cad1VemLb
        	// 
        	this.cad1VemLb.AutoSize = true;
        	this.cad1VemLb.Location = new System.Drawing.Point(15, 74);
        	this.cad1VemLb.Name = "cad1VemLb";
        	this.cad1VemLb.Size = new System.Drawing.Size(18, 20);
        	this.cad1VemLb.TabIndex = 4;
        	this.cad1VemLb.Text = "<";
        	this.cad1VemLb.Visible = false;
        	this.cad1VemLb.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cad1VemLb_MouseDown);
        	// 
        	// cad1IdLb
        	// 
        	this.cad1IdLb.AutoSize = true;
        	this.cad1IdLb.Location = new System.Drawing.Point(403, 98);
        	this.cad1IdLb.Name = "cad1IdLb";
        	this.cad1IdLb.Size = new System.Drawing.Size(18, 20);
        	this.cad1IdLb.TabIndex = 4;
        	this.cad1IdLb.Text = "0";
        	this.cad1IdLb.TextChanged += new System.EventHandler(this.Cad1IdLbTextChanged);
        	// 
        	// label16
        	// 
        	this.label16.AutoSize = true;
        	this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label16.Location = new System.Drawing.Point(17, 24);
        	this.label16.Name = "label16";
        	this.label16.Size = new System.Drawing.Size(58, 15);
        	this.label16.TabIndex = 4;
        	this.label16.Text = "Pesquisa";
        	this.label16.Visible = false;
        	// 
        	// label15
        	// 
        	this.label15.AutoSize = true;
        	this.label15.Location = new System.Drawing.Point(15, 285);
        	this.label15.Name = "label15";
        	this.label15.Size = new System.Drawing.Size(48, 20);
        	this.label15.TabIndex = 4;
        	this.label15.Text = "Email";
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(13, 101);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(51, 20);
        	this.label1.TabIndex = 4;
        	this.label1.Text = "Nome";
        	// 
        	// cad1Fone2Tb
        	// 
        	this.cad1Fone2Tb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1Fone2Tb.Location = new System.Drawing.Point(243, 243);
        	this.cad1Fone2Tb.MaxLength = 16;
        	this.cad1Fone2Tb.Name = "cad1Fone2Tb";
        	this.cad1Fone2Tb.Size = new System.Drawing.Size(180, 26);
        	this.cad1Fone2Tb.TabIndex = 4;
        	this.cad1Fone2Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1Fone2Tb_KeyDown);
        	// 
        	// cad1Fone1Tb
        	// 
        	this.cad1Fone1Tb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1Fone1Tb.Location = new System.Drawing.Point(19, 243);
        	this.cad1Fone1Tb.MaxLength = 16;
        	this.cad1Fone1Tb.Name = "cad1Fone1Tb";
        	this.cad1Fone1Tb.Size = new System.Drawing.Size(180, 26);
        	this.cad1Fone1Tb.TabIndex = 3;
        	this.cad1Fone1Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1Fone1Tb_KeyDown);
        	// 
        	// cad1EmailTb
        	// 
        	this.cad1EmailTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1EmailTb.Location = new System.Drawing.Point(19, 308);
        	this.cad1EmailTb.MaxLength = 300;
        	this.cad1EmailTb.Name = "cad1EmailTb";
        	this.cad1EmailTb.Size = new System.Drawing.Size(404, 26);
        	this.cad1EmailTb.TabIndex = 5;
        	this.cad1EmailTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1EmailTb_KeyDown);
        	// 
        	// cad1NomeTb
        	// 
        	this.cad1NomeTb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
        	this.cad1NomeTb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
        	this.cad1NomeTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1NomeTb.Location = new System.Drawing.Point(20, 124);
        	this.cad1NomeTb.MaxLength = 300;
        	this.cad1NomeTb.Name = "cad1NomeTb";
        	this.cad1NomeTb.Size = new System.Drawing.Size(404, 26);
        	this.cad1NomeTb.TabIndex = 2;
        	this.cad1NomeTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1NomeTb_KeyDown);
        	// 
        	// cad1SalvarBt
        	// 
        	this.cad1SalvarBt.Location = new System.Drawing.Point(19, 572);
        	this.cad1SalvarBt.Name = "cad1SalvarBt";
        	this.cad1SalvarBt.Size = new System.Drawing.Size(916, 30);
        	this.cad1SalvarBt.TabIndex = 15;
        	this.cad1SalvarBt.Text = "Salvar";
        	this.cad1SalvarBt.UseVisualStyleBackColor = true;
        	this.cad1SalvarBt.Click += new System.EventHandler(this.cad1SalvarBt_Click);
        	// 
        	// cad1LBt
        	// 
        	this.cad1LBt.BackColor = System.Drawing.Color.SlateGray;
        	this.cad1LBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.cad1LBt.Location = new System.Drawing.Point(985, 23);
        	this.cad1LBt.Name = "cad1LBt";
        	this.cad1LBt.Size = new System.Drawing.Size(30, 28);
        	this.cad1LBt.TabIndex = 16;
        	this.cad1LBt.Text = "L";
        	this.cad1LBt.UseVisualStyleBackColor = false;
        	this.cad1LBt.Click += new System.EventHandler(this.cad1LBt_Click);
        	// 
        	// cad1XBt
        	// 
        	this.cad1XBt.BackColor = System.Drawing.Color.Firebrick;
        	this.cad1XBt.Location = new System.Drawing.Point(1021, 23);
        	this.cad1XBt.Name = "cad1XBt";
        	this.cad1XBt.Size = new System.Drawing.Size(30, 28);
        	this.cad1XBt.TabIndex = 17;
        	this.cad1XBt.Text = "X";
        	this.cad1XBt.UseVisualStyleBackColor = false;
        	this.cad1XBt.Click += new System.EventHandler(this.cad1XBt_Click);
        	// 
        	// cad1EndPn
        	// 
        	this.cad1EndPn.BackColor = System.Drawing.SystemColors.ControlLight;
        	this.cad1EndPn.Controls.Add(this.cad1EndAtualizarBt);
        	this.cad1EndPn.Controls.Add(this.cad1NewEndBt);
        	this.cad1EndPn.Controls.Add(this.cad1TipologCb);
        	this.cad1EndPn.Controls.Add(this.panel6);
        	this.cad1EndPn.Controls.Add(this.label37);
        	this.cad1EndPn.Controls.Add(this.label8);
        	this.cad1EndPn.Controls.Add(this.cad1EnderecoTb);
        	this.cad1EndPn.Controls.Add(this.cad1NumeroTb);
        	this.cad1EndPn.Controls.Add(this.label9);
        	this.cad1EndPn.Controls.Add(this.label3);
        	this.cad1EndPn.Controls.Add(this.label2);
        	this.cad1EndPn.Controls.Add(this.cad1CepMtb);
        	this.cad1EndPn.Controls.Add(this.label6);
        	this.cad1EndPn.Controls.Add(this.cad1BairroTb);
        	this.cad1EndPn.Controls.Add(this.cad1UfTb);
        	this.cad1EndPn.Controls.Add(this.label5);
        	this.cad1EndPn.Controls.Add(this.cad1ComplementoTb);
        	this.cad1EndPn.Controls.Add(this.cad1CidadeTb);
        	this.cad1EndPn.Controls.Add(this.label11);
        	this.cad1EndPn.Enabled = false;
        	this.cad1EndPn.Location = new System.Drawing.Point(458, 51);
        	this.cad1EndPn.Name = "cad1EndPn";
        	this.cad1EndPn.Size = new System.Drawing.Size(529, 406);
        	this.cad1EndPn.TabIndex = 21;
        	// 
        	// cad1EndAtualizarBt
        	// 
        	this.cad1EndAtualizarBt.Enabled = false;
        	this.cad1EndAtualizarBt.Location = new System.Drawing.Point(13, 348);
        	this.cad1EndAtualizarBt.Name = "cad1EndAtualizarBt";
        	this.cad1EndAtualizarBt.Size = new System.Drawing.Size(157, 26);
        	this.cad1EndAtualizarBt.TabIndex = 3;
        	this.cad1EndAtualizarBt.Text = "Atualizar";
        	this.cad1EndAtualizarBt.UseVisualStyleBackColor = true;
        	this.cad1EndAtualizarBt.Click += new System.EventHandler(this.Cad1EndAtualizarBtClick);
        	// 
        	// cad1NewEndBt
        	// 
        	this.cad1NewEndBt.Location = new System.Drawing.Point(358, 348);
        	this.cad1NewEndBt.Name = "cad1NewEndBt";
        	this.cad1NewEndBt.Size = new System.Drawing.Size(157, 26);
        	this.cad1NewEndBt.TabIndex = 3;
        	this.cad1NewEndBt.Text = "Adicionar Novo";
        	this.cad1NewEndBt.UseVisualStyleBackColor = true;
        	this.cad1NewEndBt.Visible = false;
        	this.cad1NewEndBt.Click += new System.EventHandler(this.Cad1NewEndBtClick);
        	// 
        	// cad1TipologCb
        	// 
        	this.cad1TipologCb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.cad1TipologCb.FormattingEnabled = true;
        	this.cad1TipologCb.Location = new System.Drawing.Point(13, 159);
        	this.cad1TipologCb.Name = "cad1TipologCb";
        	this.cad1TipologCb.Size = new System.Drawing.Size(94, 23);
        	this.cad1TipologCb.TabIndex = 7;
        	this.cad1TipologCb.SelectedIndexChanged += new System.EventHandler(this.Cad1TipoCbSelectedIndexChanged);
        	// 
        	// panel6
        	// 
        	this.panel6.BackColor = System.Drawing.SystemColors.ControlDark;
        	this.panel6.Controls.Add(this.cad1ExcluirEndBt);
        	this.panel6.Controls.Add(this.cad1EnderecosCb);
        	this.panel6.Controls.Add(this.cad1EndLb);
        	this.panel6.Controls.Add(this.label7);
        	this.panel6.Location = new System.Drawing.Point(0, 0);
        	this.panel6.Name = "panel6";
        	this.panel6.Size = new System.Drawing.Size(529, 74);
        	this.panel6.TabIndex = 14;
        	// 
        	// cad1ExcluirEndBt
        	// 
        	this.cad1ExcluirEndBt.BackColor = System.Drawing.Color.Crimson;
        	this.cad1ExcluirEndBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.cad1ExcluirEndBt.Location = new System.Drawing.Point(417, 51);
        	this.cad1ExcluirEndBt.Name = "cad1ExcluirEndBt";
        	this.cad1ExcluirEndBt.Size = new System.Drawing.Size(99, 23);
        	this.cad1ExcluirEndBt.TabIndex = 5;
        	this.cad1ExcluirEndBt.Text = "Excluir endereço";
        	this.cad1ExcluirEndBt.UseVisualStyleBackColor = false;
        	this.cad1ExcluirEndBt.Click += new System.EventHandler(this.Cad1ExcluirEndBtClick);
        	// 
        	// cad1EnderecosCb
        	// 
        	this.cad1EnderecosCb.FormattingEnabled = true;
        	this.cad1EnderecosCb.Location = new System.Drawing.Point(13, 23);
        	this.cad1EnderecosCb.Name = "cad1EnderecosCb";
        	this.cad1EnderecosCb.Size = new System.Drawing.Size(502, 28);
        	this.cad1EnderecosCb.TabIndex = 0;
        	this.cad1EnderecosCb.SelectedIndexChanged += new System.EventHandler(this.Cad1EnderecosCbSelectedIndexChanged);
        	// 
        	// cad1EndLb
        	// 
        	this.cad1EndLb.AutoSize = true;
        	this.cad1EndLb.Location = new System.Drawing.Point(105, 3);
        	this.cad1EndLb.Name = "cad1EndLb";
        	this.cad1EndLb.Size = new System.Drawing.Size(23, 20);
        	this.cad1EndLb.TabIndex = 4;
        	this.cad1EndLb.Text = "-1";
        	this.cad1EndLb.Visible = false;
        	this.cad1EndLb.TextChanged += new System.EventHandler(this.Cad1EndLbTextChanged);
        	// 
        	// label7
        	// 
        	this.label7.AutoSize = true;
        	this.label7.Location = new System.Drawing.Point(13, 3);
        	this.label7.Name = "label7";
        	this.label7.Size = new System.Drawing.Size(86, 20);
        	this.label7.TabIndex = 4;
        	this.label7.Text = "Endereços";
        	// 
        	// label37
        	// 
        	this.label37.AutoSize = true;
        	this.label37.Location = new System.Drawing.Point(13, 133);
        	this.label37.Name = "label37";
        	this.label37.Size = new System.Drawing.Size(39, 20);
        	this.label37.TabIndex = 4;
        	this.label37.Text = "Tipo";
        	// 
        	// label8
        	// 
        	this.label8.AutoSize = true;
        	this.label8.Location = new System.Drawing.Point(9, 79);
        	this.label8.Name = "label8";
        	this.label8.Size = new System.Drawing.Size(41, 20);
        	this.label8.TabIndex = 4;
        	this.label8.Text = "CEP";
        	// 
        	// cad1EnderecoTb
        	// 
        	this.cad1EnderecoTb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
        	this.cad1EnderecoTb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
        	this.cad1EnderecoTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1EnderecoTb.Location = new System.Drawing.Point(109, 157);
        	this.cad1EnderecoTb.MaxLength = 300;
        	this.cad1EnderecoTb.Name = "cad1EnderecoTb";
        	this.cad1EnderecoTb.Size = new System.Drawing.Size(404, 26);
        	this.cad1EnderecoTb.TabIndex = 8;
        	this.cad1EnderecoTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1EnderecoTb_KeyDown);
        	// 
        	// cad1NumeroTb
        	// 
        	this.cad1NumeroTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1NumeroTb.Location = new System.Drawing.Point(13, 210);
        	this.cad1NumeroTb.MaxLength = 6;
        	this.cad1NumeroTb.Name = "cad1NumeroTb";
        	this.cad1NumeroTb.Size = new System.Drawing.Size(92, 26);
        	this.cad1NumeroTb.TabIndex = 9;
        	this.cad1NumeroTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1NumeroTb_KeyDown);
        	// 
        	// label9
        	// 
        	this.label9.AutoSize = true;
        	this.label9.Location = new System.Drawing.Point(456, 293);
        	this.label9.Name = "label9";
        	this.label9.Size = new System.Drawing.Size(60, 20);
        	this.label9.TabIndex = 7;
        	this.label9.Text = "Estado";
        	// 
        	// label3
        	// 
        	this.label3.AutoSize = true;
        	this.label3.Location = new System.Drawing.Point(105, 133);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(78, 20);
        	this.label3.TabIndex = 4;
        	this.label3.Text = "Endereco";
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(9, 187);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(65, 20);
        	this.label2.TabIndex = 4;
        	this.label2.Text = "Numero";
        	// 
        	// cad1CepMtb
        	// 
        	this.cad1CepMtb.Location = new System.Drawing.Point(13, 102);
        	this.cad1CepMtb.Mask = "00000000";
        	this.cad1CepMtb.Name = "cad1CepMtb";
        	this.cad1CepMtb.Size = new System.Drawing.Size(80, 26);
        	this.cad1CepMtb.TabIndex = 7;
        	this.cad1CepMtb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	this.cad1CepMtb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1CepMtb_KeyDown);
        	// 
        	// label6
        	// 
        	this.label6.AutoSize = true;
        	this.label6.Location = new System.Drawing.Point(9, 293);
        	this.label6.Name = "label6";
        	this.label6.Size = new System.Drawing.Size(59, 20);
        	this.label6.TabIndex = 7;
        	this.label6.Text = "Cidade";
        	// 
        	// cad1BairroTb
        	// 
        	this.cad1BairroTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1BairroTb.Location = new System.Drawing.Point(111, 210);
        	this.cad1BairroTb.MaxLength = 300;
        	this.cad1BairroTb.Name = "cad1BairroTb";
        	this.cad1BairroTb.Size = new System.Drawing.Size(407, 26);
        	this.cad1BairroTb.TabIndex = 10;
        	this.cad1BairroTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1BairroTb_KeyDown);
        	// 
        	// cad1UfTb
        	// 
        	this.cad1UfTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1UfTb.Location = new System.Drawing.Point(460, 316);
        	this.cad1UfTb.MaxLength = 2;
        	this.cad1UfTb.Name = "cad1UfTb";
        	this.cad1UfTb.Size = new System.Drawing.Size(55, 26);
        	this.cad1UfTb.TabIndex = 2;
        	this.cad1UfTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1UfTb_KeyDown);
        	// 
        	// label5
        	// 
        	this.label5.AutoSize = true;
        	this.label5.Location = new System.Drawing.Point(105, 187);
        	this.label5.Name = "label5";
        	this.label5.Size = new System.Drawing.Size(51, 20);
        	this.label5.TabIndex = 7;
        	this.label5.Text = "Bairro";
        	// 
        	// cad1ComplementoTb
        	// 
        	this.cad1ComplementoTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1ComplementoTb.Location = new System.Drawing.Point(13, 266);
        	this.cad1ComplementoTb.MaxLength = 300;
        	this.cad1ComplementoTb.Name = "cad1ComplementoTb";
        	this.cad1ComplementoTb.Size = new System.Drawing.Size(502, 26);
        	this.cad1ComplementoTb.TabIndex = 0;
        	this.cad1ComplementoTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1ComplementoTb_KeyDown);
        	// 
        	// cad1CidadeTb
        	// 
        	this.cad1CidadeTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.cad1CidadeTb.Location = new System.Drawing.Point(13, 316);
        	this.cad1CidadeTb.MaxLength = 300;
        	this.cad1CidadeTb.Name = "cad1CidadeTb";
        	this.cad1CidadeTb.Size = new System.Drawing.Size(441, 26);
        	this.cad1CidadeTb.TabIndex = 1;
        	this.cad1CidadeTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1CidadeTb_KeyDown);
        	// 
        	// label11
        	// 
        	this.label11.AutoSize = true;
        	this.label11.Location = new System.Drawing.Point(9, 243);
        	this.label11.Name = "label11";
        	this.label11.Size = new System.Drawing.Size(108, 20);
        	this.label11.TabIndex = 7;
        	this.label11.Text = "Complemento";
        	// 
        	// statusStrip1
        	// 
        	this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 14F);
        	this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMsg});
        	this.statusStrip1.Location = new System.Drawing.Point(0, 688);
        	this.statusStrip1.Name = "statusStrip1";
        	this.statusStrip1.Size = new System.Drawing.Size(1203, 30);
        	this.statusStrip1.TabIndex = 2;
        	this.statusStrip1.Text = "statusStrip1";
        	// 
        	// toolStripMsg
        	// 
        	this.toolStripMsg.Name = "toolStripMsg";
        	this.toolStripMsg.Size = new System.Drawing.Size(72, 25);
        	this.toolStripMsg.Text = "Status: ";
        	// 
        	// orc1Gb
        	// 
        	this.orc1Gb.Controls.Add(this.panel4);
        	this.orc1Gb.Controls.Add(this.panel2);
        	this.orc1Gb.Controls.Add(this.panel1);
        	this.orc1Gb.Controls.Add(this.orc1SalvarBt);
        	this.orc1Gb.Controls.Add(this.orcXbt);
        	this.orc1Gb.Controls.Add(this.orcLBt);
        	this.orc1Gb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orc1Gb.Location = new System.Drawing.Point(12, 35);
        	this.orc1Gb.Name = "orc1Gb";
        	this.orc1Gb.Size = new System.Drawing.Size(1146, 650);
        	this.orc1Gb.TabIndex = 3;
        	this.orc1Gb.TabStop = false;
        	this.orc1Gb.Text = "Orçamento";
        	this.orc1Gb.Visible = false;
        	this.orc1Gb.VisibleChanged += new System.EventHandler(this.orc1Gb_VisibleChanged);
        	// 
        	// panel4
        	// 
        	this.panel4.BackColor = System.Drawing.Color.Silver;
        	this.panel4.Controls.Add(this.orc1PagouLb);
        	this.panel4.Controls.Add(this.orc1ParcelasLv);
        	this.panel4.Controls.Add(this.orc1TotalLb);
        	this.panel4.Controls.Add(this.label38);
        	this.panel4.Controls.Add(this.label36);
        	this.panel4.Controls.Add(this.orc1ParPn);
        	this.panel4.Controls.Add(this.label23);
        	this.panel4.Controls.Add(this.orc1RestaLb);
        	this.panel4.Controls.Add(this.label27);
        	this.panel4.Controls.Add(this.label28);
        	this.panel4.Location = new System.Drawing.Point(697, 163);
        	this.panel4.Name = "panel4";
        	this.panel4.Size = new System.Drawing.Size(438, 429);
        	this.panel4.TabIndex = 9;
        	// 
        	// orc1PagouLb
        	// 
        	this.orc1PagouLb.AutoSize = true;
        	this.orc1PagouLb.Location = new System.Drawing.Point(276, 368);
        	this.orc1PagouLb.Name = "orc1PagouLb";
        	this.orc1PagouLb.Size = new System.Drawing.Size(40, 20);
        	this.orc1PagouLb.TabIndex = 3;
        	this.orc1PagouLb.Text = "0,00";
        	// 
        	// orc1ParcelasLv
        	// 
        	this.orc1ParcelasLv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
			this.valorr,
			this.data,
			this.forma});
        	this.orc1ParcelasLv.FullRowSelect = true;
        	this.orc1ParcelasLv.GridLines = true;
        	this.orc1ParcelasLv.Location = new System.Drawing.Point(9, 123);
        	this.orc1ParcelasLv.Name = "orc1ParcelasLv";
        	this.orc1ParcelasLv.ShowGroups = false;
        	this.orc1ParcelasLv.Size = new System.Drawing.Size(421, 212);
        	this.orc1ParcelasLv.TabIndex = 7;
        	this.orc1ParcelasLv.UseCompatibleStateImageBehavior = false;
        	this.orc1ParcelasLv.View = System.Windows.Forms.View.Details;
        	this.orc1ParcelasLv.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.Orc1ParcelasLvItemSelectionChanged);
        	// 
        	// valorr
        	// 
        	this.valorr.Text = "Valor";
        	this.valorr.Width = 130;
        	// 
        	// data
        	// 
        	this.data.Text = "Data";
        	this.data.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	this.data.Width = 120;
        	// 
        	// forma
        	// 
        	this.forma.Text = "Pagamento";
        	this.forma.Width = 300;
        	// 
        	// orc1TotalLb
        	// 
        	this.orc1TotalLb.AutoSize = true;
        	this.orc1TotalLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orc1TotalLb.Location = new System.Drawing.Point(263, 339);
        	this.orc1TotalLb.Name = "orc1TotalLb";
        	this.orc1TotalLb.Size = new System.Drawing.Size(58, 29);
        	this.orc1TotalLb.TabIndex = 3;
        	this.orc1TotalLb.Text = "0,00";
        	this.orc1TotalLb.TextAlign = System.Drawing.ContentAlignment.TopRight;
        	// 
        	// label38
        	// 
        	this.label38.AutoSize = true;
        	this.label38.ForeColor = System.Drawing.Color.Black;
        	this.label38.Location = new System.Drawing.Point(135, 369);
        	this.label38.Name = "label38";
        	this.label38.Size = new System.Drawing.Size(167, 20);
        	this.label38.TabIndex = 3;
        	this.label38.Text = "Pagou............................";
        	// 
        	// label36
        	// 
        	this.label36.AutoSize = true;
        	this.label36.ForeColor = System.Drawing.Color.Black;
        	this.label36.Location = new System.Drawing.Point(133, 346);
        	this.label36.Name = "label36";
        	this.label36.Size = new System.Drawing.Size(156, 20);
        	this.label36.TabIndex = 3;
        	this.label36.Text = "Total............................";
        	// 
        	// orc1ParPn
        	// 
        	this.orc1ParPn.BackColor = System.Drawing.Color.DarkGray;
        	this.orc1ParPn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.orc1ParPn.Controls.Add(this.orc1F0Cb);
        	this.orc1ParPn.Controls.Add(this.orc1ExcluiParBt);
        	this.orc1ParPn.Controls.Add(this.orc1AddParBt);
        	this.orc1ParPn.Controls.Add(this.orc1SomaParBt);
        	this.orc1ParPn.Controls.Add(this.orc1P0Dtp);
        	this.orc1ParPn.Controls.Add(this.label35);
        	this.orc1ParPn.Controls.Add(this.orc1P0Tb);
        	this.orc1ParPn.Controls.Add(this.label18);
        	this.orc1ParPn.Controls.Add(this.orc1ParIdLb);
        	this.orc1ParPn.Controls.Add(this.label34);
        	this.orc1ParPn.Location = new System.Drawing.Point(9, 25);
        	this.orc1ParPn.Name = "orc1ParPn";
        	this.orc1ParPn.Size = new System.Drawing.Size(421, 90);
        	this.orc1ParPn.TabIndex = 6;
        	// 
        	// orc1F0Cb
        	// 
        	this.orc1F0Cb.FormattingEnabled = true;
        	this.orc1F0Cb.Items.AddRange(new object[] {
			"Pendente",
			"Dinheiro",
			"Cheque",
			"Cartão",
			"Depósito",
			"Outros"});
        	this.orc1F0Cb.Location = new System.Drawing.Point(281, 23);
        	this.orc1F0Cb.Name = "orc1F0Cb";
        	this.orc1F0Cb.Size = new System.Drawing.Size(95, 28);
        	this.orc1F0Cb.TabIndex = 7;
        	this.orc1F0Cb.Text = "Pendente";
        	// 
        	// orc1ExcluiParBt
        	// 
        	this.orc1ExcluiParBt.Location = new System.Drawing.Point(378, 23);
        	this.orc1ExcluiParBt.Name = "orc1ExcluiParBt";
        	this.orc1ExcluiParBt.Size = new System.Drawing.Size(38, 60);
        	this.orc1ExcluiParBt.TabIndex = 8;
        	this.orc1ExcluiParBt.Text = "-";
        	this.orc1ExcluiParBt.UseVisualStyleBackColor = true;
        	this.orc1ExcluiParBt.Click += new System.EventHandler(this.Orc1ExcluiParBtClick);
        	// 
        	// orc1AddParBt
        	// 
        	this.orc1AddParBt.Location = new System.Drawing.Point(42, 54);
        	this.orc1AddParBt.Name = "orc1AddParBt";
        	this.orc1AddParBt.Size = new System.Drawing.Size(334, 29);
        	this.orc1AddParBt.TabIndex = 6;
        	this.orc1AddParBt.Text = "Salvar";
        	this.orc1AddParBt.UseVisualStyleBackColor = true;
        	this.orc1AddParBt.Click += new System.EventHandler(this.Orc1AddParBtClick);
        	// 
        	// orc1SomaParBt
        	// 
        	this.orc1SomaParBt.Location = new System.Drawing.Point(5, 25);
        	this.orc1SomaParBt.Name = "orc1SomaParBt";
        	this.orc1SomaParBt.Size = new System.Drawing.Size(31, 58);
        	this.orc1SomaParBt.TabIndex = 8;
        	this.orc1SomaParBt.Text = "+";
        	this.orc1SomaParBt.UseVisualStyleBackColor = true;
        	this.orc1SomaParBt.Click += new System.EventHandler(this.Orc1SomaParBtClick);
        	// 
        	// orc1P0Dtp
        	// 
        	this.orc1P0Dtp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        	this.orc1P0Dtp.Location = new System.Drawing.Point(153, 25);
        	this.orc1P0Dtp.Name = "orc1P0Dtp";
        	this.orc1P0Dtp.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
        	this.orc1P0Dtp.Size = new System.Drawing.Size(122, 26);
        	this.orc1P0Dtp.TabIndex = 1;
        	// 
        	// label35
        	// 
        	this.label35.AutoSize = true;
        	this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label35.Location = new System.Drawing.Point(151, 10);
        	this.label35.Name = "label35";
        	this.label35.Size = new System.Drawing.Size(56, 15);
        	this.label35.TabIndex = 3;
        	this.label35.Text = "Restante";
        	// 
        	// orc1P0Tb
        	// 
        	this.orc1P0Tb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.orc1P0Tb.Location = new System.Drawing.Point(42, 25);
        	this.orc1P0Tb.Name = "orc1P0Tb";
        	this.orc1P0Tb.Size = new System.Drawing.Size(107, 26);
        	this.orc1P0Tb.TabIndex = 0;
        	this.orc1P0Tb.Text = "0,00";
        	this.orc1P0Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	this.orc1P0Tb.TextChanged += new System.EventHandler(this.orc1PTb_TextChanged);
        	this.orc1P0Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.orc1P0Tb_KeyDown);
        	// 
        	// label18
        	// 
        	this.label18.AutoSize = true;
        	this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label18.Location = new System.Drawing.Point(277, 7);
        	this.label18.Name = "label18";
        	this.label18.Size = new System.Drawing.Size(94, 15);
        	this.label18.TabIndex = 3;
        	this.label18.Text = "Formas de Pag.";
        	// 
        	// orc1ParIdLb
        	// 
        	this.orc1ParIdLb.AutoSize = true;
        	this.orc1ParIdLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orc1ParIdLb.Location = new System.Drawing.Point(40, 0);
        	this.orc1ParIdLb.Name = "orc1ParIdLb";
        	this.orc1ParIdLb.Size = new System.Drawing.Size(18, 15);
        	this.orc1ParIdLb.TabIndex = 3;
        	this.orc1ParIdLb.Text = "-1";
        	this.orc1ParIdLb.Visible = false;
        	// 
        	// label34
        	// 
        	this.label34.AutoSize = true;
        	this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label34.Location = new System.Drawing.Point(39, 10);
        	this.label34.Name = "label34";
        	this.label34.Size = new System.Drawing.Size(35, 15);
        	this.label34.TabIndex = 3;
        	this.label34.Text = "Valor";
        	// 
        	// label23
        	// 
        	this.label23.AutoSize = true;
        	this.label23.Location = new System.Drawing.Point(5, 1);
        	this.label23.Name = "label23";
        	this.label23.Size = new System.Drawing.Size(91, 20);
        	this.label23.TabIndex = 3;
        	this.label23.Text = "Pagamento";
        	// 
        	// orc1RestaLb
        	// 
        	this.orc1RestaLb.AutoSize = true;
        	this.orc1RestaLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orc1RestaLb.ForeColor = System.Drawing.Color.Black;
        	this.orc1RestaLb.Location = new System.Drawing.Point(265, 392);
        	this.orc1RestaLb.Name = "orc1RestaLb";
        	this.orc1RestaLb.Size = new System.Drawing.Size(54, 26);
        	this.orc1RestaLb.TabIndex = 3;
        	this.orc1RestaLb.Text = "0,00";
        	// 
        	// label27
        	// 
        	this.label27.AutoSize = true;
        	this.label27.ForeColor = System.Drawing.Color.Black;
        	this.label27.Location = new System.Drawing.Point(135, 395);
        	this.label27.Name = "label27";
        	this.label27.Size = new System.Drawing.Size(164, 20);
        	this.label27.TabIndex = 3;
        	this.label27.Text = "Resta............................";
        	// 
        	// label28
        	// 
        	this.label28.AutoSize = true;
        	this.label28.Location = new System.Drawing.Point(137, 372);
        	this.label28.Name = "label28";
        	this.label28.Size = new System.Drawing.Size(189, 20);
        	this.label28.TabIndex = 3;
        	this.label28.Text = "____________________";
        	// 
        	// panel2
        	// 
        	this.panel2.BackColor = System.Drawing.Color.Silver;
        	this.panel2.Controls.Add(this.orc1ExcluirOrcBt);
        	this.panel2.Controls.Add(this.orc1StatusCb);
        	this.panel2.Controls.Add(this.orc1PrazoDtp);
        	this.panel2.Controls.Add(this.orc1CodigoMtb);
        	this.panel2.Controls.Add(this.panel3);
        	this.panel2.Controls.Add(this.orc1Dg);
        	this.panel2.Controls.Add(this.label20);
        	this.panel2.Controls.Add(this.label31);
        	this.panel2.Controls.Add(this.orc1IdLb);
        	this.panel2.Controls.Add(this.label22333);
        	this.panel2.Controls.Add(this.label22);
        	this.panel2.Controls.Add(this.label30);
        	this.panel2.Location = new System.Drawing.Point(14, 161);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(677, 434);
        	this.panel2.TabIndex = 8;
        	// 
        	// orc1ExcluirOrcBt
        	// 
        	this.orc1ExcluirOrcBt.BackColor = System.Drawing.Color.Red;
        	this.orc1ExcluirOrcBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orc1ExcluirOrcBt.Location = new System.Drawing.Point(601, 411);
        	this.orc1ExcluirOrcBt.Name = "orc1ExcluirOrcBt";
        	this.orc1ExcluirOrcBt.Size = new System.Drawing.Size(75, 22);
        	this.orc1ExcluirOrcBt.TabIndex = 8;
        	this.orc1ExcluirOrcBt.Text = "Excluir Orçamento";
        	this.orc1ExcluirOrcBt.UseVisualStyleBackColor = false;
        	this.orc1ExcluirOrcBt.Click += new System.EventHandler(this.Orc1ExcluirOrcBrClick);
        	// 
        	// orc1StatusCb
        	// 
        	this.orc1StatusCb.FormattingEnabled = true;
        	this.orc1StatusCb.Items.AddRange(new object[] {
			"Pendente",
			"Aprovado",
			"Em Andamento",
			"Concluído"});
        	this.orc1StatusCb.Location = new System.Drawing.Point(117, 46);
        	this.orc1StatusCb.Name = "orc1StatusCb";
        	this.orc1StatusCb.Size = new System.Drawing.Size(125, 28);
        	this.orc1StatusCb.TabIndex = 7;
        	this.orc1StatusCb.Text = "Pendente";
        	this.orc1StatusCb.SelectedIndexChanged += new System.EventHandler(this.Orc1StatusCbSelectedIndexChanged);
        	// 
        	// orc1PrazoDtp
        	// 
        	this.orc1PrazoDtp.CustomFormat = "Para dddd, dd/MM/yyyy   à\'s\' HH:mm";
        	this.orc1PrazoDtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.orc1PrazoDtp.Location = new System.Drawing.Point(309, 24);
        	this.orc1PrazoDtp.Name = "orc1PrazoDtp";
        	this.orc1PrazoDtp.Size = new System.Drawing.Size(355, 26);
        	this.orc1PrazoDtp.TabIndex = 1;
        	this.orc1PrazoDtp.ValueChanged += new System.EventHandler(this.Orc1PrazoDtpValueChanged);
        	// 
        	// orc1CodigoMtb
        	// 
        	this.orc1CodigoMtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orc1CodigoMtb.Location = new System.Drawing.Point(10, 47);
        	this.orc1CodigoMtb.Mask = "000000";
        	this.orc1CodigoMtb.Name = "orc1CodigoMtb";
        	this.orc1CodigoMtb.Size = new System.Drawing.Size(91, 32);
        	this.orc1CodigoMtb.TabIndex = 0;
        	this.orc1CodigoMtb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	this.orc1CodigoMtb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.orc1CodigoMtb_KeyDown);
        	// 
        	// panel3
        	// 
        	this.panel3.BackColor = System.Drawing.Color.DarkGray;
        	this.panel3.Controls.Add(this.label26);
        	this.panel3.Controls.Add(this.label25);
        	this.panel3.Controls.Add(this.orc1AddIndexLb);
        	this.panel3.Controls.Add(this.orc1AddDescTb);
        	this.panel3.Controls.Add(this.orc1AddValorTb);
        	this.panel3.Location = new System.Drawing.Point(6, 80);
        	this.panel3.Name = "panel3";
        	this.panel3.Size = new System.Drawing.Size(658, 43);
        	this.panel3.TabIndex = 5;
        	// 
        	// label26
        	// 
        	this.label26.AutoSize = true;
        	this.label26.BackColor = System.Drawing.Color.DarkGray;
        	this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label26.Location = new System.Drawing.Point(391, 2);
        	this.label26.Name = "label26";
        	this.label26.Size = new System.Drawing.Size(31, 13);
        	this.label26.TabIndex = 3;
        	this.label26.Text = "Valor";
        	// 
        	// label25
        	// 
        	this.label25.AutoSize = true;
        	this.label25.BackColor = System.Drawing.Color.DarkGray;
        	this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label25.Location = new System.Drawing.Point(3, 2);
        	this.label25.Name = "label25";
        	this.label25.Size = new System.Drawing.Size(55, 13);
        	this.label25.TabIndex = 3;
        	this.label25.Text = "Descrição";
        	// 
        	// orc1AddIndexLb
        	// 
        	this.orc1AddIndexLb.AutoSize = true;
        	this.orc1AddIndexLb.BackColor = System.Drawing.Color.DarkGray;
        	this.orc1AddIndexLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orc1AddIndexLb.Location = new System.Drawing.Point(72, 1);
        	this.orc1AddIndexLb.Name = "orc1AddIndexLb";
        	this.orc1AddIndexLb.Size = new System.Drawing.Size(16, 13);
        	this.orc1AddIndexLb.TabIndex = 3;
        	this.orc1AddIndexLb.Text = "-1";
        	this.orc1AddIndexLb.Visible = false;
        	// 
        	// orc1AddDescTb
        	// 
        	this.orc1AddDescTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.orc1AddDescTb.Location = new System.Drawing.Point(4, 15);
        	this.orc1AddDescTb.Name = "orc1AddDescTb";
        	this.orc1AddDescTb.Size = new System.Drawing.Size(388, 26);
        	this.orc1AddDescTb.TabIndex = 0;
        	this.orc1AddDescTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.orcAddDescTb_KeyDown);
        	// 
        	// orc1AddValorTb
        	// 
        	this.orc1AddValorTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.orc1AddValorTb.Location = new System.Drawing.Point(391, 15);
        	this.orc1AddValorTb.Name = "orc1AddValorTb";
        	this.orc1AddValorTb.Size = new System.Drawing.Size(228, 26);
        	this.orc1AddValorTb.TabIndex = 0;
        	this.orc1AddValorTb.TextChanged += new System.EventHandler(this.orc1AddValorTb_TextChanged);
        	this.orc1AddValorTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.orcAddValorTb_KeyDown);
        	// 
        	// orc1Dg
        	// 
        	this.orc1Dg.AllowUserToAddRows = false;
        	this.orc1Dg.AllowUserToDeleteRows = false;
        	this.orc1Dg.AllowUserToResizeColumns = false;
        	this.orc1Dg.AllowUserToResizeRows = false;
        	this.orc1Dg.BackgroundColor = System.Drawing.Color.Silver;
        	this.orc1Dg.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.orc1Dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.orc1Dg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.id,
			this.desc,
			this.valor});
        	dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
        	dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
        	dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle1.Format = "C2";
        	dataGridViewCellStyle1.NullValue = null;
        	dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        	dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        	dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        	this.orc1Dg.DefaultCellStyle = dataGridViewCellStyle1;
        	this.orc1Dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
        	this.orc1Dg.Location = new System.Drawing.Point(6, 149);
        	this.orc1Dg.MultiSelect = false;
        	this.orc1Dg.Name = "orc1Dg";
        	this.orc1Dg.RowHeadersVisible = false;
        	this.orc1Dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        	this.orc1Dg.ShowCellErrors = false;
        	this.orc1Dg.Size = new System.Drawing.Size(658, 241);
        	this.orc1Dg.TabIndex = 2;
        	this.orc1Dg.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.orcDg_CellClick);
        	// 
        	// id
        	// 
        	this.id.HeaderText = "ID";
        	this.id.Name = "id";
        	this.id.Width = 5;
        	// 
        	// desc
        	// 
        	this.desc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
        	this.desc.HeaderText = "Descrição";
        	this.desc.Name = "desc";
        	this.desc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	// 
        	// valor
        	// 
        	this.valor.HeaderText = "R$";
        	this.valor.Name = "valor";
        	this.valor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	this.valor.Width = 227;
        	// 
        	// label20
        	// 
        	this.label20.AutoSize = true;
        	this.label20.Location = new System.Drawing.Point(3, 126);
        	this.label20.Name = "label20";
        	this.label20.Size = new System.Drawing.Size(158, 20);
        	this.label20.TabIndex = 3;
        	this.label20.Text = "Descrição do Serviço";
        	// 
        	// label31
        	// 
        	this.label31.AutoSize = true;
        	this.label31.Location = new System.Drawing.Point(305, 5);
        	this.label31.Name = "label31";
        	this.label31.Size = new System.Drawing.Size(50, 20);
        	this.label31.TabIndex = 3;
        	this.label31.Text = "Prazo";
        	// 
        	// orc1IdLb
        	// 
        	this.orc1IdLb.AutoSize = true;
        	this.orc1IdLb.Location = new System.Drawing.Point(47, 7);
        	this.orc1IdLb.Name = "orc1IdLb";
        	this.orc1IdLb.Size = new System.Drawing.Size(18, 20);
        	this.orc1IdLb.TabIndex = 3;
        	this.orc1IdLb.Text = "0";
        	this.orc1IdLb.TextChanged += new System.EventHandler(this.orc1IdLb_TextChanged);
        	// 
        	// label22333
        	// 
        	this.label22333.AutoSize = true;
        	this.label22333.Location = new System.Drawing.Point(6, 7);
        	this.label22333.Name = "label22333";
        	this.label22333.Size = new System.Drawing.Size(57, 20);
        	this.label22333.TabIndex = 3;
        	this.label22333.Text = "id.........";
        	// 
        	// label22
        	// 
        	this.label22.AutoSize = true;
        	this.label22.Location = new System.Drawing.Point(117, 25);
        	this.label22.Name = "label22";
        	this.label22.Size = new System.Drawing.Size(56, 20);
        	this.label22.TabIndex = 3;
        	this.label22.Text = "Status";
        	// 
        	// label30
        	// 
        	this.label30.AutoSize = true;
        	this.label30.Location = new System.Drawing.Point(6, 27);
        	this.label30.Name = "label30";
        	this.label30.Size = new System.Drawing.Size(59, 20);
        	this.label30.TabIndex = 3;
        	this.label30.Text = "Codigo";
        	// 
        	// panel1
        	// 
        	this.panel1.BackColor = System.Drawing.Color.Lavender;
        	this.panel1.Controls.Add(this.orc1EndCb);
        	this.panel1.Controls.Add(this.orc1CpfMtb);
        	this.panel1.Controls.Add(this.orc1Lst);
        	this.panel1.Controls.Add(this.label24);
        	this.panel1.Controls.Add(this.label17);
        	this.panel1.Controls.Add(this.orc1CcliLb);
        	this.panel1.Controls.Add(this.orc1NomeTb);
        	this.panel1.Controls.Add(this.orc1CpfLb);
        	this.panel1.Controls.Add(this.label19);
        	this.panel1.Location = new System.Drawing.Point(14, 41);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(1121, 114);
        	this.panel1.TabIndex = 7;
        	// 
        	// orc1EndCb
        	// 
        	this.orc1EndCb.FormattingEnabled = true;
        	this.orc1EndCb.Location = new System.Drawing.Point(14, 76);
        	this.orc1EndCb.Name = "orc1EndCb";
        	this.orc1EndCb.Size = new System.Drawing.Size(681, 28);
        	this.orc1EndCb.TabIndex = 8;
        	this.orc1EndCb.SelectedIndexChanged += new System.EventHandler(this.Orc1EndCbSelectedIndexChanged);
        	// 
        	// orc1CpfMtb
        	// 
        	this.orc1CpfMtb.Location = new System.Drawing.Point(535, 28);
        	this.orc1CpfMtb.Mask = "000,000,000-00";
        	this.orc1CpfMtb.Name = "orc1CpfMtb";
        	this.orc1CpfMtb.Size = new System.Drawing.Size(160, 26);
        	this.orc1CpfMtb.TabIndex = 6;
        	this.orc1CpfMtb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	this.orc1CpfMtb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Orc1CpfMtbKeyDown);
        	// 
        	// orc1Lst
        	// 
        	this.orc1Lst.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orc1Lst.FormattingEnabled = true;
        	this.orc1Lst.Items.AddRange(new object[] {
			"1",
			"2",
			"3"});
        	this.orc1Lst.Location = new System.Drawing.Point(703, 21);
        	this.orc1Lst.Name = "orc1Lst";
        	this.orc1Lst.Size = new System.Drawing.Size(410, 82);
        	this.orc1Lst.TabIndex = 5;
        	this.orc1Lst.Visible = false;
        	this.orc1Lst.SelectedIndexChanged += new System.EventHandler(this.orc1Lst_SelectedIndexChanged);
        	// 
        	// label24
        	// 
        	this.label24.AutoSize = true;
        	this.label24.BackColor = System.Drawing.Color.Transparent;
        	this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label24.Location = new System.Drawing.Point(703, 5);
        	this.label24.Name = "label24";
        	this.label24.Size = new System.Drawing.Size(105, 13);
        	this.label24.TabIndex = 3;
        	this.label24.Text = "Histórico de serviços";
        	// 
        	// label17
        	// 
        	this.label17.AutoSize = true;
        	this.label17.Location = new System.Drawing.Point(10, 5);
        	this.label17.Name = "label17";
        	this.label17.Size = new System.Drawing.Size(58, 20);
        	this.label17.TabIndex = 3;
        	this.label17.Text = "Cliente";
        	// 
        	// orc1CcliLb
        	// 
        	this.orc1CcliLb.AutoSize = true;
        	this.orc1CcliLb.Location = new System.Drawing.Point(65, 5);
        	this.orc1CcliLb.Name = "orc1CcliLb";
        	this.orc1CcliLb.Size = new System.Drawing.Size(18, 20);
        	this.orc1CcliLb.TabIndex = 3;
        	this.orc1CcliLb.Text = "0";
        	this.orc1CcliLb.TextChanged += new System.EventHandler(this.orc1CcliLb_TextChanged);
        	// 
        	// orc1NomeTb
        	// 
        	this.orc1NomeTb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
        	this.orc1NomeTb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
        	this.orc1NomeTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.orc1NomeTb.Location = new System.Drawing.Point(14, 28);
        	this.orc1NomeTb.Name = "orc1NomeTb";
        	this.orc1NomeTb.Size = new System.Drawing.Size(491, 26);
        	this.orc1NomeTb.TabIndex = 1;
        	this.orc1NomeTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.orc1NomeTb_KeyDown);
        	// 
        	// orc1CpfLb
        	// 
        	this.orc1CpfLb.Location = new System.Drawing.Point(531, 8);
        	this.orc1CpfLb.Name = "orc1CpfLb";
        	this.orc1CpfLb.Size = new System.Drawing.Size(40, 20);
        	this.orc1CpfLb.TabIndex = 3;
        	this.orc1CpfLb.Text = "CPF";
        	// 
        	// label19
        	// 
        	this.label19.AutoSize = true;
        	this.label19.Location = new System.Drawing.Point(14, 56);
        	this.label19.Name = "label19";
        	this.label19.Size = new System.Drawing.Size(78, 20);
        	this.label19.TabIndex = 3;
        	this.label19.Text = "Endereço";
        	// 
        	// orc1SalvarBt
        	// 
        	this.orc1SalvarBt.Location = new System.Drawing.Point(6, 601);
        	this.orc1SalvarBt.Name = "orc1SalvarBt";
        	this.orc1SalvarBt.Size = new System.Drawing.Size(1134, 43);
        	this.orc1SalvarBt.TabIndex = 5;
        	this.orc1SalvarBt.Text = "Salvar";
        	this.orc1SalvarBt.UseVisualStyleBackColor = true;
        	this.orc1SalvarBt.Click += new System.EventHandler(this.orc1SalvarBt_Click);
        	// 
        	// orcXbt
        	// 
        	this.orcXbt.BackColor = System.Drawing.Color.Firebrick;
        	this.orcXbt.Location = new System.Drawing.Point(1110, 11);
        	this.orcXbt.Name = "orcXbt";
        	this.orcXbt.Size = new System.Drawing.Size(30, 28);
        	this.orcXbt.TabIndex = 1;
        	this.orcXbt.Text = "X";
        	this.orcXbt.UseVisualStyleBackColor = false;
        	this.orcXbt.Click += new System.EventHandler(this.orcXbt_Click);
        	// 
        	// orcLBt
        	// 
        	this.orcLBt.BackColor = System.Drawing.Color.SlateGray;
        	this.orcLBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.orcLBt.Location = new System.Drawing.Point(1074, 11);
        	this.orcLBt.Name = "orcLBt";
        	this.orcLBt.Size = new System.Drawing.Size(30, 28);
        	this.orcLBt.TabIndex = 0;
        	this.orcLBt.Text = "L";
        	this.orcLBt.UseVisualStyleBackColor = false;
        	this.orcLBt.Click += new System.EventHandler(this.orcLBt_Click);
        	// 
        	// rel1Gb
        	// 
        	this.rel1Gb.Controls.Add(this.rel1AteDtp);
        	this.rel1Gb.Controls.Add(this.rel1DeDtp);
        	this.rel1Gb.Controls.Add(this.rel1ImprimeBt);
        	this.rel1Gb.Controls.Add(this.rel1RelataBt);
        	this.rel1Gb.Controls.Add(this.rel1Tb);
        	this.rel1Gb.Controls.Add(this.rel1XBt);
        	this.rel1Gb.Controls.Add(this.rel1LBt);
        	this.rel1Gb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.rel1Gb.Location = new System.Drawing.Point(164, 32);
        	this.rel1Gb.Name = "rel1Gb";
        	this.rel1Gb.Size = new System.Drawing.Size(1089, 475);
        	this.rel1Gb.TabIndex = 10;
        	this.rel1Gb.TabStop = false;
        	this.rel1Gb.Text = "Relatório";
        	this.rel1Gb.Visible = false;
        	// 
        	// rel1AteDtp
        	// 
        	this.rel1AteDtp.CustomFormat = "ATÉ  dd/M/yyyy";
        	this.rel1AteDtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.rel1AteDtp.Location = new System.Drawing.Point(234, 67);
        	this.rel1AteDtp.Name = "rel1AteDtp";
        	this.rel1AteDtp.Size = new System.Drawing.Size(165, 26);
        	this.rel1AteDtp.TabIndex = 2;
        	// 
        	// rel1DeDtp
        	// 
        	this.rel1DeDtp.CustomFormat = "DE  dd/M/yyyy";
        	this.rel1DeDtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.rel1DeDtp.Location = new System.Drawing.Point(45, 66);
        	this.rel1DeDtp.Name = "rel1DeDtp";
        	this.rel1DeDtp.Size = new System.Drawing.Size(163, 26);
        	this.rel1DeDtp.TabIndex = 2;
        	// 
        	// rel1ImprimeBt
        	// 
        	this.rel1ImprimeBt.Location = new System.Drawing.Point(479, 442);
        	this.rel1ImprimeBt.Name = "rel1ImprimeBt";
        	this.rel1ImprimeBt.Size = new System.Drawing.Size(117, 27);
        	this.rel1ImprimeBt.TabIndex = 1;
        	this.rel1ImprimeBt.Text = "Imprimir";
        	this.rel1ImprimeBt.UseVisualStyleBackColor = true;
        	// 
        	// rel1RelataBt
        	// 
        	this.rel1RelataBt.Location = new System.Drawing.Point(41, 358);
        	this.rel1RelataBt.Name = "rel1RelataBt";
        	this.rel1RelataBt.Size = new System.Drawing.Size(358, 36);
        	this.rel1RelataBt.TabIndex = 1;
        	this.rel1RelataBt.Text = "relatar";
        	this.rel1RelataBt.UseVisualStyleBackColor = true;
        	// 
        	// rel1Tb
        	// 
        	this.rel1Tb.Location = new System.Drawing.Point(479, 48);
        	this.rel1Tb.Multiline = true;
        	this.rel1Tb.Name = "rel1Tb";
        	this.rel1Tb.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        	this.rel1Tb.Size = new System.Drawing.Size(569, 391);
        	this.rel1Tb.TabIndex = 0;
        	// 
        	// rel1XBt
        	// 
        	this.rel1XBt.BackColor = System.Drawing.Color.Firebrick;
        	this.rel1XBt.Location = new System.Drawing.Point(1053, 14);
        	this.rel1XBt.Name = "rel1XBt";
        	this.rel1XBt.Size = new System.Drawing.Size(30, 28);
        	this.rel1XBt.TabIndex = 1;
        	this.rel1XBt.Text = "X";
        	this.rel1XBt.UseVisualStyleBackColor = false;
        	// 
        	// rel1LBt
        	// 
        	this.rel1LBt.BackColor = System.Drawing.Color.SlateGray;
        	this.rel1LBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.rel1LBt.Location = new System.Drawing.Point(1017, 14);
        	this.rel1LBt.Name = "rel1LBt";
        	this.rel1LBt.Size = new System.Drawing.Size(30, 28);
        	this.rel1LBt.TabIndex = 1;
        	this.rel1LBt.Text = "L";
        	this.rel1LBt.UseVisualStyleBackColor = false;
        	// 
        	// pesGb
        	// 
        	this.pesGb.Controls.Add(this.pes1Lst);
        	this.pesGb.Controls.Add(this.button1);
        	this.pesGb.Controls.Add(this.pes1CliTb);
        	this.pesGb.Controls.Add(this.button2);
        	this.pesGb.Controls.Add(this.button3);
        	this.pesGb.Controls.Add(this.label33);
        	this.pesGb.Controls.Add(this.label4);
        	this.pesGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.pesGb.Location = new System.Drawing.Point(123, 32);
        	this.pesGb.Name = "pesGb";
        	this.pesGb.Size = new System.Drawing.Size(938, 569);
        	this.pesGb.TabIndex = 11;
        	this.pesGb.TabStop = false;
        	this.pesGb.Text = "pesquisa";
        	this.pesGb.Visible = false;
        	// 
        	// pes1Lst
        	// 
        	this.pes1Lst.FormattingEnabled = true;
        	this.pes1Lst.ItemHeight = 20;
        	this.pes1Lst.Location = new System.Drawing.Point(172, 173);
        	this.pes1Lst.Name = "pes1Lst";
        	this.pes1Lst.Size = new System.Drawing.Size(460, 284);
        	this.pes1Lst.TabIndex = 19;
        	this.pes1Lst.SelectedIndexChanged += new System.EventHandler(this.pes1Lst_SelectedIndexChanged);
        	// 
        	// button1
        	// 
        	this.button1.Location = new System.Drawing.Point(754, 32);
        	this.button1.Name = "button1";
        	this.button1.Size = new System.Drawing.Size(66, 30);
        	this.button1.TabIndex = 0;
        	this.button1.Text = "Converter DB";
        	this.button1.UseVisualStyleBackColor = true;
        	this.button1.Visible = false;
        	this.button1.Click += new System.EventHandler(this.button1_Click_1);
        	// 
        	// pes1CliTb
        	// 
        	this.pes1CliTb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
        	this.pes1CliTb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
        	this.pes1CliTb.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.pes1CliTb.Location = new System.Drawing.Point(172, 95);
        	this.pes1CliTb.Name = "pes1CliTb";
        	this.pes1CliTb.Size = new System.Drawing.Size(467, 26);
        	this.pes1CliTb.TabIndex = 18;
        	this.pes1CliTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pes1CliTb_KeyDown);
        	// 
        	// button2
        	// 
        	this.button2.BackColor = System.Drawing.Color.Firebrick;
        	this.button2.Location = new System.Drawing.Point(893, 19);
        	this.button2.Name = "button2";
        	this.button2.Size = new System.Drawing.Size(30, 28);
        	this.button2.TabIndex = 17;
        	this.button2.Text = "X";
        	this.button2.UseVisualStyleBackColor = false;
        	// 
        	// button3
        	// 
        	this.button3.BackColor = System.Drawing.Color.SlateGray;
        	this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.button3.Location = new System.Drawing.Point(857, 19);
        	this.button3.Name = "button3";
        	this.button3.Size = new System.Drawing.Size(30, 28);
        	this.button3.TabIndex = 16;
        	this.button3.Text = "L";
        	this.button3.UseVisualStyleBackColor = false;
        	// 
        	// label33
        	// 
        	this.label33.AutoSize = true;
        	this.label33.Location = new System.Drawing.Point(168, 153);
        	this.label33.Name = "label33";
        	this.label33.Size = new System.Drawing.Size(82, 20);
        	this.label33.TabIndex = 4;
        	this.label33.Text = "Resultado";
        	// 
        	// label4
        	// 
        	this.label4.AutoSize = true;
        	this.label4.Location = new System.Drawing.Point(168, 72);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(51, 20);
        	this.label4.TabIndex = 4;
        	this.label4.Text = "Nome";
        	// 
        	// rel2Gb
        	// 
        	this.rel2Gb.Controls.Add(this.groupBox1);
        	this.rel2Gb.Controls.Add(this.rel2XBt);
        	this.rel2Gb.Controls.Add(this.rel2Dg);
        	this.rel2Gb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.rel2Gb.Location = new System.Drawing.Point(12, 30);
        	this.rel2Gb.Name = "rel2Gb";
        	this.rel2Gb.Size = new System.Drawing.Size(1090, 571);
        	this.rel2Gb.TabIndex = 10;
        	this.rel2Gb.TabStop = false;
        	this.rel2Gb.Visible = false;
        	this.rel2Gb.VisibleChanged += new System.EventHandler(this.Rel2GbVisibleChanged);
        	// 
        	// groupBox1
        	// 
        	this.groupBox1.Controls.Add(this.groupBox2);
        	this.groupBox1.Location = new System.Drawing.Point(14, 19);
        	this.groupBox1.Name = "groupBox1";
        	this.groupBox1.Size = new System.Drawing.Size(222, 123);
        	this.groupBox1.TabIndex = 2;
        	this.groupBox1.TabStop = false;
        	this.groupBox1.Text = "Filtro";
        	// 
        	// groupBox2
        	// 
        	this.groupBox2.Controls.Add(this.rel1StatusPendenteCh);
        	this.groupBox2.Location = new System.Drawing.Point(17, 26);
        	this.groupBox2.Name = "groupBox2";
        	this.groupBox2.Size = new System.Drawing.Size(188, 78);
        	this.groupBox2.TabIndex = 5;
        	this.groupBox2.TabStop = false;
        	this.groupBox2.Text = "Status";
        	// 
        	// rel1StatusPendenteCh
        	// 
        	this.rel1StatusPendenteCh.Location = new System.Drawing.Point(19, 31);
        	this.rel1StatusPendenteCh.Name = "rel1StatusPendenteCh";
        	this.rel1StatusPendenteCh.Size = new System.Drawing.Size(143, 24);
        	this.rel1StatusPendenteCh.TabIndex = 0;
        	this.rel1StatusPendenteCh.Text = "Mostrar Todos";
        	this.rel1StatusPendenteCh.UseVisualStyleBackColor = true;
        	this.rel1StatusPendenteCh.CheckedChanged += new System.EventHandler(this.Rel1StatusPendenteChCheckedChanged);
        	// 
        	// rel2XBt
        	// 
        	this.rel2XBt.BackColor = System.Drawing.Color.Firebrick;
        	this.rel2XBt.Location = new System.Drawing.Point(1047, 16);
        	this.rel2XBt.Name = "rel2XBt";
        	this.rel2XBt.Size = new System.Drawing.Size(30, 28);
        	this.rel2XBt.TabIndex = 1;
        	this.rel2XBt.Text = "X";
        	this.rel2XBt.UseVisualStyleBackColor = false;
        	this.rel2XBt.Click += new System.EventHandler(this.Rel2XBtClick);
        	// 
        	// rel2Dg
        	// 
        	this.rel2Dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.rel2Dg.Location = new System.Drawing.Point(11, 155);
        	this.rel2Dg.Name = "rel2Dg";
        	this.rel2Dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        	this.rel2Dg.Size = new System.Drawing.Size(1051, 397);
        	this.rel2Dg.TabIndex = 2;
        	this.rel2Dg.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Rel2DgCellClick);
        	// 
        	// bkpGb
        	// 
        	this.bkpGb.Controls.Add(this.bkpInstrucoesTb);
        	this.bkpGb.Controls.Add(this.bkpOnCloseCh);
        	this.bkpGb.Controls.Add(this.bkpAlterarBt);
        	this.bkpGb.Controls.Add(this.bkpPachLb);
        	this.bkpGb.Controls.Add(this.label21);
        	this.bkpGb.Controls.Add(this.bkpXBt);
        	this.bkpGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.bkpGb.Location = new System.Drawing.Point(459, 35);
        	this.bkpGb.Name = "bkpGb";
        	this.bkpGb.Size = new System.Drawing.Size(375, 410);
        	this.bkpGb.TabIndex = 12;
        	this.bkpGb.TabStop = false;
        	this.bkpGb.Text = "Configurações de Backup";
        	this.bkpGb.Visible = false;
        	// 
        	// bkpInstrucoesTb
        	// 
        	this.bkpInstrucoesTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.bkpInstrucoesTb.Location = new System.Drawing.Point(33, 231);
        	this.bkpInstrucoesTb.Multiline = true;
        	this.bkpInstrucoesTb.Name = "bkpInstrucoesTb";
        	this.bkpInstrucoesTb.Size = new System.Drawing.Size(274, 153);
        	this.bkpInstrucoesTb.TabIndex = 6;
        	this.bkpInstrucoesTb.Text = resources.GetString("bkpInstrucoesTb.Text");
        	// 
        	// bkpOnCloseCh
        	// 
        	this.bkpOnCloseCh.Location = new System.Drawing.Point(36, 45);
        	this.bkpOnCloseCh.Name = "bkpOnCloseCh";
        	this.bkpOnCloseCh.Size = new System.Drawing.Size(271, 24);
        	this.bkpOnCloseCh.TabIndex = 5;
        	this.bkpOnCloseCh.Text = "Fazer backup ao fechar aplicativo";
        	this.bkpOnCloseCh.UseVisualStyleBackColor = true;
        	// 
        	// bkpAlterarBt
        	// 
        	this.bkpAlterarBt.Location = new System.Drawing.Point(36, 157);
        	this.bkpAlterarBt.Name = "bkpAlterarBt";
        	this.bkpAlterarBt.Size = new System.Drawing.Size(271, 31);
        	this.bkpAlterarBt.TabIndex = 4;
        	this.bkpAlterarBt.Text = "Alterar Destino";
        	this.bkpAlterarBt.UseVisualStyleBackColor = true;
        	this.bkpAlterarBt.Click += new System.EventHandler(this.BkpAlterarBtClick);
        	// 
        	// bkpPachLb
        	// 
        	this.bkpPachLb.ForeColor = System.Drawing.Color.DarkGreen;
        	this.bkpPachLb.Location = new System.Drawing.Point(33, 112);
        	this.bkpPachLb.Name = "bkpPachLb";
        	this.bkpPachLb.Size = new System.Drawing.Size(451, 23);
        	this.bkpPachLb.TabIndex = 3;
        	this.bkpPachLb.Text = "C:";
        	// 
        	// label21
        	// 
        	this.label21.Location = new System.Drawing.Point(33, 88);
        	this.label21.Name = "label21";
        	this.label21.Size = new System.Drawing.Size(173, 23);
        	this.label21.TabIndex = 3;
        	this.label21.Text = "Backup será salvo em:";
        	// 
        	// bkpXBt
        	// 
        	this.bkpXBt.BackColor = System.Drawing.Color.Firebrick;
        	this.bkpXBt.Location = new System.Drawing.Point(345, 14);
        	this.bkpXBt.Name = "bkpXBt";
        	this.bkpXBt.Size = new System.Drawing.Size(30, 28);
        	this.bkpXBt.TabIndex = 1;
        	this.bkpXBt.Text = "X";
        	this.bkpXBt.UseVisualStyleBackColor = false;
        	// 
        	// MainForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(1203, 718);
        	this.Controls.Add(this.statusStrip1);
        	this.Controls.Add(this.menuStrip1);
        	this.Controls.Add(this.cad1Gb);
        	this.Controls.Add(this.pesGb);
        	this.Controls.Add(this.rel1Gb);
        	this.Controls.Add(this.rel2Gb);
        	this.Controls.Add(this.bkpGb);
        	this.Controls.Add(this.orc1Gb);
        	this.MainMenuStrip = this.menuStrip1;
        	this.Name = "MainForm";
        	this.Text = "Calhas Bafuni";
        	this.menuStrip1.ResumeLayout(false);
        	this.menuStrip1.PerformLayout();
        	this.cad1Gb.ResumeLayout(false);
        	this.cad1Gb.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.cad1Dg)).EndInit();
        	this.cad1EndPn.ResumeLayout(false);
        	this.cad1EndPn.PerformLayout();
        	this.panel6.ResumeLayout(false);
        	this.panel6.PerformLayout();
        	this.statusStrip1.ResumeLayout(false);
        	this.statusStrip1.PerformLayout();
        	this.orc1Gb.ResumeLayout(false);
        	this.panel4.ResumeLayout(false);
        	this.panel4.PerformLayout();
        	this.orc1ParPn.ResumeLayout(false);
        	this.orc1ParPn.PerformLayout();
        	this.panel2.ResumeLayout(false);
        	this.panel2.PerformLayout();
        	this.panel3.ResumeLayout(false);
        	this.panel3.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.orc1Dg)).EndInit();
        	this.panel1.ResumeLayout(false);
        	this.panel1.PerformLayout();
        	this.rel1Gb.ResumeLayout(false);
        	this.rel1Gb.PerformLayout();
        	this.pesGb.ResumeLayout(false);
        	this.pesGb.PerformLayout();
        	this.rel2Gb.ResumeLayout(false);
        	this.groupBox1.ResumeLayout(false);
        	this.groupBox2.ResumeLayout(false);
        	((System.ComponentModel.ISupportInitialize)(this.rel2Dg)).EndInit();
        	this.bkpGb.ResumeLayout(false);
        	this.bkpGb.PerformLayout();
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesquisarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem efetuarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.GroupBox cad1Gb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cad1EnderecoTb;
        private System.Windows.Forms.TextBox cad1NomeTb;
        private System.Windows.Forms.Button cad1SalvarBt;
        private System.Windows.Forms.Button cad1LBt;
        private System.Windows.Forms.Button cad1XBt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox cad1UfTb;
        private System.Windows.Forms.TextBox cad1ObsTb;
        private System.Windows.Forms.TextBox cad1CidadeTb;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox cad1ComplementoTb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox cad1BairroTb;
        private System.Windows.Forms.MaskedTextBox cad1CepMtb;
        private System.Windows.Forms.MaskedTextBox cad1CodigoMtb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label cad1CpfCnpjLb;
        private System.Windows.Forms.TextBox cad1NumeroTb;
        private System.Windows.Forms.TextBox cad1Fone2Tb;
        private System.Windows.Forms.TextBox cad1Fone1Tb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label cad1VaiLb;
        private System.Windows.Forms.Label cad1VemLb;
        private System.Windows.Forms.Label cad1IdLb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox cad1EmailTb;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripMsg;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox cad1CpfTb;
        private System.Windows.Forms.GroupBox orc1Gb;
        private System.Windows.Forms.ToolStripMenuItem orçamentoToolStripMenuItem;
        private System.Windows.Forms.DataGridView orc1Dg;
        private System.Windows.Forms.Button orc1SalvarBt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label orc1CpfLb;
        private System.Windows.Forms.TextBox orc1NomeTb;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label orc1CcliLb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button orcXbt;
        private System.Windows.Forms.Button orcLBt;
        private System.Windows.Forms.TextBox orc1AddValorTb;
        private System.Windows.Forms.TextBox orc1AddDescTb;
        private System.Windows.Forms.Label orc1AddIndexLb;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox orc1Lst;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.MaskedTextBox orc1CodigoMtb;
        private System.Windows.Forms.DateTimePicker orc1PrazoDtp;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox rel1Gb;
        private System.Windows.Forms.Button rel1ImprimeBt;
        private System.Windows.Forms.Button rel1RelataBt;
        private System.Windows.Forms.TextBox rel1Tb;
        private System.Windows.Forms.Button rel1XBt;
        private System.Windows.Forms.Button rel1LBt;
        private System.Windows.Forms.DateTimePicker rel1AteDtp;
        private System.Windows.Forms.DateTimePicker rel1DeDtp;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label orc1IdLb;
        private System.Windows.Forms.Label label22333;
        private System.Windows.Forms.ListBox cad1Lst;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor;
        private System.Windows.Forms.GroupBox pesGb;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox pes1CliTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox pes1Lst;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox orc1CpfMtb;
        private System.Windows.Forms.DataGridView cad1Dg;
        private System.Windows.Forms.Button bkpAlterarBt;
        private System.Windows.Forms.Label bkpPachLb;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ToolStripMenuItem configuraçõesDeBackupToolStripMenuItem;
        private System.Windows.Forms.GroupBox bkpGb;
        private System.Windows.Forms.Button bkpXBt;
        private System.Windows.Forms.ToolStripMenuItem segurançaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel orc1ParPn;
        private System.Windows.Forms.Label orc1TotalLb;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DateTimePicker orc1P0Dtp;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox orc1P0Tb;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label orc1PagouLb;
        private System.Windows.Forms.Label orc1RestaLb;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox orc1StatusCb;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cad1TipoCb;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel cad1EndPn;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox cad1EnderecosCb;
        private System.Windows.Forms.Label cad1EndLb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cad1TipologCb;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button cad1ExcluirBt;
        private System.Windows.Forms.ComboBox orc1EndCb;
        private System.Windows.Forms.Button cad1NewEndBt;
        private System.Windows.Forms.ToolStripMenuItem pendendênciasToolStripMenuItem;
        private System.Windows.Forms.GroupBox rel2Gb;
        private System.Windows.Forms.DataGridView rel2Dg;
        private System.Windows.Forms.Button rel2XBt;
        private System.Windows.Forms.Button orc1AddParBt;
        private System.Windows.Forms.ComboBox orc1F0Cb;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button cad1ExcluirEndBt;
        private System.Windows.Forms.Button cad1EndAtualizarBt;
        private System.Windows.Forms.CheckBox rel1StatusPendenteCh;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox bkpOnCloseCh;
        private System.Windows.Forms.TextBox bkpInstrucoesTb;
        private System.Windows.Forms.ToolStripMenuItem backupRápidoToolStripMenuItem;
        private System.Windows.Forms.ListView orc1ParcelasLv;
        private System.Windows.Forms.ColumnHeader valorr;
        private System.Windows.Forms.ColumnHeader data;
        private System.Windows.Forms.ColumnHeader forma;
        private System.Windows.Forms.Button orc1SomaParBt;
        private System.Windows.Forms.Label orc1ParIdLb;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button orc1ExcluiParBt;
        private System.Windows.Forms.Button cad1AtualizarclienteBt;
        private System.Windows.Forms.Button orc1ExcluirOrcBt;
    }
}

