﻿/*
 * Created by SharpDevelop.
 * User: richa
 * Date: 02/05/2017
 * Time: 16:21
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace BafuniSys
{
	/// <summary>
	/// Description of Exceptions.
	/// </summary>
	public class Exceptions
	{
		public Exceptions()
		{
		}
		
		public int id{get;set;}
		public string className{
			get; set;
		}
		public string Message{get;set;}
	}
}
