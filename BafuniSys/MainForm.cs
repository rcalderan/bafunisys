﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BafuniSys
{
    public partial class MainForm : Form
    {
    	SqLite conexao;

        List<Control> gbs;

        public MainForm()
        {
            InitializeComponent();
            gbs = new List<Control>() { cad1Gb, orc1Gb, rel1Gb,pesGb,rel2Gb,bkpGb };
            conexao = new SqLite();
            conexao.check_tables();

            var editColumn = new System.Windows.Forms.DataGridViewButtonColumn
            {
                Text = "X",
                UseColumnTextForButtonValue = true,
                Name = "Excluir",
                DataPropertyName = "exclui"
            };
            orc1Dg.Columns.Add(editColumn);
            populateAutocomplete();
            Config conf = Config.Load(conexao.GetConnection());
            bkpOnCloseCh.Checked = conf.BackupOnClose;
            bkpPachLb.Text = conf.BackupPath;
            reposicionarGb();
            statusSender("",Control.DefaultBackColor);

        }

        private void mostraGb(Control control)
        {
            foreach(Control gb in gbs)
            {
                if (control == gb)
                    gb.Show();
                else
                    gb.Hide();
                statusSender("", Control.DefaultBackColor);
            }
        }

        private void cad1SalvarBt_Click(object sender, EventArgs e)
        {
            try
            {
                if (cad1NomeTb.Text=="")
                {
                    statusSender("Digite o nome primeiro!");
                    cad1NomeTb.Select();
                    return;
                }
                long id;
                	string obs= "";
                	foreach(string s in cad1ObsTb.Lines)
                		obs+=s+'\n';
                if (!long.TryParse(cad1IdLb.Text, out id))
                    id = 0;
                if (id == 0)
                {
                    string msg = Cliente.New(cad1TipoCb.SelectedIndex,cad1CpfTb.Text,
                        cad1NomeTb.Text,
                        cad1Fone1Tb.Text,
                        cad1Fone2Tb.Text,
                        obs,
                        conexao.GetConnection());
                    if (msg == "")
                    {
                        statusSender("Cadastro inserido com sucesso!", Color.Green);
                        
                        Cliente c = Cliente.GetLast(conexao.GetConnection());
                        //Endereco.New(c.id,cad1TipologCb.Text,cad1CepMtb.Text,cad1EnderecoTb.Text,cad1NumeroTb.Text,cad1BairroTb.Text,cad1CidadeTb.Text,cad1UfTb.Text,cad1ComplementoTb.Text,conexao.GetConnection());
                        limpaService();
                        cad1IdLb.Text = c.id.ToString();
                        cad1EnderecoTb.Select();
                    }
                    else
                        statusSender(msg);
                }
                else
                {//atualizar
                    string msg = Cliente.Update(id,cad1TipoCb.SelectedIndex,cad1CpfTb.Text,
                    cad1NomeTb.Text, cad1EmailTb.Text,
                        cad1Fone1Tb.Text,
                        cad1Fone2Tb.Text,
                        obs,
                        conexao.GetConnection());
                    Cliente cli = Cliente.Load(id, conexao.GetConnection());
                    if (msg!="")
                    {
                        carregaCliente(cli);
                        statusSender(msg);
                    }
                    else
                    {
                        carregaCliente(cli);
                        //mostraGb(orc1Gb);
                        statusSender("Cliente Atualizado!", Control.DefaultBackColor);
                    }
                }
            }
            catch(Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }

        private void calculaValoresServicos()
        {
            try
            {
                float total=0,sub=0, aux;
                string s;
                foreach (DataGridViewRow r in orc1Dg.Rows)
                {
                    s= r.Cells["valor"].Value.ToString();
                    if (float.TryParse(r.Cells["valor"].Value.ToString(), out aux))
                    {
                        total += aux;
                    }
                }

                orc1PagouLb.Text = sub.ToString("F2");
                orc1RestaLb.Text = (total - sub).ToString("F2");
                orc1TotalLb.Text = total.ToString("F2");

            }
            catch(Exception e)
            {

                statusSender("Erro: "+e.Message);
            }
        }

        private void populateAutocomplete()
        {
            try
            {
                string[] names = Cliente.GetAllNames(conexao.GetConnection()).ToArray();
                orc1NomeTb.AutoCompleteCustomSource.Clear();
                orc1NomeTb.AutoCompleteCustomSource.AddRange(names);
                cad1NomeTb.AutoCompleteCustomSource.Clear();
                cad1NomeTb.AutoCompleteCustomSource.AddRange(names);
                pes1CliTb.AutoCompleteCustomSource.Clear();
                pes1CliTb.AutoCompleteCustomSource.AddRange(names);
                
                cad1EnderecoTb.AutoCompleteCustomSource.Clear();
                cad1EnderecoTb.AutoCompleteCustomSource.AddRange(Endereco.allLogradFromCity("SÃO CARLOS"));
                cad1TipologCb.Items.AddRange(Endereco.allTipoLogradFromCity("SÃO CARLOS"));
                
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void limpaCad1()
        {
            try
            {
                cad1IdLb.Text = "0";
                foreach(DataGridViewRow r in cad1Dg.Rows)
                {
                    r.Cells[1].Value = "0,00";
                }
                cad1CodigoMtb.Clear();
                cad1TipoCb.SelectedIndex = 0;
                cad1CpfTb.Clear();
                cad1NomeTb.Clear();
                cad1Fone1Tb.Clear();
                cad1Fone2Tb.Clear();
                cad1EmailTb.Clear();
                cad1ObsTb.Clear();
                
                cad1EndLb.Text="-1";
                cad1EnderecosCb.Text="";
                cad1EnderecosCb.Items.Clear();
                cad1CepMtb.Clear();
                cad1EnderecoTb.Clear();
                cad1NumeroTb.Clear();
                cad1BairroTb.Clear();
                cad1CidadeTb.Clear();
                cad1UfTb.Clear();
                cad1ComplementoTb.Clear();
                cad1Lst.Items.Clear();
                cad1Lst.Hide();
                toolStripMsg.BackColor = Control.DefaultBackColor;
                toolStripMsg.Text = "Status:";
                populateAutocomplete();


                Control[] selecPn = cad1Gb.Controls.Find("selectPn", true);
                if (selecPn.Length!=0)
                {
                    foreach (Control c in selecPn[0].Controls)
                        c.Dispose();
                    selecPn[0].Dispose();
                }
            }
            catch {
            }
        }

        private void cad1LBt_Click(object sender, EventArgs e)
        {
            try
            {
                limpaCad1();
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                mostraGb(cad1Gb);
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1XBt_Click(object sender, EventArgs e)
        {
            try
            {
                cad1Gb.Hide();
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private string carregaCliente( BafuniSys.Cliente cliente)
        {
            try
            {
                if (cliente != null)
                {
                	limpaCad1();
                    cad1CodigoMtb.Text = cliente.id.ToString();
                    cad1NomeTb.Text = cliente.nome;
                    cad1CpfTb.Text = cliente.cpf;
                    cad1Fone1Tb.Text = cliente.fone1;
                    cad1Fone2Tb.Text = cliente.fone2;
                    cad1TipoCb.SelectedItem = cliente.tipo;
                    if (cliente.tipo==0){
                    	cad1CpfCnpjLb.Text="CPF";
                    	cad1CpfTb.Mask="000,000,000-00";
                    }
                    else{
                    	cad1CpfCnpjLb.Text="CNPJ";
                    	cad1CpfTb.Mask="00,000,000/0000-00";
                    	
                    }
                    
                    foreach(Endereco end in cliente.endereco)
                    	cad1EnderecosCb.Items.Add(end.id.ToString()+"-"+end.logradouro+","+end.numero.ToString()+". "+end.bairro+" "+end.cidade+"/"+end.uf);
                    if (cliente.endereco.Count>0)
                    	cad1EnderecosCb.SelectedIndex=0;
                    
                    cad1ObsTb.Text = cliente.obs;
                    List<Service> all = Service.GetAllFrom(cliente.id, conexao.GetConnection());
                    if (all.Count == 0)
                    {
                        cad1Lst.Hide();
                    }
                    else
                    {
                        cad1Lst.Items.Clear();
                        foreach (Service serv in all)
                        {
                            cad1Lst.Items.Add(serv.id.ToString()+"- "+serv.data.ToString("dd/MM/yyyy"));
                        }
                        cad1Lst.Show();
                    }

                    cad1Dg.DataSource = Financa.GetFinanca(cliente, conexao.GetConnection());
                    
                    cad1IdLb.Text = cliente.id.ToString();

                    return "";
                }
                else
                {
                    return "Cliente não encontrado";
                }
            }
            catch(Exception e) { return "Erro: " + e.Message; }
        }
        private void cad1CodigoMtb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode== Keys.Enter)
            {
                cad1CodigoMtb.Text = cad1CodigoMtb.Text.Trim();
                long id;
                if (long.TryParse(cad1CodigoMtb.Text, out id))
                {
                    Cliente cli = Cliente.Load(id, conexao.GetConnection());
                    string msg = carregaCliente(cli);
                    if (msg != "")
                    {
                        statusSender( msg);
                        cad1CodigoMtb.Select();
                    }
                    else
                    {
                        cad1CodigoMtb.Clear();
                        cad1NomeTb.Select();
                    }
                }
                else
                {

                    statusSender( "Codigo não encontrado",Control.DefaultBackColor);
                    cad1CodigoMtb.Select();
                }
            }
        }

        private void statusSender(string msg)
        {
            try
            {
                Random rand = new Random();
                int r = rand.Next(100,255), g = rand.Next(100, 255), b = rand.Next(100, 255);
                Color cor = Color.FromArgb(255, r, g, b);
                toolStripMsg.BackColor = cor;
                toolStripMsg.Text = "Status: "+msg;
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }
        private void statusSender(string msg,Color cor)
        {
            try
            { 
                toolStripMsg.BackColor = cor;
                toolStripMsg.Text = "Status: " + msg;
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1VemLb_MouseDown(object sender, MouseEventArgs e)
        {
            long id;
            if (long.TryParse(cad1IdLb.Text, out id))
            {
                id--;
                Cliente cli = Cliente.Load(id, conexao.GetConnection());
                string msg = carregaCliente(cli);
                if (msg != "")
                    statusSender(msg);
            }
        }

        private void cad1VaiLb_Click(object sender, EventArgs e)
        {
            long id;
            if (long.TryParse(cad1IdLb.Text, out id))
            {
                id++;
                Cliente cli = Cliente.Load(id, conexao.GetConnection());
                string msg = carregaCliente(cli);
                if (msg != "")
                    statusSender(msg);
            }
        }

        private void cad1NomeTb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
            	Cliente cadastrado;
            		long ccli;
            		if (long.TryParse(cad1IdLb.Text,out ccli))
            		{
            			cadastrado = Cliente.Load(ccli,conexao.GetConnection());
            			if (cadastrado!=null)
            			{
            				if (cadastrado.nome!= cad1NomeTb.Text)
            				{
            					if (DialogResult.Yes== MessageBox.Show("Você deseja alterar o nome já cadastrado?\nAntes era:\n"+cadastrado.nome+"\nSerá alterado para:\n"+cad1NomeTb.Text,"Alterar o nome",MessageBoxButtons.YesNo))
            					{
            						if (Cliente.Update(ccli,"nome",cad1NomeTb.Text,conexao.GetConnection()))
            							statusSender("Nome atualizado com sucesso!");
            						else
            							statusSender("Não foi possível alterar o nome do cliente.",Color.Red);
            						return;
            					}
            					else{
            						if (DialogResult.Yes== MessageBox.Show("Você deseja registrar como cliente novo?","Nova insersão?",MessageBoxButtons.YesNo))
            						{
            							string nameAux=cad1NomeTb.Text;
            							limpaCad1();
            							cad1NomeTb.Text=nameAux;
            						}
            						else{
            							carregaCliente(cadastrado);
            							cad1Fone1Tb.Select();
            						}
            						carregaCliente(cadastrado);
            						cad1Fone1Tb.Select();
            					}
            				}
            				
            			}
            			else
            			{
            				List<Cliente> clientes = Cliente.GetByName(cad1NomeTb.Text, conexao.GetConnection());
            				if (clientes.Count == 1)
            				{
            					carregaCliente(clientes[0]);
            					cad1Fone1Tb.Select();
            				}
            				else
            					if (clientes.Count > 1 && clientes.Count <= 15)
            				{
            					SelectCliente(clientes, cad1NomeTb);
            				}
            				else
            					if (clientes.Count > 15)
            				{
            					statusSender(clientes.Count.ToString() + " resultados encontrados. Seja mais específico.");
            				}
            				else
            				{
            					cad1Fone1Tb.Select();
            				}
            			}
            		}
            	if (cad1IdLb.Text!="0")
            	{
            		
            	}
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void orçamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                mostraGb(orc1Gb);
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }
        

        private void orcAddDescTb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode==Keys.Enter)
                {
                	orc1AddValorTb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        
        private void orcAddValorTb_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    float val;
                    int addIndex;
                    long cliente, servico;
                    if (!long.TryParse(orc1CcliLb.Text, out cliente))
                    {
                        if (orc1NomeTb.Text == "")
                        {
                            statusSender("Primeiro carregue ou cadastre um cliente!");
                            orc1NomeTb.Select();
                            return;
                        }
                        else
                        {
                            List<Cliente> cList = Cliente.GetByName(orc1NomeTb.Text, conexao.GetConnection());
                            if (cList.Count == 0)
                            {
                                statusSender("Primeiro carregue ou cadastre um cliente!");
                                orc1NomeTb.Select();
                                return;
                            }
                            else
                            {
                                SelectCliente(cList, orc1Gb);
                            }
                        }
                        return;
                    }
                    
                    
                    float descTot=0,parPg=0,parFalta=0,aux_itemVal;
                    foreach(ListViewItem it in orc1ParcelasLv.Items)
                    {
                    	if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
                    		if (it.SubItems[2].Text!="Pendente")
                    			parPg+=aux_itemVal;
                    		else
                    			parFalta+=aux_itemVal;
                    	}
                    }
                    
                    orc1TotalLb.Text = (parPg+parFalta).ToString("F2");
                    orc1PagouLb.Text = parPg.ToString("F2");
                    orc1RestaLb.Text = parFalta.ToString("F2");
                    
                    
                    
                    
                    if (!long.TryParse(orc1IdLb.Text, out servico))
                        servico = 0;
                    
                    if (!int.TryParse(orc1AddIndexLb.Text, out addIndex))
                        addIndex = -1;
                    
                    
                    if (addIndex == -1)
                    {//novo
                    	DataGridViewButtonCell bt = new DataGridViewButtonCell();
                    	if (float.TryParse(orc1AddValorTb.Text, out val))
                        {
                        	if (servico!=0){
                    			ServiceDescricao.New(servico,orc1AddDescTb.Text,val,conexao.GetConnection());
                    			ServiceDescricao des = ServiceDescricao.GetLast(conexao.GetConnection());
                    			string id_desc="0";
                    			if (des!=null)
                    				id_desc = des.id.ToString();
                    			object[] vals = {id_desc, orc1AddDescTb.Text, val.ToString("F2"), bt };
                    			orc1Dg.Rows.Add(vals);
                    			orc1AddDescTb.Clear();
                    			orc1AddValorTb.Clear();
                    			orc1AddDescTb.Select();
                    			parcelaAdicionar(cliente,servico,val);
                    		}
                    		else{
                    			object[] vals = {"", orc1AddDescTb.Text, val.ToString("F2"), bt };
                    			orc1Dg.Rows.Add(vals);
                    			orc1AddDescTb.Clear();
                    			orc1AddValorTb.Clear();
                    			orc1AddDescTb.Select();
                    			parcelaAdicionar(cliente,servico,val);
                    		}
                    	}
                    	else
                        {
                            if (orc1AddValorTb.Text == "")
                            {
                            	if (servico!=0){
                            		ServiceDescricao.New(servico,orc1AddDescTb.Text,val,conexao.GetConnection());
                            		ServiceDescricao des = ServiceDescricao.GetLast(conexao.GetConnection());
                        		string id_desc="0";
                        		if (des!=null)
                        			id_desc = des.id.ToString();
                                object[] vals = { id_desc, orc1AddDescTb.Text, "0,00", bt };
                                orc1Dg.Rows.Add(vals);
                                orc1AddDescTb.Clear();
                                orc1AddValorTb.Clear();
                                orc1AddDescTb.Select();
                            	}else{
                    			object[] vals = {"", orc1AddDescTb.Text, val.ToString("F2"), bt };
                    			orc1Dg.Rows.Add(vals);
                    			orc1AddDescTb.Clear();
                    			orc1AddValorTb.Clear();
                    			orc1AddDescTb.Select();
                    		}
                            }
                            else
                            {
                                statusSender("Valor incorreto...");
                                orc1AddValorTb.Select();
                            }
                        }
                    	
                    }
                    else
                    {//atualiza
                    	
                    	long servRowId;
                    	if (!long.TryParse(orc1Dg.Rows[addIndex].Cells[0].Value.ToString(),out servRowId))
                    	{//está atualizando novo
                    		servRowId=0;
                    	}
                        if (float.TryParse(orc1AddValorTb.Text, out val))
                        {
                        	descTot =val;
                        	foreach(DataGridViewRow r in orc1Dg.Rows){
                        		if (float.TryParse(r.Cells[2].Value.ToString(),out aux_itemVal)){
                        			if(addIndex!=r.Index)
                        				descTot+=aux_itemVal;
                        		}
                        	}
                        	if (servico!=0)
                        	{
                        		if (!ServiceDescricao.Update(servRowId,orc1AddDescTb.Text,val.ToString("F2"),conexao.GetConnection()))
                        		{
                        			carregaService(Service.Load(servico,conexao.GetConnection()));
                        			statusSender("Não foi possível atualizar...");
                        		}
                        		else{
                        			
                        			alterarDescricao(addIndex,orc1AddDescTb.Text,val);
                        			orc1AddDescTb.Clear();
                        			orc1AddValorTb.Clear();
                        			orc1AddDescTb.Select();
                        			orc1AddIndexLb.Text = "-1";
                        			statusSender("Atualizado!");
                        		}
                        	}
                        	else{
                        		alterarDescricao(addIndex,orc1AddDescTb.Text,val);
                        		orc1AddDescTb.Clear();
                        		orc1AddValorTb.Clear();
                        		orc1AddDescTb.Select();
                        		orc1AddIndexLb.Text = "-1";
                        		
                        	}
                        }
                        else
                        {
                        	statusSender("Valor incorreto...");
                        	orc1AddValorTb.Select();
                        }
                    }
                    descTot=0;parPg=0;parFalta=0;
                    foreach(ListViewItem it in orc1ParcelasLv.Items)
                    {
                    	if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
                    		if (it.SubItems[2].Text!="Pendente")
                    			parPg+=aux_itemVal;
                    		else
                    			parFalta+=aux_itemVal;
                    	}
                    }
                    
                    orc1TotalLb.Text = (parPg+parFalta).ToString("F2");
                    orc1PagouLb.Text = parPg.ToString("F2");
                    orc1RestaLb.Text = parFalta.ToString("F2");

                }
                
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void SelectCliente(List<Cliente> clientes,Control parent)
        {
            try
            {
                Panel pn = new Panel();
                Point p = new Point(0, 0); 
                if (parent is TextBox)
                {
                    parent.Parent.Controls.Add(pn);
                }
                else
                    parent.Controls.Add(pn);
                pn.Location = parent.Location;
                pn.BringToFront();
                pn.AutoSize = true;
                pn.Name = "selectPn";

                List<Button> bt = new List<Button>();
                Button aux;
                foreach(Cliente c in clientes)
                {
                    aux = new Button();
                    aux.AutoSize = true;
                    aux.Text = c.id.ToString() + " - " + c.nome + " (Cel: " + c.fone1 +", Fixo: "+ c.fone2 + ")";
                    aux.Click += selectClienteButton_Click;
                    aux.Location = p;
                    bt.Add(aux);
                    p = new Point(0, aux.Height + p.Y + 12);
                }
                pn.Controls.AddRange(bt.ToArray());
                pn.Show();

            }catch(Exception e)
            {
                statusSender(e.Message);
            }
        }

        private void selectClienteButton_Click(object sender, EventArgs e)
        {
            try
            {
                Button bt = (Button)sender;
                long id;
                if (!long.TryParse(bt.Text.Substring(0, bt.Text.IndexOf(" -")), out id))
                    id = 0;
                Cliente cli = Cliente.Load(id, conexao.GetConnection());
                Control parent = bt.Parent;
                if (parent.Parent.Text == "Cliente")
                    carregaCliente(cli);
                else
                    carregaClienteService(cli);
                foreach(Control c in bt.Parent.Controls)
                {
                    c.Dispose();
                }
                parent.Dispose();

            }
            catch (Exception er)
            {
                statusSender(er.Message);
            }
        }
        
        private void orc1PTb_TextChanged(object sender, EventArgs e)
        {
        	try
        	{
        		double t1=0,t2;
        		
        		foreach(ListViewItem i in orc1ParcelasLv.Items)
        		{
        			if (double.TryParse(i.SubItems[0].Text,out t2))
        				t1+=t2;
        		}
        		orc1TotalLb.Text = (t1).ToString("F2");
        	}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }
        
        public bool parcelaAdicionar(long cliente,long servico, float valor){
        	try{
        		if (valor == 0){
        			return false;
        		}
        		float parPg=0,parFalta=0,aux_itemVal;
        		if (servico==0)
        		{
        			ListViewItem item = new ListViewItem(
        					new String[]{
        					valor.ToString("F2"),
        					DateTime.Today.ToString("dd/MM/yyyy"),
        					"Pendente"
        				});
        			orc1ParcelasLv.Items.Add(item);
        			foreach(ListViewItem it in orc1ParcelasLv.Items)
        			{
        				if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
        					if (it.SubItems[2].Text!="Pendente")
        						parPg+=aux_itemVal;
        					else
        						parFalta+=aux_itemVal;
        				}
        			}
        			
        			orc1TotalLb.Text = (parPg+parFalta).ToString("F2");
        			orc1PagouLb.Text = parPg.ToString("F2");
        			orc1RestaLb.Text = parFalta.ToString("F2");
        			return true;
        		}
        		else{
        			string msg = Financa.NewOrUpdate(orc1ParcelasLv.Items.Count,cliente,servico,valor,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection());
        			if (msg!=""){
        				statusSender("Erro ao adicionar parcela: "+msg);
        				return false;
        			}
        			else{
        				ListViewItem item = new ListViewItem(
        					new String[]{
        						valor.ToString("F2"),
        						DateTime.Today.ToString("dd/MM/yyyy"),
        						"Pendente"
        					});
        				orc1ParcelasLv.Items.Add(item);
        				foreach(ListViewItem it in orc1ParcelasLv.Items)
        				{
        					if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
        						if (it.SubItems[2].Text!="Pendente")
        							parPg+=aux_itemVal;
        						else
        							parFalta+=aux_itemVal;
        					}
        				}
        				
        				orc1TotalLb.Text = (parPg+parFalta).ToString("F2");
        				orc1PagouLb.Text = parPg.ToString("F2");
        				orc1RestaLb.Text = parFalta.ToString("F2");
        				return true;
        			}
        		}
        		
        		
        	}catch(Exception e)
        	{
        		statusSender(e.Message);
        		return false;
        	}
        	
        }
        
        
        public bool parcelaRemover(int parcela, long servico){
        	try{
        		float restante,valor=0;
        		float total = 0, parcelas = 0,aux;
        		if(float.TryParse(orc1ParcelasLv.Items[parcela].SubItems[0].Text,out valor)){
        			orc1ParcelasLv.Items.RemoveAt(parcela);
        		}
                foreach (DataGridViewRow r in orc1Dg.Rows)
                {
                    if (float.TryParse(r.Cells["valor"].Value.ToString(), out aux))
                    {
                        total += aux;
                    }
                }
                foreach(ListViewItem it in orc1ParcelasLv.Items){
                	if (float.TryParse(it.SubItems[0].Text, out aux))
                    {
                        parcelas += aux;
                    }
                }
        		if (servico==0){
                	if(total < parcelas){
                		orc1Dg.Rows.Add(new string[]{"","",(parcelas-total).ToString("F2")});
                		
                	}else if(total>parcelas){
                		restante = valor;
        				for(int i=orc1Dg.Rows.Count-1; i>=0; i--){
        					if (float.TryParse(orc1Dg.Rows[i].Cells[2].Value.ToString(),out aux)){
        							restante-=aux;
        						if (restante > 0){
        								if(orc1Dg.Rows[i].Cells[1].Value.ToString()=="")
        									orc1Dg.Rows.RemoveAt(i);
        								else
        									orc1Dg.Rows[i].Cells[2].Value="0,00";
        						}
        						else if(restante <0){
        							orc1Dg.Rows[i].Cells[2].Value = (restante*-1).ToString("F2");
        							break;
        							}else {
        								if(orc1Dg.Rows[i].Cells[1].Value.ToString()=="")
        									orc1Dg.Rows.RemoveAt(i);
        								else
        									orc1Dg.Rows[i].Cells[2].Value="0,00";
        								break;
        							}
        					}
        				}
                	}else{
                		
                	}
        			return true;
        		}else{
                	ServiceDescricao ds;
                	if(total < parcelas){
                		if(ServiceDescricao.New(servico,"",parcelas-total,conexao.GetConnection())==""){
                			ds = ServiceDescricao.GetLast(conexao.GetConnection());
                			if(ds!=null)
                				orc1Dg.Rows.Add(new string[]{ds.id.ToString(),ds.descricao,ds.valor.ToString("F2")});
                		}
                		
                	}else if(total>parcelas){
                		restante = valor;
        				for(int i=orc1Dg.Rows.Count-1; i>=0; i--){
        					if (float.TryParse(orc1Dg.Rows[i].Cells[2].Value.ToString(),out aux)){
                				ds = ServiceDescricao.LoadId(long.Parse(orc1Dg.Rows[i].Cells[0].Value.ToString()),conexao.GetConnection());
                				restante-=aux;
        						if (restante > 0){
        								if (ds!=null){
        									if(orc1Dg.Rows[i].Cells[1].Value.ToString()==""){
        										if(ServiceDescricao.Remove(ds.id,conexao.GetConnection()))
        											orc1Dg.Rows.RemoveAt(i);
        									}else{
        										if(ServiceDescricao.Update(ds.id,orc1Dg.Rows[i].Cells[1].Value.ToString(),"0,00",conexao.GetConnection()))
        											orc1Dg.Rows[i].Cells[2].Value="0,00";
        									}
        								}
        							}
        							else if(restante <0){
        								if(ServiceDescricao.Update(ds.id,orc1Dg.Rows[i].Cells[1].Value.ToString(),(restante*-1).ToString("F2"),conexao.GetConnection()))
        									orc1Dg.Rows[i].Cells[2].Value = (restante*-1).ToString("F2");
        								break;
        							}else {
        								if(orc1Dg.Rows[i].Cells[1].Value.ToString()==""){
        									if(ServiceDescricao.Remove(ds.id,conexao.GetConnection()))
        											orc1Dg.Rows.RemoveAt(i);
        								}
        								else{
        									if(ServiceDescricao.Update(ds.id,orc1Dg.Rows[i].Cells[1].Value.ToString(),"0,00",conexao.GetConnection()))
        										orc1Dg.Rows[i].Cells[2].Value = "0,00";
        									
        								}
        								break;
        							}
                			}
                		}
                	}
                	Financa.Delete(parcela,servico,conexao.GetConnection());
        			return true;
        		}
        		
        	}catch(Exception e)
        	{
        		statusSender(e.Message);
        		return false;
        	}
        	
        }
        

        private void calculaTotal()
        {
            try
            {
                float aux, total = 0, pg = 0;
                aux = total - pg;
                orc1TotalLb.Text = total.ToString("F2");
                orc1PagouLb.Text = pg.ToString("F2");
                orc1RestaLb.Text = aux.ToString("F2");
                if (aux > 0)
                    orc1RestaLb.ForeColor = Color.Red;
                else
                    orc1RestaLb.ForeColor = Color.Black;

            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void orc1Gb_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (orc1Gb.Visible)
                {
                    if (orc1NomeTb.Text != "")
                        orc1AddDescTb.Select();
                    else
                        orc1NomeTb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }



        private void limpaService()
        {
            try
            {
                //descr
                orc1IdLb.Text = "0";
                orc1CcliLb.Text = "0";
                
                orc1AddIndexLb.Text = "-1";
                orc1CodigoMtb.Clear();
                orc1CodigoMtb.Enabled = true;
                orc1AddDescTb.Clear();
                orc1AddValorTb.Clear();
                orc1Dg.Rows.Clear();
                orc1Dg.Enabled = true;
                orc1PrazoDtp.Value = DateTime.Now;
                //cli
                orc1CpfLb.Text="CPF";
                orc1CpfMtb.Mask="000,000,000-00";
                orc1CpfMtb.Clear();
                orc1NomeTb.Clear();
                orc1EndCb.Items.Clear();
                orc1EndCb.Text="";
                
                orc1P0Dtp.Value = DateTime.Now;
                orc1F0Cb.SelectedIndex = 0;
                populateAutocomplete();

                orc1ParcelasLv.Items.Clear();
                orc1ParIdLb.Text="-1";
                orc1Lst.Items.Clear();
                orc1Lst.Hide();
                Control[] selecPn = orc1Gb.Controls.Find("selectPn", true);
                if (selecPn.Length != 0)
                {
                    foreach(Control c in selecPn[0].Controls)
                        c.Dispose();
                    selecPn[0].Dispose();
                }
                orc1StatusCb.SelectedItem=0;



                //sub
                orc1PagouLb.Text = "0,00";
                orc1RestaLb.Text = "0,00";
                orc1TotalLb.Text = "0,00";
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void orcLBt_Click(object sender, EventArgs e)
        {
            try
            {
                limpaService();
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void carregaClienteService(Cliente cli)
        {
            try
            {
                orc1NomeTb.Text = cli.nome;
                orc1CpfMtb.Text = cli.cpf;
                orc1CcliLb.Text = cli.id.ToString();
                orc1EndCb.Items.Clear();
                foreach(Endereco end in cli.endereco)
                {
                	orc1EndCb.Items.Add(end.id.ToString()+"-"+end.logradouro+","+end.numero.ToString()+". "+end.bairro+" "+end.cidade+"/"+end.uf);
                }
                if (orc1EndCb.Items.Count==0)
                {
                	carregaCliente(cli);
                	mostraGb(cad1Gb);
                	statusSender("Adicione um endereço para este cliente!");
                }
                else
                	if (orc1EndCb.Items.Count==1)
                {
                	orc1EndCb.SelectedIndex=0;
                	orc1Lst.Items.Clear();
                	foreach(Service s in  Service.GetAllFrom(cli.id,conexao.GetConnection()))
                	{
                		orc1Lst.Items.Add(s.id.ToString()+"-"+s.endereco.logradouro+", "+s.endereco.numero.ToString()+". "+s.endereco.bairro+"."+s.endereco.cidade+"/"+s.endereco.uf);
                	}
                	if (orc1Lst.Items.Count==0)
                		orc1Lst.Hide();
                	else
                	{
                		orc1Lst.Show();
                	}
                }
                else
                {
                	orc1EndCb.DroppedDown = true;
                	statusSender("Selecione um endereço para este cliente!");
                }
            }
            catch(Exception e) { statusSender("Erro: " + e.Message); }
        }

        private void orc1NomeTb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                	string aux = orc1NomeTb.Text;
                	limpaService();
                	orc1NomeTb.Text = aux;
                    List<Cliente> cList = Cliente.GetByName(orc1NomeTb.Text, conexao.GetConnection());

                    if (cList.Count == 0)
                    {

                    }
                    else
                    {
                        if (cList.Count == 1)
                        {
                            carregaClienteService(cList[0]);
                        }
                        else
                        {

                            SelectCliente(cList, orc1Gb);
                            statusSender("Escolha o cliente");
                        }
                    }
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void orcXbt_Click(object sender, EventArgs e)
        {
            try
            {
                orc1Gb.Hide();
                limpaService();
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void carregaService(Service serv)
        {
            try
            {
                if (serv!=null)
                {
                	orc1IdLb.Text="0";
                	orc1CcliLb.Text="0";
                	orc1ParcelasLv.Items.Clear();
                    orc1Dg.Rows.Clear();
                    List<object> obj = new List<object>(), pars = new List<object>();
                    double total_desc = 0 ;
                    foreach (ServiceDescricao des in serv.listaDeDecricao)
                    {
                        total_desc += des.valor;
                        obj.Clear();
                        obj.Add(des.id.ToString());
                        obj.Add(des.descricao);
                        obj.Add(des.valor.ToString("F2"));
                        obj.Add(new DataGridViewButtonCell());
                        orc1Dg.Rows.Add(obj.ToArray());
                    }
                    DateTime dtPg = DateTime.Today;
                    orc1StatusCb.SelectedIndex = (int)serv.status;
                    List<ListViewItem> ps = new List<ListViewItem>();
                    if(serv.parcelas.Count>0)
                    {
                    	int parIndex=0;
                    	foreach(Financa f in serv.parcelas)
                    	{
                    		ps.Add(new ListViewItem(new string[]{ f.valor.ToString("F2"),f.data.ToString("dd/MM/yyyy"),f.forma.ToString()}));
                    		parIndex++;
                    	}
                    	orc1ParcelasLv.Items.AddRange(ps.ToArray());
                    	
                    }
                    double pago=0,falta=0,aux_itemVal;
                    foreach(ListViewItem it in orc1ParcelasLv.Items)
                    {
                    	if (double.TryParse(it.SubItems[0].Text,out aux_itemVal)){
                    		if (it.SubItems[2].Text!="Pendente")
                    			pago+=aux_itemVal;
                    		else
                    			falta+=aux_itemVal;
                    	}
                    }
                    orc1TotalLb.Text = (pago+falta).ToString("F2");
                    orc1PagouLb.Text = pago.ToString("F2");
                    orc1RestaLb.Text = falta.ToString("F2");
                    orc1PrazoDtp.Value = serv.data;
                    
                    
                    //carregaClienteService(serv.cliente);
                    if (serv.cliente.tipo==0)
                    {
                    	orc1CpfLb.Text="CPF";
                    	orc1CpfMtb.Mask="000,000,000-00";
                    }
                    else{
                    	orc1CpfLb.Text="CNPJ";
                    	orc1CpfMtb.Mask="00,000,000/0000-00";
                    	
                    }
                    
                    //carregar isso por ultimo. Evitar autosave
                    orc1NomeTb.Text = serv.cliente.nome;
                    orc1EndCb.Items.Clear();
                    foreach(Endereco end in serv.cliente.endereco)
                    	orc1EndCb.Items.Add(end.id.ToString()+"-"+end.logradouro+","+end.numero.ToString()+". "+end.bairro+" "+end.cidade+"/"+end.uf);
                    orc1EndCb.Text = serv.endereco.id.ToString()+"-"+serv.endereco.logradouro+","+serv.endereco.numero.ToString()+". "+serv.endereco.bairro+" "+serv.endereco.cidade+"/"+serv.endereco.uf;
                    orc1CodigoMtb.Text = serv.codigo.ToString();
                    orc1IdLb.Text = serv.id.ToString();
                    orc1CcliLb.Text = serv.cliente.id.ToString();
                    statusSender("Orçamento localizado!", Control.DefaultBackColor);
                }
                else
                    statusSender("Orçamento não encontrado!");

            }
            catch (Exception e)
            {
                statusSender("Erro: " + e.Message);
            }
        }

        private void orc1CodigoMtb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long id;
                    if (long.TryParse(orc1CodigoMtb.Text, out id))
                    {
                        Service service = Service.LoadByBafuniCod(id, conexao.GetConnection());
                        if (service!=null)
                            carregaService(service);
                        else
                        {
                            atualizaService();
                        }
                    }
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        
        }


        private void orc1AddValorTb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (orc1AddValorTb.Text == "")
                    return;
                float f;
                if (!float.TryParse(orc1AddValorTb.Text, out f))
                {
                    orc1AddValorTb.BackColor = Color.Red;
                    statusSender("formato incorreto");
                }
                else
                {
                    orc1AddValorTb.BackColor = SystemColors.Window;
                    statusSender("",Control.DefaultBackColor);
                }

            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }


        private void relatóriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                mostraGb(rel1Gb);

            }catch(Exception er)
            {
                statusSender("Erro: " + er.Message);
            }
        }

        private void rel1LBt_Click(object sender, EventArgs e)
        {
            try
            {
                limpaRel1();
            }
            catch (Exception er)
            {
                statusSender("Erro: " + er.Message);
            }
        }

        private void limpaRel1()
        {
            try
            {
                rel1Tb.Clear();
            }
            catch (Exception er)
            {
                statusSender("Erro: " + er.Message);
            }
        }

        private void rel1XBt_Click(object sender, EventArgs e)
        {

            try
            {
                limpaRel1();
                rel1Gb.Hide();
            }
            catch (Exception er)
            {
                statusSender("Erro: " + er.Message);
            }
        }

        private void rel1RelataBt_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime de,ate;
                List<Service> all = Service.GetAll(conexao.GetConnection());
                if(all.Count>0)
                {
                    List<string> lines = new List<string>();
                    de = new DateTime(rel1DeDtp.Value.Year, rel1DeDtp.Value.Month, rel1DeDtp.Value.Day, 0, 0, 0);
                    ate = new DateTime(rel1AteDtp.Value.Year, rel1AteDtp.Value.Month, rel1AteDtp.Value.Day, 0, 0, 0);
                    lines.Add("Orçamentos de ");
                    all.Sort();//<--ver regra
                    foreach(Service se in all)
                    {
                        if ((DateTime.Compare(se.data, de) >= 0) && (DateTime.Compare(se.data, ate) < 0))
                        {
                            lines.Add("cod. " + se.id.ToString() + "  \"" + rel1Gb.Name + "\" " + se.cliente.fone1 + "  " + se.cliente.fone2);
                            foreach (ServiceDescricao des in se.listaDeDecricao)
                                lines.Add("    " + des.descricao + "   " + des.valor.ToString("F2"));

                            lines.Add("____________________________________________________________________");
                        }
                    }
                    rel1Tb.Lines = lines.ToArray();
                }
            }
            catch (Exception er)
            {
                statusSender("Erro: " + er.Message);
            }
        }

        private void orc1SalvarBt_Click(object sender, EventArgs e)
        {
            try
            {
                long id,ccli;
                if (!long.TryParse(orc1CcliLb.Text, out ccli))
                {
                    if (orc1NomeTb.Text!="")
                    {
                        List<Cliente> clie = Cliente.Find(orc1NomeTb.Text, conexao.GetConnection());
                        if (clie.Count == 0)
                        {
                            statusSender("Você precisa Adicionar um cliente...");
                            return;
                        }
                        else
                        if (clie.Count > 15)
                        {
                            SelectCliente(clie,orc1Gb);
                            statusSender("Você precisa Adicionar um cliente...");
                            return;
                        }
                        else
                            carregaClienteService(clie[0]);
                    }
                    else
                    {
                        statusSender("Você precisa Adicionar um cliente...");
                        orc1NomeTb.Select();
                        return;
                    }
                }
                if (!long.TryParse(orc1IdLb.Text, out id))
                    id = 0;

                if (id == 0)
                {//novo
                    Cliente cli = Cliente.Load(ccli, conexao.GetConnection());
                    if (cli != null)
                    {//validação
                        if (orc1Dg.Rows.Count == 0)
                        {
                            statusSender("Adicione algo ao Orçamento.");
                            return;
                        }
                        if (orc1PrazoDtp.Value.CompareTo(DateTime.Now) < 0)
                        {
                            if (MessageBox.Show("O prazo está correto?", "Verificar prazo", MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                orc1PrazoDtp.Select();
                                return;
                            }
                        }
                        //validaçao end
                        Service.descricao aux;
                        //Financa.Irow_parcer aux2;
                        List<Service.descricao> descricao = new List<Service.descricao>();
                        List<Financa.Irow_parcer> parcelas = new List<Financa.Irow_parcer>();
                        Double aux0;
                        foreach (DataGridViewRow r in orc1Dg.Rows)
                        {
                            aux = new Service.descricao();
                            aux.desc_str = r.Cells[1].Value.ToString();
                            if (Double.TryParse(r.Cells[2].Value.ToString(), out aux0))
                                aux.valor = aux0;
                            else
                                aux.valor = 0;
                            descricao.Add(aux);
                        }
                        long codigo,endereco=0,auxTipoPag;
                        if (!long.TryParse(orc1CodigoMtb.Text, out codigo))
                            codigo = 0;
                        foreach( ListViewItem it in orc1ParcelasLv.Items)
                        {
                        	if (!double.TryParse(it.SubItems[0].Text,out aux0))
                        	    aux0=0;
                        	auxTipoPag = Financa.TipoFormasStringToId(it.SubItems[2].Text);
                        	parcelas.Add(new Financa.Irow_parcer((long)it.Index,
                        	                                     DateTime.Parse(it.SubItems[1].Text),
                        	                                     Financa.setTipo(auxTipoPag),
                        	                                     aux0));
                        }
                        int indexof =orc1EndCb.Text.IndexOf('-');
                        if (indexof!=-1)
                        {
                        	if (!long.TryParse(orc1EndCb.Text.Substring(0,indexof),out endereco))
                        	{
                        		statusSender("Escolha um endereço");
                        		orc1EndCb.Show();
                        		orc1EndCb.DroppedDown=true;
                        		return;
                        	}
                        }
                        else{
                        	statusSender("Escolha um endereço");
                        	orc1EndCb.Show();
                        	orc1EndCb.DroppedDown=true;
                        	return;
                        }
                        
                        string erro = Service.New(codigo, cli.id,endereco, DateTime.Now, orc1PrazoDtp.Value, 0, "", descricao,parcelas, conexao.GetConnection());

                        if (erro == "")
                        {
                            statusSender("Orçamento registrado no sistema!", SystemColors.Window);
                            limpaService();
                        }
                        else
                        {
                            statusSender("Erro: " + erro);
                        }
                    }
                    else
                    {
                        List<Cliente> lst = Cliente.Find(orc1NomeTb.Text, conexao.GetConnection());
                        if (lst.Count == 0)
                        {
                            limpaCad1();
                            cad1NomeTb.Text = orc1NomeTb.Text;
                            mostraGb(cad1Gb);
                            statusSender("Cliente não cadastrado... Cadastre o primeiro");
                        }
                        else
                        if(lst.Count == 1)
                        {
                            carregaClienteService(lst[0]);
                            statusSender("Cliente não estava carregado corretamente. Tente de novo!");
                        }
                        else
                        {
                            SelectCliente(lst, orc1Gb);
                            statusSender("Cliente não estava carregado corretamente. Escolha um deles ou cadastre de novo!");
                            orc1NomeTb.Select();
                        }
                    }
                }
                else
                {//atualiza...
                    if (!Service.Update(id, "codigo", orc1CodigoMtb.Text, conexao.GetConnection()))
                        statusSender("não foi possivel atualizar o codigo...");
                    if (!Service.Update(id, "data", orc1PrazoDtp.Value.ToString("yyyy-MM-dd HH:mm:ss"), conexao.GetConnection()))
                        statusSender("São foi possivel atuaçizar o Prazo...");
                    if (!
                    Service.Update(id, "codigo", orc1CodigoMtb.Text, conexao.GetConnection()))
                        statusSender("não foi possivel atualizar o codigo...");
                    long id_list,auxForma;
                    double val;
                    DateTime auxDt;
                    string s;
                    foreach (DataGridViewRow r in orc1Dg.Rows)
                    {
                    	s=r.Cells[2].Value.ToString();
                    	if (!double.TryParse(s, out val))
                            val = 0;
                        if (long.TryParse(r.Cells[0].Value.ToString(), out id_list))
                        {
                        	if (!ServiceDescricao.Update(id_list, r.Cells[1].Value.ToString(),val.ToString("F2"), conexao.GetConnection()))
                        	{
                        		carregaService(Service.Load(id,conexao.GetConnection()));
                        		statusSender("Não foi possivel atualizar a descrição...");
                        		return;
                        	}
                        }
                        else
                        {
                            s = ServiceDescricao.New(id, r.Cells[1].Value.ToString(), val, conexao.GetConnection());
                        }
                    }
                    
                    foreach(ListViewItem it in orc1ParcelasLv.Items)
                    {
                    	if (!double.TryParse(it.SubItems[0].Text, out val))
                    		val=0;
                    	if (!DateTime.TryParse(it.SubItems[1].Text,out auxDt))
                    		auxDt=DateTime.Now;
                    	if (!long.TryParse(it.SubItems[2].Text, out auxForma))
                    		auxForma = 0;
                    	Financa.NewOrUpdate((long)it.Index, ccli, id,val, Financa.setTipo(auxForma),
                    	                    DateTime.Today, auxDt, conexao.GetConnection());
                    }
                    limpaService();
                    carregaService(Service.Load(id, conexao.GetConnection()));
                }
            }
            catch (Exception er)
            {
                statusSender("Erro: " + er.Message);
            }
        }

        private void orc1CcliLb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                long id;
                if (long.TryParse(orc1CcliLb.Text, out id))
                {
                    List<Service> allServ = Service.GetAllFrom(id, conexao.GetConnection());
                    if (allServ.Count > 0)
                    {
                        orc1Lst.Show();
                        orc1Lst.Items.Clear();
                        foreach(Service s in allServ){
                        	orc1Lst.Items.Add(s.id.ToString()+"-"+s.endereco.logradouro+","+s.endereco.numero.ToString()+" ("+s.data.ToString("dd/MM/yyyy")+" às "+ s.data.ToString("HH:mm")+")");
                        	
                        }
                    }
                    else
                        orc1Lst.Hide();
                }
            }
            catch(Exception er) { statusSender("Erro: " + er.Message); }
        }

        private void atualizaService()
        {
            try
            {
                long id,cli,index;
                if (!long.TryParse(orc1IdLb.Text, out id))
                    return;
                if (!long.TryParse(orc1CcliLb.Text, out cli))
                    return;

                if (orc1CodigoMtb.Text != "")
                    Service.Update(id, "codigo", orc1CodigoMtb.Text, conexao.GetConnection());

                Service.Update(id, "data", orc1PrazoDtp.Value, conexao.GetConnection());

                foreach(DataGridViewRow r in orc1Dg.Rows)
                {
                	if (!long.TryParse(r.Cells[0].Value.ToString(),out index))
                		continue;
                    ServiceDescricao.Update(index, r.Cells[1].Value.ToString(), r.Cells[2].Value.ToString(), conexao.GetConnection());
                }

            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }
        

        private void orcDg_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1)
                    return;
                if (e.ColumnIndex == 0)
                    orc1AddDescTb.Select();
                else
                    orc1AddValorTb.Select();
                
                if (orc1Dg.Rows[e.RowIndex].Cells[e.ColumnIndex] is DataGridViewButtonCell)
                {
                    long sevId,cliId;
                    if (!long.TryParse(orc1IdLb.Text, out sevId))
                    	sevId = 0;
                    if (!long.TryParse(orc1CcliLb .Text, out cliId))
                    	cliId = 0;
                    removeDescricao(e.RowIndex);
                    return;
                }
                orc1AddIndexLb.Text = e.RowIndex.ToString();
                orc1AddDescTb.Text = orc1Dg.Rows[e.RowIndex].Cells["desc"].Value.ToString();
                orc1AddValorTb.Text = orc1Dg.Rows[e.RowIndex].Cells["valor"].Value.ToString();

            }
            catch (Exception erro){ statusSender("Erro: " + erro.Message); }
        }
        
        

        private void cad1CpfTb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Cliente cli = Cliente.Load(cad1CpfTb.Text, conexao.GetConnection());
                    if (cli != null)
                    {
                        carregaCliente(cli);
                        cad1NomeTb .Select();
                    }
                    else
                    {
                    	long id;
                    	if(long.TryParse(cad1IdLb.Text,out id))
                    	{
                    		if (!Cliente.Update(id,"cpf",cad1CpfTb.Text,conexao.GetConnection()))
                    		{
                    			statusSender("Não foi possível atualizar");
                    			carregaCliente(Cliente.Load(id,conexao.GetConnection()));
                    		}
                    		else
                    		{
                    			statusSender("Atualização realizada com sucesso");
                    		}
                    		
                    	}
                    	cad1CpfTb.Select();
                    }
                }

            }
            catch (Exception erro) { statusSender("Erro: " + erro.Message); }
        }


        private void orc1Lst_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string str = orc1Lst.GetItemText(orc1Lst.SelectedItem);
                int inxOf = str.IndexOf('-');
                if (inxOf == -1)
                    return;
                long id;
                if (long.TryParse(str.Substring(0,inxOf),out id))
                {
                    limpaService();
                    Service serv = Service.Load(id, conexao.GetConnection());
                    carregaService(serv);
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void orc1IdLb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (orc1IdLb.Text == "0")
                {
                	orc1SalvarBt.Show();
                	orc1AddParBt.Hide();
                	orc1ExcluirOrcBt.Hide();
                }
                else{
                	orc1SalvarBt.Hide();
                	orc1AddParBt.Show();
                	orc1ExcluirOrcBt.Show();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1Lst_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string str = cad1Lst.GetItemText(cad1Lst.SelectedItem);
                int inxOf = str.IndexOf('-');
                if (inxOf == -1)
                    return;
                long id;
                if (long.TryParse(str.Substring(0, inxOf), out id))
                {
                    Service serv = Service.Load(id, conexao.GetConnection());
                    carregaService(serv);
                    mostraGb(orc1Gb);
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
            	using (var comm = new System.Data.SQLite.SQLiteCommand(conexao.GetConnection()))
                {
                    conexao.GetConnection().Open();
                    
                	comm.CommandText = "ALTER TABLE cliente ADD COLUMN email TEXT NOT NULL DEFAULT ''";
                        if( comm.ExecuteNonQuery()>0)
                        	statusSender("ok");
                        else 
                        	statusSender("zica");
                        conexao.GetConnection().Close();
                }
            	
            	/*MySql.Data.MySqlClient.MySqlConnection con = SqLite.mysqlConect;
                DataTable dt = new DataTable();
                con.Open();
                MySql.Data.MySqlClient.MySqlDataAdapter adp = new MySql.Data.MySqlClient.MySqlDataAdapter("select * from cep where cidade='São Carlos' or cidade='Araraquara' or "+
                                                                                                          "cidade='Rio Claro' or cidade='itirapina' or cidade='Ribeirão Bonito' or cidade='Ibaté' or cidade='Analândia'or cidade='Porto Ferreira'" , con);
                con.Close();
                adp.Fill(dt);

                string sqliteConnectionString = "Data Source=" + System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\cep;Version=3;";
                System.Data.SQLite.SQLiteConnection sqliteCon = new System.Data.SQLite.SQLiteConnection(sqliteConnectionString);
                using (var comm = new System.Data.SQLite.SQLiteCommand(sqliteCon))
                {
                    sqliteCon.Open();
                	MessageBox.Show("Start!");
                    foreach (DataRow r in dt.Rows)
                    {
                        comm.CommandText = "INSERT INTO cep (cep,log_tipo_logradouro,logradouro,bairro,cidade,uf) values (@cep,@tipo,@log,@bairro,@cidade,@uf)";
                        comm.Parameters.AddWithValue("@cep", r["cep"]);
                        comm.Parameters.AddWithValue("@tipo", r["log_tipo_logradouro"].ToString().ToUpper());
                        comm.Parameters.AddWithValue("@log", r["logradouro"].ToString().ToUpper());
                        comm.Parameters.AddWithValue("@bairro", r["bairro"].ToString().ToUpper());
                        comm.Parameters.AddWithValue("@cidade", r["cidade"].ToString().ToUpper());
                        comm.Parameters.AddWithValue("@uf", r["uf"].ToString().ToUpper());
                        comm.ExecuteNonQuery();

                    }
                    sqliteCon.Close();
                }
                MessageBox.Show("Pronto!");*/
            }
            catch (Exception er){ MessageBox.Show(er.Message); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pesGb.Hide();
        }


        private void pesquisarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mostraGb(pesGb);
        }

        private void pes1CliTb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    List<Cliente> clientes = Cliente.Find(pes1CliTb.Text, conexao.GetConnection());
                    if(clientes.Count==0)
                    {
                        statusSender("Cliente não encontrado...",Control.DefaultBackColor);
                    }
                    else
                    {
                        if (clientes.Count==1)
                        {
                            carregaCliente(clientes[0]);
                        }
                        pes1Lst.Items.Clear();
                        string itemDesc; 
                        foreach(Cliente c in clientes)
                        {
                        	itemDesc = c.id.ToString() + " - " + c.nome + " (" + c.endereco[0].logradouro + ", " + c.endereco[0].numero.ToString() + ")";
                            pes1Lst.Items.Add(itemDesc);
                        }
                    }
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void pes1Lst_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string str = pes1Lst.GetItemText(pes1Lst.SelectedItem);
                int inxOf = str.IndexOf(" -");
                if (inxOf == -1)
                    return;
                long id;
                if (long.TryParse(str.Substring(0, inxOf), out id))
                {
                    Cliente cliente = Cliente.Load(id, conexao.GetConnection());
                    carregaCliente(cliente);
                    mostraGb(cad1Gb);
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1CepMtb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Endereco end = Endereco.LoadCep(cad1CepMtb.Text);
                    if (end!=null)
                    {
                        cad1EnderecoTb.Text = end.tipoLogradouro + " " + end.logradouro;
                        cad1BairroTb.Text = end.bairro;
                        cad1CidadeTb.Text = end.cidade;
                        cad1UfTb.Text = end.uf;
                        statusSender("", Control.DefaultBackColor);
                        cad1EnderecoTb.Select();
                    }
                    else
                    {
                        cad1EnderecoTb.Clear();
                        cad1BairroTb.Clear();
                        cad1CidadeTb.Clear();
                        cad1UfTb.Clear();
                        statusSender("Endereço não encontrado", Control.DefaultBackColor);

                        cad1CepMtb.Select();
                    }
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1IdLb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (cad1IdLb.Text=="0")
                    cad1SalvarBt.Text = "Salvar";
                else
                    cad1SalvarBt.Text = "Atualizar";

            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1Fone1Tb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                	Cliente cadastrado = Cliente.LoadFromTel(cad1Fone1Tb.Text,conexao.GetConnection());
                	if (cadastrado!=null)
                	{
                		carregaCliente(cadastrado);
                		statusSender("Já existe um cliente com este telefone!");
                		return;
                	}
                	
                	long id;
                	if(long.TryParse(cad1IdLb.Text,out id))
                	{
                		if (!Cliente.Update(id,"fone1",cad1Fone1Tb .Text,conexao.GetConnection()))
                		{
                			statusSender("Não foi possível atualizar");
                			carregaCliente(Cliente.Load(id,conexao.GetConnection()));
                		}
                		else
                		{
                			statusSender("Atualização realizada com sucesso");
                		}
                		
                	}
                    cad1Fone2Tb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1Fone2Tb_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                	Cliente cadastrado = Cliente.LoadFromTel(cad1Fone2Tb.Text,conexao.GetConnection());
                	if (cadastrado!=null)
                	{
                		carregaCliente(cadastrado);
                		statusSender("Já existe um cliente com este telefone!");
                		return;
                	}
                	long ccli;
                	if(long.TryParse(cad1IdLb.Text,out ccli))
                	{
                		if (!Cliente.Update(ccli,"fone2",cad1Fone2Tb .Text,conexao.GetConnection()))
                		{
                			statusSender("Não foi possível atualizar");
                			carregaCliente(Cliente.Load(ccli,conexao.GetConnection()));
                		}
                		else
                		{
                			statusSender("Atualização realizada com sucesso");
                		}
                		
                	}
                    cad1EmailTb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1EmailTb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
            	if(e.KeyCode == Keys.Enter){
            		cad1ObsTb.Select();
            	}
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1EnderecoTb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Endereco end = Endereco.FindCep(cad1EnderecoTb.Text,cad1CidadeTb.Text);
                    cad1NumeroTb.Clear();
                    if (end!=null)
                    {
                        cad1CepMtb.Text = end.cep;
                        cad1TipologCb.Text= end.tipoLogradouro;
                        cad1EnderecoTb.Text = end.logradouro;
                        cad1BairroTb.Text = end.bairro;
                        cad1CidadeTb.Text = end.cidade;
                        cad1ComplementoTb.Text =end.complemento;
                        cad1UfTb.Text = end.uf;
                        statusSender("", Control.DefaultBackColor);
                        cad1EnderecoTb.Select();
                    }
                    else
                    {
                        cad1BairroTb.Clear();
                        cad1CidadeTb.Clear();
                        cad1UfTb.Clear();
                    }
                    cad1NumeroTb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1NumeroTb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cad1BairroTb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1BairroTb_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cad1ComplementoTb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1ComplementoTb_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                   cad1CidadeTb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1CidadeTb_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cad1UfTb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void cad1UfTb_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cad1NewEndBt.Select();
                }
            }
            catch { }
        }

        private void orc1CpfMtb_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Cliente cli = Cliente.Load(orc1CpfMtb.Text, conexao.GetConnection());
                if (cli!=null)
                {
                    carregaClienteService(cli);
                }
                else
                {
                    if (MessageBox.Show("Cpf não encontrado, deseja cadastrar novo?") == DialogResult.Yes)
                    {
                        mostraGb(cad1Gb);
                        limpaCad1();
                        cad1CpfTb.Text = orc1CpfMtb.Text;
                        cad1NomeTb.Select();
                    }
                    else
                        cad1CepMtb.Select();
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string sobre = "VERSÃO BETA!!!\nSistema desenvolvido especialmente para \"Calhas Bafuni\" por Richard Calderan.\n" +
                    "Contato: richardcck@hotmail.com (zap) 16997292729";
                MessageBox.Show(sobre, "Sobre");
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                statusSender("Uma cópia de segurança seria criada...");
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }

        
        
        private void orc1P0Tb_KeyDown(object sender, KeyEventArgs e)
       {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                	
                	int id;
                	
                	if (!int.TryParse(orc1ParIdLb.Text,out id))
                		id=-1;
                	float valor;
                	if (!float.TryParse(orc1P0Tb.Text,out valor))
                		valor=0;
                	if (id==-1)
                	{//adicoona novo
                		if (MessageBox.Show("Deseja adicionar este NOVO pagamento?","Adicionar",MessageBoxButtons.YesNo)== DialogResult.Yes)
                		{
                			adicionar(valor);
                		}
                	}
                	else{
                		if (MessageBox.Show("Deseja atualizar?","Atualizar",MessageBoxButtons.YesNo)== DialogResult.Yes)
                		{
                			atualizar(id,valor);
                		}
                	}
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
        }


		void Orc1PrazoDtpValueChanged(object sender, EventArgs e)
		{
			try
			{
				long cli,serv;
				if (!long.TryParse(orc1CcliLb.Text, out cli))
					cli=0;
				
				if (!long.TryParse(orc1IdLb.Text, out serv))
					serv=0;
				
				if (serv==0 ||cli==0)
					return;
				
				if (!Service.Update(serv,"data",orc1PrazoDtp.Value,conexao.GetConnection()))
				{
					carregaService(Service.Load(serv,conexao.GetConnection()));
					statusSender("não foi possível atualizar o prazo");
				}
				else{
					statusSender("Atualização concluida!",SystemColors.Window);
				}
				
				
			}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
	
		}
		void Orc1IdLbTextChanged(object sender, EventArgs e)
		{
	
		}
		void Orc1SalvarBtClick(object sender, EventArgs e)
		{
	
		}
		void Orc1StatusCbSelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				long id;
				
				if (long.TryParse(orc1IdLb.Text,out id))
				{
					
					if (!Service.Update(id,"status",orc1StatusCb.SelectedIndex, conexao.GetConnection()))
						statusSender("Não foi possivel salvar");
					else
					{
						statusSender("Status salvo!");
					}
				}
			}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
	
		}
		void Cad1GbEnter(object sender, EventArgs e)
		{
	
		}
		void BackupToolStripMenuItemClick(object sender, EventArgs e)
		{
			try
            {
				FolderBrowserDialog fd =new FolderBrowserDialog();
				
				if (fd.ShowDialog() == DialogResult.OK)
				{
					bkpPachLb.Text = fd.SelectedPath;
					if (Config.IniciarBackup(bkpPachLb.Text,conexao.GetConnection()))
						statusSender("Backup Realizado com sucesso! Salvo em \""+bkpPachLb.Text+"\"");
					else
						statusSender("Não foi possível realizar o backup! Escolha outro destino!");
				}
				
            }
			catch(Exception er)
            {
				MessageBox.Show("Não foi possível realizar o backup! Escolha outro destino! Cautch: "+er.Message);
            }
			
		}
		
		void Cad1TipoCbSelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (cad1TipoCb.SelectedIndex==0)
				{
					cad1CpfCnpjLb.Text="CPF";
					cad1CpfTb.Mask="000,000,000-00";
					
				}
				else
				{
					cad1CpfCnpjLb.Text="CNPJ";
					cad1CpfTb.Mask="00,000,000/0000-00";
				}
				long id;
				if(long.TryParse(cad1IdLb.Text,out id))
				{
					if (id==0)
						return;
					if (!Cliente.Update(id,"tipo",cad1TipoCb.SelectedIndex,conexao.GetConnection()))
					{
						statusSender("Não foi possível atualizar");
						carregaCliente(Cliente.Load(id,conexao.GetConnection()));
					}
					else
					{
						statusSender("Atualização realizada com sucesso");
					}
						
				}
			}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
	
		}
		void Cad1IdLbTextChanged(object sender, EventArgs e)
		{
			try{
				long id;
				if (long.TryParse(cad1IdLb.Text,out id))
				{
					if (id==0){
						cad1SalvarBt.Show();
						cad1ExcluirBt.Hide();
						cad1NewEndBt.Hide();
						cad1EndPn.Enabled=false;
						cad1AtualizarclienteBt.Visible =false;
						
					}
					else{
						cad1SalvarBt.Hide();
						cad1ExcluirBt.Show();
						cad1NewEndBt.Show();
						cad1EndPn.Enabled = true;
						cad1AtualizarclienteBt.Visible =true;
					}
				}
				else{
					cad1IdLb.Text="0";
					cad1SalvarBt.Show();
					cad1ExcluirBt.Hide();
					cad1EndPn.Enabled = false;
					cad1AtualizarclienteBt.Visible =false;
				}
				
			}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
		}
		void Cad1ExcluirBtClick(object sender, EventArgs e)
		{
	
			try
			{
				if (DialogResult.Yes== MessageBox.Show("Você deseja mesmo Excluir o cadastro do cliente?\nVocê irá perder todos os \"Endereços\" e \"Orçamentos\" vinculados a este cliente.\nDeseja Proceguir?","Excluir cliente",MessageBoxButtons.YesNo))
				{
					long id;
					if (long.TryParse(cad1IdLb.Text,out id))
					{
						if (!Cliente.Delete(id,conexao.GetConnection()))
						{
							statusSender("Não foi possível excluir cliente...");
						}
						else
						{
							limpaCad1();
							statusSender("Cliente excluído com sucesso!");
						}
					}
				}
				
			}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
		}
		
		private void carregaEnderecoEmCliente(long id_endereco)
		{
			try{
				Endereco end = Endereco.LoadId(id_endereco,conexao.GetConnection());
				if (end!=null)
				{
					cad1EndLb.Text = end.id.ToString();
					cad1CepMtb.Text = end.cep;
					cad1EnderecoTb.Text = end.logradouro;
					cad1NumeroTb.Text = end.numero.ToString();
					cad1TipologCb.Text = end.tipoLogradouro;
					cad1BairroTb.Text = end.bairro;
					cad1CidadeTb.Text = end.cidade;
					cad1UfTb.Text = end.uf;
					cad1ComplementoTb.Text = end.complemento;
				}
			}
			catch(Exception e)
			{
				statusSender(e.Message);
			}
		}
		void Cad1EnderecosCbSelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				long cli,endereco;
				if (long.TryParse(cad1IdLb.Text,out cli))
				{
					int indexof = cad1EnderecosCb.Text.IndexOf('-');
					if (indexof!=-1)
					{
						if (long.TryParse(cad1EnderecosCb.Text.Substring(0,indexof),out endereco))
						{
							carregaEnderecoEmCliente(endereco);
						}
					}
                    
				}
			}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
		}
		void Orc1EndCbSelectedIndexChanged(object sender, EventArgs e)
		{
			try{
				int indexof = orc1EndCb.Text.IndexOf('-');
				if (indexof!=-1)
				{
					long end_id,serv;
					if (long.TryParse(orc1EndCb.Text.Substring(0,indexof), out end_id))
					{
						Endereco endereco = Endereco.LoadId(end_id,conexao.GetConnection());
						if (endereco!=null)
						{
							if (long.TryParse(orc1IdLb.Text, out serv))
							{
								if (serv!=0)
									if (Service.Update(serv,"endereco",endereco.id,conexao.GetConnection()))
										statusSender("Endereço atualizado!");
							}
						}
						
					}
				}			
			}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
		}
		void Cad1NewEndBtClick(object sender, EventArgs e)
		{
			try{
				long cli;
				if (long.TryParse(cad1IdLb.Text,out cli))
				{
					if(cli!=0)
					{
						List<Endereco> all = Endereco.LoadCli(cli,conexao.GetConnection());
						if (all.Count>0)
						{
							foreach(Endereco end in all)
							{
								if((cad1EnderecoTb.Text==end.logradouro)&&(cad1NumeroTb.Text==end.numero.ToString()))
								{
									statusSender("Já existe um endereço igual!");
									return;
								}
							}
							
						}
						string erro = Endereco.New(cli,cad1TipologCb.Text,cad1CepMtb.Text,cad1EnderecoTb.Text,cad1NumeroTb.Text,cad1BairroTb.Text,cad1CidadeTb.Text,cad1UfTb.Text,cad1ComplementoTb.Text,conexao.GetConnection());
							if (erro==""){
								statusSender("Endereço adicionado com sucesso!");
								Cliente c = Cliente.Load(cli,conexao.GetConnection());
								//Endereco.New(c.id,cad1TipologCb.Text,cad1CepMtb.Text,cad1EnderecoTb.Text,cad1NumeroTb.Text,cad1BairroTb.Text,cad1CidadeTb.Text,cad1UfTb.Text,cad1ComplementoTb.Text,conexao.GetConnection());
								limpaService();
								orc1CcliLb.Text = cli.ToString();
								orc1NomeTb.Text = c.nome;
								if (c.tipo==0)
								{
									orc1CpfLb.Text="CPF";
									orc1CpfMtb.Mask="000,000,000-00";
					
								}
								else
								{
									orc1CpfLb.Text="CNPJ";
									orc1CpfMtb.Mask="00,000,000/0000-00";
								}
								orc1CpfMtb.Text = c.cpf;
								Endereco end = Endereco.Last(conexao.GetConnection());
								if (end!=null){
									foreach(Endereco en in c.endereco){
										orc1EndCb.Items.Add(en.id+"-"+en.tipoLogradouro+" "+en.logradouro+","+en.numero+". "+en.bairro+" "+en.cidade+"/"+en.uf);
									}
								//orc1EndCb.Items.Add(end.id+"-"+end.tipoLogradouro+" "+end.logradouro+","+end.numero+". "+end.bairro+" "+end.cidade+"/"+end.uf);
								orc1EndCb.Text = end.id+"-"+end.tipoLogradouro+" "+end.logradouro+","+end.numero+". "+end.bairro+" "+end.cidade+"/"+end.uf;
								}
								limpaCad1();
								mostraGb(orc1Gb);								
						}
							else
								statusSender(erro);
					}
				}
			}
			catch(Exception er)
			{
				statusSender(er.Message);
			}
	
		}
		void Cad1ObsTbLeave(object sender, EventArgs e)
		{
			try{
				long ccli;
				if(long.TryParse(cad1IdLb.Text,out ccli))
				{
					if (!Cliente.Update(ccli,"obs",cad1ObsTb.Text,conexao.GetConnection()))
					{
						statusSender("Não foi possível atualizar");
						carregaCliente(Cliente.Load(ccli,conexao.GetConnection()));
					}
					else
					{
						statusSender("Atualização realizada com sucesso");
					}
					
				}
				else
				{
					cad1SalvarBt.Select();
				}
								
			}
			catch(Exception er){
				statusSender(er.Message);
			}
		}
		void PendendênciasToolStripMenuItemClick(object sender, EventArgs e)
		{
			try{
				mostraGb(rel2Gb);
			}
			catch(Exception erro){
				statusSender(erro.Message);
			}
		}
		void Rel2XBtClick(object sender, EventArgs e)
		{
			try{
				limpaRel2();
				rel2Gb.Hide();
			}
			catch(Exception erro){
				statusSender(erro.Message);
			}
		}
		
		private void limpaRel2()
		{
			
			try{
				rel2Dg.Rows.Clear();
			}
			catch(Exception erro){
				statusSender(erro.Message);
			}
		}
		void Orc1CpfMtbKeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				long ccli;
				if(long.TryParse(orc1CcliLb.Text,out ccli))
				{
					if (!Cliente.Update(ccli,"cpf",orc1CpfMtb.Text,conexao.GetConnection()))
						statusSender("não foi possível atualizar cpf/cnpj...");
					else
						statusSender("Atualizado!");
					
				}
			}
			catch(Exception err)
			{
				statusSender(err.Message);
			}
		}
		void Rel2GbVisibleChanged(object sender, EventArgs e)
		{
			try{
				if (rel2Gb.Visible)
				{
					pendencias();
				}
				
			}
			catch(Exception erro){
				statusSender(erro.Message);
			}
	
		}
		private void pendencias()
		{
			try{
				rel2Dg.DataSource= Financa.Pendencias(rel1StatusPendenteCh.Checked, conexao.GetConnection());
			}
			catch(Exception e){
				statusSender(e.Message);
			}
		}
		void Button1Click(object sender, EventArgs e)
		{
	
		}
		
		private void atualizar(long parcela, float valor)
		{
			try{
				long cli,serv;
				
				if (!long.TryParse(orc1CcliLb.Text,out cli)){
					statusSender("Carregue o orçamento.");
					return;
				}
				
				if (!long.TryParse(orc1IdLb.Text,out serv)){
					statusSender("Carregue o orçamento.");
					return;
				}
				
				float sum_desc=0,sum_par=0,falta=0,aux;
				
				if (serv!=0){
					foreach(DataGridViewRow r in orc1Dg.Rows)
					{
						if (float.TryParse(r.Cells[2].Value.ToString(),out aux))
							sum_desc+=aux;
					}
					foreach(ListViewItem it in orc1ParcelasLv.Items)
					{
						if (float.TryParse(it.SubItems[0].Text,out aux)){
							if (it.Index==parcela)
								sum_par+=valor;
							else
								sum_par+=aux;
						}
					}
					if (sum_desc < sum_par){//adicionar nova descr
						
						if (valor!=0){
						if (ServiceDescricao.New(serv,orc1AddDescTb.Text,sum_par-sum_desc,conexao.GetConnection())==""){
							ServiceDescricao des = ServiceDescricao.GetLast(conexao.GetConnection());
							if (Financa.NewOrUpdate(parcela,cli,serv,(double)valor,Financa.setTipo(orc1F0Cb.SelectedIndex),DateTime.Now,orc1P0Dtp.Value,conexao.GetConnection())==""){
								DataGridViewButtonCell bt = new DataGridViewButtonCell();
								string id_desc="0";
								if (des!=null)
									id_desc = des.id.ToString();
								object[] vals = {id_desc, "", (sum_par-sum_desc).ToString("F2"), bt };
								orc1Dg.Rows.Add(vals);
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Parcela "+(parcela+1) +" foi atualizada com sucesso!");
							}else{
								ServiceDescricao.Remove(des.id,conexao.GetConnection());
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Não foi possível salvar novo valor...");
							}
						}
						else{
							carregaService(Service.Load(serv,conexao.GetConnection()));
							statusSender("Não foi possível salvar novo valor...");
						}
						}else{
							if (Financa.Delete((int)parcela,serv,conexao.GetConnection())){
								if (ServiceDescricao.New(serv,orc1AddDescTb.Text,sum_par-sum_desc,conexao.GetConnection())==""){
									ServiceDescricao des = ServiceDescricao.GetLast(conexao.GetConnection());
									DataGridViewButtonCell bt = new DataGridViewButtonCell();
									string id_desc="0";
									if (des!=null)
										id_desc = des.id.ToString();
									object[] vals = {id_desc, "", (sum_par-sum_desc).ToString("F2"), bt };
									orc1Dg.Rows.Add(vals);
									carregaService(Service.Load(serv,conexao.GetConnection()));
									statusSender("Parcela "+(parcela+1) +" foi atualizada com sucesso!");
									
								}
								else{
									carregaService(Service.Load(serv,conexao.GetConnection()));
									statusSender("O valor foi adicionado mas a diferença não pode ser corrigida. Faça manualmente.");
								}
								if (parcelaAdicionar(cli,serv,sum_par-sum_desc)){
									orc1ParcelasLv.Items.RemoveAt((int)parcela);
									carregaService(Service.Load(serv,conexao.GetConnection()));
									statusSender("As alterações necessárias foram realizadas com sucesso!");
								}
								else{
									carregaService(Service.Load(serv,conexao.GetConnection()));
									statusSender("A parcela foi removida, mas não foi possível adicionar a diferença. Faça isso manualmente.");
								}
							}
							else{
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Não foi possível atualizar.");
								return;
							}
						}
					}
					else
						if (sum_desc > sum_par){
						if (valor!=0){
							if (Financa.NewOrUpdate(parcela,cli,serv,(double)valor,Financa.setTipo(orc1F0Cb.SelectedIndex),DateTime.Now,orc1P0Dtp.Value,conexao.GetConnection())==""){
								if (parcelaAdicionar(cli,serv,sum_desc-sum_par)){
									carregaService(Service.Load(serv,conexao.GetConnection()));
									statusSender("As alterações necessárias foram realizadas com sucesso!");
								}
								else{
									carregaService(Service.Load(serv,conexao.GetConnection()));
									statusSender("A parcela foi atualizada, mas não foi possível adicionar a diferença. Faça isso manualmente.");
								}
							}else{
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Não foi possível atualizar.");
								return;
							}
						}
						else{
							if (Financa.Delete((int)parcela,serv,conexao.GetConnection())){
								if (parcelaAdicionar(cli,serv,sum_desc-sum_par)){
									orc1ParcelasLv.Items.RemoveAt((int)parcela);
									carregaService(Service.Load(serv,conexao.GetConnection()));
									statusSender("As alterações necessárias foram realizadas com sucesso!");
								}
								else{
									carregaService(Service.Load(serv,conexao.GetConnection()));
									statusSender("A parcela foi removida, mas não foi possível adicionar a diferença. Faça isso manualmente.");
								}
							}
							else{
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Não foi possível atualizar.");
								return;
							}
						}
					}else{
						if (valor!=0){
							if (Financa.NewOrUpdate(parcela,cli,serv,(double)valor,Financa.setTipo(orc1F0Cb.SelectedIndex),DateTime.Now,orc1P0Dtp.Value,conexao.GetConnection())==""){
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Parcela "+(parcela+1) +" foi atualizada com sucesso!");
							}
							else{
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Não foi possível salvar novo valor...");
							}
						}
						else{
							if (Financa.Delete((int)parcela,serv,conexao.GetConnection())){carregaService(Service.Load(serv,conexao.GetConnection()));
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Parcela "+(parcela+1) +" foi removida com sucesso!");
							}else{
								carregaService(Service.Load(serv,conexao.GetConnection()));
								statusSender("Não foi possível salvar novo valor...");
							}
						}
					}
				}
				else{
					foreach(DataGridViewRow r in orc1Dg.Rows)
					{
						if (float.TryParse(r.Cells[2].Value.ToString(),out aux))
							sum_desc+=aux;
					}
					foreach(ListViewItem it in orc1ParcelasLv.Items)
					{
						if (float.TryParse(it.SubItems[0].Text,out aux)){
							if (it.Index==parcela)
								sum_par+=valor;
							else
								sum_par+=aux;
						}
					}
					    
					if (sum_desc < sum_par){
						DataGridViewButtonCell bt = new DataGridViewButtonCell();
						object[] vals = {"", "", (sum_par-sum_desc).ToString("F2"), bt };
						orc1Dg.Rows.Add(vals);
					}
					else{
						if (sum_desc > sum_par){
							if (valor!=0){
								orc1ParcelasLv.Items[(int)parcela].SubItems[0].Text = valor.ToString("F2");
								orc1ParcelasLv.Items[(int)parcela].SubItems[1].Text = orc1P0Dtp.Value.ToString("dd/MM/yyyy");
								orc1ParcelasLv.Items[(int)parcela].SubItems[2].Text = orc1F0Cb.Text;
							}
							else{
								orc1ParcelasLv.Items.RemoveAt((int)parcela);
							}
							parcelaAdicionar(cli,serv,sum_desc-sum_par);
						}
						else{
							if (valor!=0){
								orc1ParcelasLv.Items[(int)parcela].SubItems[0].Text = valor.ToString("F2");
								orc1ParcelasLv.Items[(int)parcela].SubItems[1].Text = orc1P0Dtp.Value.ToString("dd/MM/yyyy");
								orc1ParcelasLv.Items[(int)parcela].SubItems[2].Text = orc1F0Cb.Text;
							}
							else{
								orc1ParcelasLv.Items.RemoveAt((int)parcela);
							}
						}
					}
					
					
					
				}
					
				
				orc1ParIdLb.Text="-1";
				orc1P0Tb.Text="0,00";
				orc1P0Dtp.Value=DateTime.Today;
				orc1F0Cb.SelectedIndex = 0;
				falta=0;
				sum_desc=0;sum_par=0;
				foreach(ListViewItem it in orc1ParcelasLv.Items)
				{
					if (float.TryParse(it.SubItems[0].Text,out aux))
					{
						if (it.SubItems[2].Text=="Pendente")
							falta+=aux;
						else
							sum_par+=aux;
					}
				}
				orc1TotalLb.Text = (sum_par+falta).ToString("F2");
				orc1PagouLb.Text = (sum_par).ToString("F2");
				orc1RestaLb.Text = falta.ToString("F2");
			}
			catch{}
		}
		
		private void adicionar(float valor)
		{
			try{			
				if (valor<=0){
					statusSender("Escolha um valor maior que 0 para poder inserir.");
					return;
				}
				long cli,serv;
				
				if (!long.TryParse(orc1CcliLb.Text,out cli)){
					statusSender("Carregue o orçamento.");
					return;
				}
				
				if (!long.TryParse(orc1IdLb.Text,out serv)){
					statusSender("Carregue o orçamento.");
					return;
				}
				
				float sum_desc=0,sum_par=0,aux;
				
				
				if (serv!=0){
					sum_par=valor;
					foreach(DataGridViewRow r in orc1Dg.Rows)
					{
						if (float.TryParse(r.Cells[2].Value.ToString(),out aux))
							sum_desc+=aux;
					}
					foreach(ListViewItem it in orc1ParcelasLv.Items)
					{
						if (float.TryParse(it.SubItems[0].Text,out aux))
							sum_par+=aux;
					}
					if (sum_desc < sum_par){//adicionar nova descr
						if (parcelaAdicionar(cli,serv,valor)){
							DataGridViewButtonCell bt = new DataGridViewButtonCell();
							ServiceDescricao.New(serv,orc1AddDescTb.Text,sum_par+valor-sum_desc,conexao.GetConnection());
							ServiceDescricao des = ServiceDescricao.GetLast(conexao.GetConnection());
							string id_desc="0";
							if (des!=null)
								id_desc = des.id.ToString();
							object[] vals = {id_desc, "Deferença de valor", (sum_par+valor-sum_desc).ToString("F2"), bt };
							orc1Dg.Rows.Add(vals);
							carregaService(Service.Load(serv,conexao.GetConnection()));
						}
						else{
							carregaService(Service.Load(serv,conexao.GetConnection()));
							statusSender("Não foi possível salvar novo valor...");
						}
					}
					else{
						if (sum_desc > sum_par - valor){
							MessageBox.Show("A soma das parcelas está menor que a soma dos itens deste orçamento.");
							carregaService(Service.Load(serv,conexao.GetConnection()));
						}
					}
				}
				else{
					foreach(DataGridViewRow r in orc1Dg.Rows)
					{
						if (float.TryParse(r.Cells[2].Value.ToString(),out aux))
							sum_desc+=aux;
					}
					foreach(ListViewItem it in orc1ParcelasLv.Items)
					{
						if (float.TryParse(it.SubItems[0].Text,out aux))
							sum_par+=aux;
					}
					if (sum_desc < sum_par){
						DataGridViewButtonCell bt = new DataGridViewButtonCell();
						object[] vals = {"", "Deferença de valor", (sum_par-sum_desc).ToString("F2"), bt };
						orc1Dg.Rows.Add(vals);
					}
					else{
						if (sum_desc > sum_par){
							ListViewItem it = new ListViewItem(new String[]{(sum_desc-sum_par).ToString(),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"});
							orc1ParcelasLv.Items.Add(it);
						}
						else{
							DataGridViewButtonCell bt = new DataGridViewButtonCell();
							object[] vals = {"", "Deferença de valor", (valor).ToString("F2"), bt };
							orc1Dg.Rows.Add(vals);
							ListViewItem it = new ListViewItem(new String[]{(valor).ToString("F2"),orc1P0Dtp.Value.ToString("dd/MM/yyyy"),orc1F0Cb.Text});
							orc1ParcelasLv.Items.Add(it);
						}
					
					}
				}
				orc1ParIdLb.Text="-1";
				orc1P0Tb.Text="0,00";
				orc1P0Dtp.Value=DateTime.Today;
				orc1F0Cb.SelectedIndex = 0;
				float falta=0;
				sum_desc=0;sum_par=0;
				foreach(ListViewItem it in orc1ParcelasLv.Items)
				{
					if (float.TryParse(it.SubItems[0].Text,out aux))
					{
						if (it.SubItems[2].Text=="Pendente")
							falta+=aux;
						else
							sum_par+=aux;
					}
				}
				orc1TotalLb.Text = (sum_par+falta).ToString("F2");
				orc1PagouLb.Text = (sum_par).ToString("F2");
				orc1RestaLb.Text = falta.ToString("F2");
				
			}
			catch{}
		}
		void Orc1AddParBtClick(object sender, EventArgs e)
		{
			try{
				
				int id;
				
				if (!int.TryParse(orc1ParIdLb.Text,out id))
					id=-1;
				float valor;
				if (!float.TryParse(orc1P0Tb.Text,out valor))
					valor=0;
				if (id==-1)
				{//adicoona novo
					if (MessageBox.Show("Deseja adicionar este NOVO pagamento?","Adicionar",MessageBoxButtons.YesNo)== DialogResult.Yes)
					{
						adicionar(valor);
					}
				}
				else{
					if (MessageBox.Show("Deseja atualizar?","Atualizar",MessageBoxButtons.YesNo)== DialogResult.Yes)
					{
						atualizar(id,valor);
					}
				}
	
			}catch{}
		}
		
		
		void Orc1F1CbSelectedIndexChanged(object sender, EventArgs e)
		{
			try{
			}
			catch(Exception er){statusSender(er.Message);}
		}
		void Button4Click(object sender, EventArgs e)
		{
			Cliente c = Cliente.Load(long.Parse(cad1IdLb.Text),conexao.GetConnection());
			if (c!=null){
				MessageBox.Show(c.tipo.ToString());
			}
	
		}
		void Cad1ExcluirEndBtClick(object sender, EventArgs e)
		{
			try{
				int index = cad1EnderecosCb.Text.IndexOf('-');
				if (index!=-1)
				{
					long id;
					if (long.TryParse(cad1EnderecosCb.Text.Substring(0,index),out id))
					{
						if (MessageBox.Show("Tem certeza que deseja excluir o endereço \""+cad1EnderecosCb.Text+"\" do banco de dados?","Excluir endereço", MessageBoxButtons.YesNo) == DialogResult.Yes)
						{
							string erro = Endereco.Delete(id,conexao.GetConnection());
							if (erro=="")
								statusSender("Endereço excluido com sucesso!");
							else
								statusSender("Erro ao tentar exclir endereço: "+ erro);
							carregaCliente(Cliente.Load(long.Parse(orc1IdLb.Text),conexao.GetConnection()));
						}
					}
				}
				
			}
			catch(Exception er){
				statusSender(er.Message);
			}
		}
		
		void Orc1CodigoMtbLeave(object sender, EventArgs e)
		{
			try
            {
                long id;
                if (long.TryParse(orc1CodigoMtb.Text, out id))
                {
                	Service service = Service.LoadByBafuniCod(id, conexao.GetConnection());
                	if (service!=null)
                		carregaService(service);
                	else
                	{
                		atualizaService();
                	}
                }
            }
			catch(Exception er)
			{
				statusSender(er.Message);
			}
		}
		void Rel2DgCellClick(object sender, DataGridViewCellEventArgs e)
		{
			try{
				if (rel2Dg.Columns.Count>0){
					long id;
					if(long.TryParse(rel2Dg.Rows[e.RowIndex].Cells["ID"].Value.ToString(),out id)){
						carregaService(Service.Load(id,conexao.GetConnection()));
						mostraGb(orc1Gb);
						statusSender("Orçamento carregado com sucesso!");
					}
				}
			}catch{}
				
		}
		void Rel1StatusPendenteChCheckedChanged(object sender, EventArgs e)
		{
			rel2Dg.DataSource= Financa.Pendencias(rel1StatusPendenteCh.Checked, conexao.GetConnection());
		}
		
		void Cad1AtualizarEndBtClick(object sender, EventArgs e)
		{
			try{
				long id;
				int index = cad1EnderecosCb.Text.IndexOf('-');
				if (index!=-1)
				{
					if (long.TryParse(cad1EnderecosCb.Text.Substring(0,index),out id))
					{
						string erro = Endereco.Update(id,cad1CepMtb.Text,cad1TipologCb.Text,cad1EnderecoTb.Text,cad1NumeroTb.Text,cad1BairroTb.Text,cad1CidadeTb.Text,cad1UfTb.Text,
						                              cad1ComplementoTb.Text,conexao.GetConnection());
						if (erro=="")
							statusSender("Endereço atualizado com sucesso!");
						else
						{
							statusSender(erro);
							long ccli;
							if (long.TryParse(cad1IdLb.Text,out ccli)){
								carregaCliente(Cliente.Load(ccli,conexao.GetConnection()));
							}
							statusSender(erro);
						}
						
					}
				}
			}
			catch(Exception ex){
				statusSender(ex.Message);
			}
		}
		void BkpAlterarBtClick(object sender, EventArgs e)
		{
			try{
				FolderBrowserDialog dial = new FolderBrowserDialog();
				if (dial.ShowDialog()== DialogResult.OK)
				{
					bkpPachLb.Text = dial.SelectedPath;
					if (Config.Save(bkpOnCloseCh.Checked,bkpPachLb.Text,false,false,false,conexao.GetConnection()))
						statusSender("Alterações salvas com sucesso");
					else
					{
						Config c = Config.Load(conexao.GetConnection());
						if (c!=null)
						{
							bkpOnCloseCh.Checked = c.BackupOnClose;
							bkpPachLb.Text = c.BackupPath;
						}
						statusSender("Falha ao tentar salvar alterações.");
					}
				}
			}catch{
				statusSender("Não foi possivel salvar. Cautch: BkpAlterarBtClick");
			}
	
		}
		void ConfiguraçõesDeBackupToolStripMenuItemClick(object sender, EventArgs e)
		{
			mostraGb(bkpGb);
		}
		void BkpXBtClick(object sender, EventArgs e)
		{
			bkpGb.Hide();
		}
		void MainFormFormClosed(object sender, FormClosedEventArgs e)
		{
			if (bkpOnCloseCh.Checked)
				Config.IniciarBackup(bkpPachLb.Text,conexao.GetConnection());
		}
		void MainFormSizeChanged(object sender, EventArgs e)
		{
			try{
				reposicionarGb();
			}catch{}
	
		}
		private void reposicionarGb(){
			try{
				Point p;
				foreach(GroupBox gb in gbs){
					p=new Point(gb.Parent.Width/2 - gb.Width/2,gb.Location.Y);
					gb.Location=p;
				}
				
			}catch{}
		}
		void BackupRápidoToolStripMenuItemClick(object sender, EventArgs e)
		{
			try{
				if (Config.IniciarBackup(bkpPachLb.Text,conexao.GetConnection()))
					statusSender("Backup Realizado com sucesso! Salvo em \""+bkpPachLb.Text+"\"");
				else
					statusSender("Não foi possível realizar o backup! Vá em \"configurações de backup\" e mude o destino.");
				
			}catch{statusSender("Não foi possível realizar o backup!  Vá em \"configurações de backup\" e mude o destino. Cautch BackupRápidoToolStripMenuItemClick");}
	
		}
		void BkpOnCloseChCheckedChanged(object sender, EventArgs e)
		{
			try{
				if (Config.Save(bkpOnCloseCh.Checked,bkpPachLb.Text,false,false,false,conexao.GetConnection()))
					statusSender("Alteração feita com sucesso!");
				else
					statusSender("Erro ao tentar salvar alterações: Backup.Save()");
				
			}catch{statusSender("Não foi possivel salvar. Cautch: Backup.Save()");}
		}
		void Orc1ParcelasLvItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			try
			{
				orc1ParIdLb.Text = e.ItemIndex.ToString();
				float valor;
				if (float.TryParse(e.Item.SubItems[0].Text,out valor))
				{
					orc1P0Tb.Text = valor.ToString("F2");
				}
				DateTime data;
				if (DateTime.TryParse(e.Item.SubItems[1].Text,out data))
				{
					orc1P0Dtp.Value = data;
				}
				orc1F0Cb.SelectedIndex = (int)Financa.TipoFormasStringToId(e.Item.SubItems[2].Text);
				orc1P0Tb.Select();
			}
			catch(Exception er){
				statusSender(er.Message);
			}
	
		}
		void Orc1SomaParBtClick(object sender, EventArgs e)
		{
			try
			{
				double sum=0,aux;
				long servico,cliente;
				if (!long.TryParse(orc1IdLb.Text,out servico)){
					statusSender("Erro. Carregue o orçamento de novo!");
					return;
				}
				if (!long.TryParse(orc1CcliLb.Text,out cliente)){
					statusSender("Erro. Carregue o orçamento de novo!");
					return;
				}
				
				if (servico !=0 ){
					foreach(ListViewItem lv in orc1ParcelasLv.SelectedItems){
						if (!double.TryParse(lv.SubItems[0].Text,out aux))
							aux=0;
						if (Financa.Delete(lv.Index,servico,conexao.GetConnection())){
							orc1ParcelasLv.Items.RemoveAt(lv.Index);
							sum+=aux;
						}
					}
					if (sum!=0 && "" == Financa.NewOrUpdate(orc1ParcelasLv.Items.Count,cliente,servico,sum,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection())){
						ListViewItem novo = new ListViewItem(new String[]{sum.ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"});
						orc1ParcelasLv.Items.Add(novo);
					}
					else
						statusSender("erro ao incluir valor");
				}else{
					foreach(ListViewItem lv in orc1ParcelasLv.SelectedItems){
						if (!double.TryParse(lv.SubItems[0].Text,out aux))
							aux=0;
						orc1ParcelasLv.Items.RemoveAt(lv.Index);
						sum+=aux;
					}
					if (sum!=0){
						ListViewItem novo = new ListViewItem(new String[]{sum.ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"});
						orc1ParcelasLv.Items.Add(novo);
					}
				}
				
				float parPg=0,parFalta=0, aux_itemVal;
				foreach(ListViewItem it in orc1ParcelasLv.Items)
				{
					if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
						if (it.SubItems[2].Text!="Pendente")
							parPg+=aux_itemVal;
						else
							parFalta+=aux_itemVal;
					}
				}
				
				orc1TotalLb.Text = (parPg+parFalta).ToString("F2");
				orc1PagouLb.Text = parPg.ToString("F2");
				orc1RestaLb.Text = parFalta.ToString("F2");
				
				orc1AddIndexLb.Text = "-1";
				orc1P0Tb.Text = "0,00";
				orc1F0Cb.SelectedIndex = 0;
				orc1P0Dtp.Value = DateTime.Today;
			}
			catch(Exception er){
				statusSender("Cautch: Orc1SomaParBtClick. " + er.Message);
			}
		}
		
		void Orc1DgUserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			MessageBox.Show("deleting row");
		}
		
		void Orc1ParIdLbTextChanged(object sender, EventArgs e)
		{
			try{
			if (orc1ParIdLb.Text=="-1")
			{
				orc1P0Tb.Text="0,00";
				orc1P0Dtp.Value = DateTime.Today;
				orc1F0Cb.SelectedIndex = 0;
				orc1AddParBt .Text = "Adicionar Novo";
			}
			else{
				int i=-1;
				int.TryParse(orc1ParIdLb.Text,out i);
				i++;
				orc1AddParBt.Text = "Atualizar parcela "+i;
			}
				
			}catch(Exception er){statusSender("Cautch: Orc1ParIdLbTextChanged. " +er.Message);}
		}
		
		
        private void alterarDescricao(int index,string descricao,float valor){
        	try{
				orc1Dg.Rows[index].Cells[1].Value = descricao;
				orc1Dg.Rows[index].Cells[2].Value = valor.ToString("F2");
				
        		float tot_par=0,tot_desc=0, aux_itemVal,restante;
        		foreach(ListViewItem it in orc1ParcelasLv.Items)
				{
					if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
						tot_par+=aux_itemVal;
					}
				}
				foreach(DataGridViewRow r in orc1Dg.Rows){
					if (float.TryParse(r.Cells[2].Value.ToString(),out aux_itemVal)){
        				tot_desc+=aux_itemVal;
					}
				}
				long serv;
				if (!long.TryParse(orc1IdLb.Text,out serv))
					serv=0;
				long ccli;
				if(!long.TryParse(orc1CcliLb.Text,out ccli))
					ccli=0;
				
				if (serv==0){
					if(tot_desc<tot_par){
						restante = tot_par-tot_desc;
						for(int i=orc1ParcelasLv.Items.Count-1;i>=0;i--){
							if(float.TryParse(orc1ParcelasLv.Items[i].SubItems[0].Text,out aux_itemVal)){
								if(restante-aux_itemVal>0){
									orc1ParcelasLv.Items.RemoveAt(i);
									restante-=aux_itemVal;
								}else if(restante - aux_itemVal <0){
									restante-=aux_itemVal;
									orc1ParcelasLv.Items[i].SubItems[0].Text = (restante*-1).ToString("F2");
									break;
								}else{
									orc1ParcelasLv.Items.RemoveAt(i);
									break;
								}
							}
						}
						
					}else if (tot_desc>tot_par){
						orc1ParcelasLv.Items.Add(new ListViewItem(new string[]{(tot_desc-tot_par).ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"}));
					}
				}else{
					if(tot_desc<tot_par){
						
						restante = tot_par-tot_desc;
						for(int i=orc1ParcelasLv.Items.Count-1;i>=0;i--){
							if(float.TryParse(orc1ParcelasLv.Items[i].SubItems[0].Text,out aux_itemVal)){
								if(restante-aux_itemVal>0){
									if(Financa.Delete(i,serv,conexao.GetConnection())){
										orc1ParcelasLv.Items.RemoveAt(i);
										restante-=aux_itemVal;
									}
								}else if(restante - aux_itemVal <0){
										restante-=aux_itemVal;
									if(Financa.NewOrUpdate(i,ccli,serv,restante*-1,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection())==""){
										orc1ParcelasLv.Items[i].SubItems[0].Text = (restante*-1).ToString("F2");
										break;
									}
								}else{
									if(Financa.Delete(i,serv,conexao.GetConnection())){
									orc1ParcelasLv.Items.RemoveAt(i);
									break;
									}
								}
							}
						}
						if(restante>0)
							if(Financa.NewOrUpdate(orc1ParcelasLv.Items.Count,ccli,serv,restante,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection())==""){
							carregaService(Service.Load(serv,conexao.GetConnection()));
						}
						
					}else if (tot_desc>tot_par){
						if(Financa.NewOrUpdate(orc1ParcelasLv.Items.Count,ccli,serv,tot_desc-tot_par,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection())==""){
							carregaService(Service.Load(serv,conexao.GetConnection()));
						}
					}else{
						
					}
				}
				
				foreach(ListViewItem it in orc1ParcelasLv.Items)
				{
					if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
						if (it.SubItems[2].Text!="Pendente")
							tot_par+=aux_itemVal;
						else
							tot_desc+=aux_itemVal;
					}
				}
				
				orc1TotalLb.Text = (tot_par+tot_desc).ToString("F2");
				orc1PagouLb.Text = tot_par.ToString("F2");
				orc1RestaLb.Text = tot_desc.ToString("F2");
				
				orc1AddIndexLb.Text = "-1";
				orc1P0Tb.Text = "0,00";
				orc1F0Cb.SelectedIndex = 0;
				orc1P0Dtp.Value = DateTime.Today;
        		
        	}catch(Exception e){
        		statusSender(e.Message);
        	}
        	
        }
		
		public void corrigeValores()
		{
			try{
				float tot_par=0,tot_desc=0, aux_itemVal;
				long serv;
				if (!long.TryParse(orc1IdLb.Text,out serv))
					serv=0;
				
				foreach(ListViewItem it in orc1ParcelasLv.Items)
				{
					if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
						tot_par+=aux_itemVal;
					}
				}
				foreach(DataGridViewRow r in orc1Dg.Rows){
					if (float.TryParse(r.Cells[2].Value.ToString(),out aux_itemVal)){
						tot_desc+=aux_itemVal;
					}
				}
				
				if (serv==0){
					if (tot_desc<tot_par){
						orc1Dg.Rows.Add(new string[]{"","",(tot_par-tot_desc).ToString("F2")});
						
					}else if (tot_desc > tot_par){
						ListViewItem lv = new ListViewItem(new String[]{(tot_desc-tot_par).ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"});
						orc1ParcelasLv.Items.Add(lv);
					}
				}
				else{
					if (tot_desc<tot_par){
						if( ServiceDescricao.New(serv,"Deferença de parcela",tot_par-tot_desc,conexao.GetConnection())==""){
							ServiceDescricao last = ServiceDescricao.GetLast(conexao.GetConnection());
							if (last!=null){
								orc1Dg.Rows.Add(new string[]{last.id.ToString(),last.descricao,last.valor.ToString("F2")});
							}
						}
					}
					else if (tot_desc > tot_par){
						long cli;
						if (!long.TryParse(orc1CcliLb.Text,out cli))
							cli=0;
						if (Financa.NewOrUpdate(orc1ParcelasLv.Items.Count,cli,serv,tot_desc-tot_par,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection())==""){
							ListViewItem lv = new ListViewItem(new String[]{(tot_desc-tot_par).ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"});
							orc1ParcelasLv.Items.Add(lv);
						}
						
					}
				}
				foreach(ListViewItem it in orc1ParcelasLv.Items)
				{
					if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
						if (it.SubItems[2].Text!="Pendente")
							tot_par+=aux_itemVal;
						else
							tot_desc+=aux_itemVal;
					}
				}
				
				orc1TotalLb.Text = (tot_par+tot_desc).ToString("F2");
				orc1PagouLb.Text = tot_par.ToString("F2");
				orc1RestaLb.Text = tot_desc.ToString("F2");
				
				orc1AddIndexLb.Text = "-1";
				orc1P0Tb.Text = "0,00";
				orc1F0Cb.SelectedIndex = 0;
				orc1P0Dtp.Value = DateTime.Today;
			}catch(Exception e){
				statusSender("Corrige valores exception: "+e.Message);
			}
		}
		
		
        private void removeDescricao(int index){
        	try{
        		float tot_par=0,tot_desc=0, aux_itemVal,restante;
				long serv,ccli;
				if (!long.TryParse(orc1IdLb.Text,out serv))
					serv=0;
				if(!long.TryParse(orc1CcliLb.Text,out ccli))
					ccli=0;
				orc1Dg.Rows.RemoveAt(index);
				foreach(ListViewItem it in orc1ParcelasLv.Items)
				{
					if (float.TryParse(it.SubItems[0].Text,out aux_itemVal)){
						tot_par+=aux_itemVal;
					}
				}
				foreach(DataGridViewRow r in orc1Dg.Rows){
					if (float.TryParse(r.Cells[2].Value.ToString(),out aux_itemVal)){
						tot_desc+=aux_itemVal;
					}
				}
				if(serv==0){
					if (tot_desc<tot_par){
						restante = tot_par-tot_desc;
						for(int i=orc1ParcelasLv.Items.Count-1;i>=0;i--){
							if(float.TryParse(orc1ParcelasLv.Items[i].SubItems[0].Text,out aux_itemVal)){
								if(restante-aux_itemVal>0){
									orc1ParcelasLv.Items.RemoveAt(i);
									restante-=aux_itemVal;
								}else if(restante - aux_itemVal <0){
									restante-=aux_itemVal;
									orc1ParcelasLv.Items[i].SubItems[0].Text = (restante*-1).ToString("F2");
									break;
								}else{
									restante-=aux_itemVal;
									orc1ParcelasLv.Items.RemoveAt(i);
									break;
								}
							}
						}
						if(restante>0)
							orc1ParcelasLv.Items.Add(new ListViewItem(new String[]{restante.ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"}));
					}else if (tot_desc > tot_par){
						orc1ParcelasLv.Items.Add(new ListViewItem(new String[]{(tot_desc-tot_par).ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"}));
					
					}
				}
				else{
					List<ServiceDescricao> sds = ServiceDescricao.GetAllFrom(serv,conexao.GetConnection());
					long idDescr=sds[index].id;
					ServiceDescricao.Remove(idDescr,conexao.GetConnection());
					if (tot_desc<tot_par){
						restante = tot_par-tot_desc;
						for(int i=orc1ParcelasLv.Items.Count-1;i>=0;i--){
							if(float.TryParse(orc1ParcelasLv.Items[i].SubItems[0].Text,out aux_itemVal)){
								if(restante-aux_itemVal>0){
									if(Financa.Delete(i,serv,conexao.GetConnection())){
										orc1ParcelasLv.Items.RemoveAt(i);
										restante-=aux_itemVal;
									}
								}else if(restante - aux_itemVal <0){
										restante-=aux_itemVal;
									if(Financa.NewOrUpdate(i,ccli,serv,restante*-1,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection())==""){
										orc1ParcelasLv.Items[i].SubItems[0].Text = (restante*-1).ToString("F2");
										break;
									}
								}else{
									if(Financa.Delete(i,serv,conexao.GetConnection())){
										restante-=aux_itemVal;
										orc1ParcelasLv.Items.RemoveAt(i);
										break;
									}
								}
							}
						}
						if(restante>0){
							if(Financa.NewOrUpdate(orc1ParcelasLv.Items.Count,ccli,serv,restante,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection())==""){
								orc1ParcelasLv.Items.Add(new ListViewItem(new String[]{restante.ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"}));
							}
						}
					}else if (tot_desc > tot_par){
						if(Financa.NewOrUpdate(orc1ParcelasLv.Items.Count,ccli,serv,tot_desc-tot_par,Financa.TipoDeFormas.Pendente,DateTime.Now,DateTime.Today,conexao.GetConnection())==""){
							orc1ParcelasLv.Items.Add(new ListViewItem(new String[]{(tot_desc-tot_par).ToString("F2"),DateTime.Today.ToString("dd/MM/yyyy"),"Pendente"}));
						}
					}
				}
				
				
        	}catch(Exception e){
        		statusSender(e.Message);
        	}
        }
		
		void Orc1ExcluiParBtClick(object sender, EventArgs e)
		{
			try{
				if (MessageBox.Show("Deseja excluir a parcela?","Excluir",MessageBoxButtons.YesNo)== DialogResult.Yes){
					long serv,ccli;
					if (!long.TryParse(orc1IdLb.Text,out serv))
						serv=0;
					if (!long.TryParse(orc1CcliLb.Text,out ccli))
						ccli=0;
					foreach(ListViewItem i in orc1ParcelasLv.SelectedItems){
						parcelaRemover(i.Index,serv);
					}
					carregaService(Service.Load(serv,conexao.GetConnection()));
					float total=0,pago=0,aux;
					foreach(ListViewItem i in orc1ParcelasLv.Items){
						if(float.TryParse(i.SubItems[0].Text,out aux)){
							total+=aux;
							if(i.SubItems[2].Text!="Pendente")
								pago+=aux;
						}
							
					}
					orc1TotalLb.Text = total.ToString("F2");
					orc1PagouLb.Text = pago.ToString("F2");
					orc1RestaLb.Text = (total-pago).ToString("F2");
					//corrigeValores();
				}
			}catch{}
		}
		
		void Cad1EndLbTextChanged(object sender, EventArgs e)
		{
			try{
				if(cad1EndLb.Text!="-1")
					cad1EndAtualizarBt.Enabled  = true;
				else
					cad1EndAtualizarBt.Enabled  = false;
					
				
			}catch{}
	
		}
		void Cad1EndAtualizarBtClick(object sender, EventArgs e)
		{
			try{
				long id;
				if (DialogResult.Yes == MessageBox.Show("Deseja Atualizar o endereço "+cad1EnderecoTb.Text+"?","Atualizar",MessageBoxButtons.YesNo)){
					if (long.TryParse(cad1EndLb.Text,out id)){
						if(""== Endereco.Update(id,cad1CepMtb.Text,cad1TipologCb.Text,cad1EnderecoTb.Text,cad1NumeroTb.Text,cad1BairroTb.Text,cad1CidadeTb.Text,cad1UfTb.Text,cad1ComplementoTb.Text,
						                        conexao.GetConnection())){
							statusSender("Endereço atualizado com sucesso!");
						}else{
							carregaEnderecoEmCliente(id);
							statusSender("Não foi possível atualizar o endereço");
						}
					}
				}
				
			}catch{}
		}
		void Cad1GbVisibleChanged(object sender, EventArgs e)
		{
			try{
				
				
			}catch{}
	
		}
		void Cad1AtualizarclienteBtClick(object sender, EventArgs e)
		{
			try{
				long id;
				if (long.TryParse(cad1IdLb.Text,out id)){
					if(DialogResult.Yes == MessageBox.Show("Deseja Atualizar o cliente?","Atualizar",MessageBoxButtons.YesNo)){
						string res = Cliente.Update(id,cad1TipoCb.SelectedIndex,cad1CpfTb.Text,cad1NomeTb.Text,cad1EmailTb.Text,
						               cad1Fone1Tb.Text,cad1Fone2Tb.Text,cad1ObsTb.Text,conexao.GetConnection());
						carregaCliente(Cliente.Load(id,conexao.GetConnection()));
						if (res==""){
							statusSender("Cliente atualizado com sucesso!");
						}else{
							statusSender("Não foi possível atualizar: "+res);
						}
					}
				}
				
			}catch{}
		}
		void Orc1ExcluirOrcBrClick(object sender, EventArgs e)
		{
	
			try{
				long id;
				if (long.TryParse(orc1IdLb.Text,out id)){
					if(DialogResult.Yes == MessageBox.Show("Deseja EXCLUIR este orçamento?","EXCLUIR",MessageBoxButtons.YesNo)){
						if (Service.Delete(id,conexao.GetConnection())){
							limpaService();
							statusSender("Orçamento ECLUÍDO com sucesso!");
						}else{
							carregaService(Service.Load(id,conexao.GetConnection()));
							statusSender("Não foi possível Excluir: ");
						}
					}
				}
				
				
			}catch{}
		}
		
		
    }
}
