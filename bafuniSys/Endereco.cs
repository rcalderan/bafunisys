﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BafuniSys
{
    class Endereco
    {
    	public long id {set;get;}
    	public long cliente {set;get;}
        
        public long numero { get; set; }
        public string complemento { get; set; }
        
        public string cep { set; get; }
        public string tipoLogradouro { set; get; }
        public string logradouro { set; get; }
        public string bairro { set; get; }
        public string cidade { set; get; }
        public string uf { set; get; }
        
        
        
        public static string New(long cliente,string tipo_log,string cep, string endereco, string numero, string bairro, string cidade, string uf, string complemento, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {


                    long id = 0;
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from endereco order by id_endereco desc limit 1", con);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                        id = (long)dt.Rows[0]["id_endereco"];
                    id++;

                    command.CommandText = "INSERT INTO endereco (id_endereco,cliente,cep,tipologr,endereco,numero,bairro,cidade,uf,complemento) VALUES (" +
                        "@id,@cliente,@cep,@tipologr,@endereco,@numero,@bairro,@cidade,@uf,@complemento)";
                    //var command = new System.Data.SQLite.SQLiteCommand(query, con);
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@cliente", cliente);
                    command.Parameters.AddWithValue("@tipologr", tipo_log);
                    command.Parameters.AddWithValue("@cep", cep);
                    command.Parameters.AddWithValue("@tipologr", tipo_log);
                    command.Parameters.AddWithValue("@endereco", endereco);
                    command.Parameters.AddWithValue("@numero", numero);
                    command.Parameters.AddWithValue("@bairro", bairro);
                    command.Parameters.AddWithValue("@cidade", cidade);
                    command.Parameters.AddWithValue("@uf", uf);
                    command.Parameters.AddWithValue("@complemento", complemento);
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return "";
            }
            catch(Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }
        
        public static List<Endereco> LoadCli(long cliente,System.Data.SQLite.SQLiteConnection con)
        {
        	List<Endereco> ret = new List<Endereco>();
            try
            {
                con.Open();
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from endereco where cliente='" + cliente.ToString() + "'", con);
                con.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                	Endereco end;
                	foreach(DataRow r in dt.Rows)
                	{
                		end = new Endereco();
                		end.id = (long)r["id_endereco"];
                		end.cliente = (long)r["cliente"];
                		end.cep = r["cep"].ToString();
                		end.tipoLogradouro = r["tipologr"].ToString();
                		end.logradouro = r["endereco"].ToString();
                		end.numero = (long)r["numero"];
                		end.bairro = r["bairro"].ToString();
                		end.cidade = r["cidade"].ToString();
                		end.uf = r["uf"].ToString();
                		end.complemento = r["complemento"].ToString();
                		ret.Add(end);
                	}
                    
                }
            }
            catch
            {

                if (SqLite.cepConnection.State != ConnectionState.Closed)
                    SqLite.cepConnection.Close();
            }
            return ret;
        }
        
        public static string Update(long id, string cep, string tipologr,string endereco, string numero, string bairro, string cidade, string uf, string complemento, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {/*CREATE TABLE IF NOT EXISTS endereco ('id_endereco' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                	'cliente' INTEGER, 'cep' INTEGER, 
'endereco' TEXT, 'numero' INTEGER, 'bairro' TEXT, 'cidade' TEXT, 'uf' TEXT, 'complemento' TEXT,'tipologr' TEXT, "+
*/
                    command.CommandText = "UPDATE endereco set tipologr=@tipologr,cep=@cep,endereco=@endereco,numero=@numero,bairro=@bairro,cidade=@cidade,uf=@uf,complemento=@complemento WHERE id_endereco=@id";
                    //var command = new System.Data.SQLite.SQLiteCommand(query, con);
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@tipologr", tipologr);
                    command.Parameters.AddWithValue("@cep", cep);
                    command.Parameters.AddWithValue("@endereco", endereco);
                    command.Parameters.AddWithValue("@numero", numero);
                    command.Parameters.AddWithValue("@bairro", bairro);
                    command.Parameters.AddWithValue("@cidade", cidade);
                    command.Parameters.AddWithValue("@uf", uf);
                    command.Parameters.AddWithValue("@complemento", complemento);
                    con.Open();
                    int x = command.ExecuteNonQuery();
                    if (x > 0){
                    	con.Close();
                		return "";
                    }
                    else
                    {
                    	con.Close();
                    	return "nada";
                    }
                }
            }
            catch (Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }
        
        
        public static string Delete(long id, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "DELETE FROM endereco WHERE id_endereco=@id";
                    //var command = new System.Data.SQLite.SQLiteCommand(query, con);
                    command.Parameters.AddWithValue("@id", id);
                    con.Open();
                    int x = command.ExecuteNonQuery();
                    if (x > 0){

                    }
                    else
                    {

                    }
                    con.Close();
                }
                return "";
            }
            catch (Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }
        
        public static Endereco LoadId(long id,System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                	Endereco ret = new Endereco();
                	con.Open();
                	System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from endereco where id_endereco="+id.ToString(), con);
                	con.Close();
                	DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                    {
                    	ret.id = (long)dt.Rows[0]["id_endereco"];
                    	ret.cliente = (long)dt.Rows[0]["cliente"];
                    	ret.cep = dt.Rows[0]["cep"].ToString();
                    	ret.tipoLogradouro = dt.Rows[0]["tipologr"].ToString();
                    	ret.logradouro = dt.Rows[0]["endereco"].ToString();
                    	ret.numero = (long)dt.Rows[0]["numero"];
                    	ret.bairro = dt.Rows[0]["bairro"].ToString();
                    	ret.cidade = dt.Rows[0]["cidade"].ToString();
                    	ret.uf = dt.Rows[0]["uf"].ToString();
                    	ret.complemento = dt.Rows[0]["complemento"].ToString();
                    	return ret;
                    }
                    else
                    	return null;
                }
            }
            catch
            {

                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }
        
        
        public static Endereco Last(System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                	Endereco ret = new Endereco();
                	con.Open();
                	System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from endereco ORDER BY id_endereco DESC LIMIT 1", con);
                	con.Close();
                	DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                    {
                    	ret.id = (long)dt.Rows[0]["id_endereco"];
                    	ret.cliente = (long)dt.Rows[0]["cliente"];
                    	ret.cep = dt.Rows[0]["cep"].ToString();
                    	ret.tipoLogradouro = dt.Rows[0]["tipologr"].ToString();
                    	ret.logradouro = dt.Rows[0]["endereco"].ToString();
                    	ret.numero = (long)dt.Rows[0]["numero"];
                    	ret.bairro = dt.Rows[0]["bairro"].ToString();
                    	ret.cidade = dt.Rows[0]["cidade"].ToString();
                    	ret.uf = dt.Rows[0]["uf"].ToString();
                    	ret.complemento = dt.Rows[0]["complemento"].ToString();
                    	return ret;
                    }
                    else
                    	return null;
                }
            }
            catch
            {

                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }
        
        public static Endereco LoadCep(string cep)
        {
            try
            {
                SqLite.cepConnection.Open();
                System.Data.SQLite.SQLiteCommand comand = new System.Data.SQLite.SQLiteCommand(SqLite.cepConnection);
                comand.CommandText = "select * from cep where cep=@cep";
                comand.Parameters.AddWithValue("@cep", cep);
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(comand);
                SqLite.cepConnection.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Endereco endereco = new Endereco();
                    endereco.cep = cep;
                    endereco.tipoLogradouro = dt.Rows[0]["log_tipo_logradouro"].ToString();
                    endereco.logradouro = dt.Rows[0]["logradouro"].ToString();
                    endereco.bairro= dt.Rows[0]["bairro"].ToString();
                    endereco.cidade = dt.Rows[0]["cidade"].ToString();
                    endereco.uf = dt.Rows[0]["uf"].ToString();

                    return endereco;
                }
                else
                {
                    return null;
                }
            }
            catch
            {

                if (SqLite.cepConnection.State != ConnectionState.Closed)
                    SqLite.cepConnection.Close();
                return null;
            }
        }

        public static Endereco FindCep(string logradouro, string cidade)
        {
            try
            {
            	string city=cidade;
            	if (cidade=="")
            		city ="SÃO CARLOS";
                SqLite.cepConnection.Open();
                System.Data.SQLite.SQLiteCommand comand = new System.Data.SQLite.SQLiteCommand(SqLite.cepConnection);
                comand.CommandText = "select * from cep where cidade=@cidade and logradouro=@log";
                
                comand.Parameters.AddWithValue("@log", logradouro);
                comand.Parameters.AddWithValue("@cidade", city);
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(comand);
                SqLite.cepConnection.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Endereco endereco = new Endereco();
                    endereco.cep = dt.Rows[0]["cep"].ToString();
                    endereco.tipoLogradouro = dt.Rows[0]["log_tipo_logradouro"].ToString();
                    endereco.logradouro = dt.Rows[0]["logradouro"].ToString();
                    endereco.bairro = dt.Rows[0]["bairro"].ToString();
                    endereco.cidade = dt.Rows[0]["cidade"].ToString();
                    endereco.uf = dt.Rows[0]["uf"].ToString();

                    return endereco;
                }
                else
                {
                    return null;
                }
            }
            catch
            {

                if (SqLite.cepConnection.State != ConnectionState.Closed)
                    SqLite.cepConnection.Close();
                return null;
            }
        }
        
        public static string[] allTipoLogradFromCity(string city)
        {
            List<string> all = new List<string>();
            try
            {
                SqLite.cepConnection.Open();
                System.Data.SQLite.SQLiteCommand comand = new System.Data.SQLite.SQLiteCommand(SqLite.cepConnection);
                comand.CommandText = "select log_tipo_logradouro from cep where cidade=@city group by log_tipo_logradouro";
                comand.Parameters.AddWithValue("@city", city);
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(comand);
                SqLite.cepConnection.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                all.Add("RUA");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                    	if (r["log_tipo_logradouro"].ToString()!="RUA")
                    		all.Add(r["log_tipo_logradouro"].ToString().ToUpper());
                    }
                }
            }
            catch
            {

                if (SqLite.cepConnection.State != ConnectionState.Closed)
                    SqLite.cepConnection.Close();
            }
            return all.ToArray();
        }


        public static string[] allLogradFromCity(string city)
        {
            List<string> all = new List<string>();
            try
            {
                SqLite.cepConnection.Open();
                System.Data.SQLite.SQLiteCommand comand = new System.Data.SQLite.SQLiteCommand(SqLite.cepConnection);
                comand.CommandText = "select * from cep where cidade=@city";
                comand.Parameters.AddWithValue("@city", city);
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(comand);
                SqLite.cepConnection.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        
                        all.Add(r["logradouro"].ToString());
                    }
                }
            }
            catch
            {

                if (SqLite.cepConnection.State != ConnectionState.Closed)
                    SqLite.cepConnection.Close();
            }
            return all.ToArray();
        }

    }
}
