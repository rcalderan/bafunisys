﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BafuniSys
{

    class ServiceDescricao
    {

        public long id { set; get; }
        public long service { set; get; }

        public string descricao { set; get; }
        public double valor { set; get; }

        public static string New(long servico, string descricao, double valor, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    long id = 0;
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from serv_descricao order by id_descricao desc limit 1", con);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                        id = (long)dt.Rows[0]["id_descricao"];
                    id++;

                    command.CommandText = "INSERT INTO serv_descricao (id_descricao,servico,descricao,valor) VALUES (" +
                        "@id,@servico,@descricao,@valor)";
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@servico", servico);
                    command.Parameters.AddWithValue("@descricao", descricao);
                    command.Parameters.AddWithValue("@valor", valor.ToString("F2"));
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return "";
            }
            catch (Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }
        public static bool Remove(long id, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "DELETE FROM serv_descricao WHERE id_descricao=@id";
                    command.Parameters.AddWithValue("@id", id);
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return true;
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
        public static string Remove(string descricao, long service, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "DELETE FROM serv_descricao WHERE descricao=@descicao and servico=@service";
                    command.Parameters.AddWithValue("@service", service);
                    command.Parameters.AddWithValue("@descicao", descricao);
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return "";
            }
            catch (Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }

        
        public static bool Update(long id, string descricao, string valor, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "UPDATE serv_descricao SET descricao=@descricao,valor=@valor WHERE id_descricao=" + id.ToString();
                    command.Parameters.AddWithValue("@descricao", descricao);
                    command.Parameters.AddWithValue("@valor", valor);
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return true;
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }

        private static ServiceDescricao loadRow(DataRow row)
        {
            ServiceDescricao service = new ServiceDescricao();
            try
            {
                service = new ServiceDescricao();
                service.id = (long)row["id_descricao"];
                service.service = (long)row["servico"];
                service.descricao = row["descricao"].ToString();
                service.valor = (double)row["valor"];

                return service;
            }
            catch { return null; }
        }

        public static List<ServiceDescricao> Load(long service, System.Data.SQLite.SQLiteConnection con)
        {
            List<ServiceDescricao> all = new List<ServiceDescricao>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from serv_descricao where servico=" + service, con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    ServiceDescricao servDescr;
                    foreach (DataRow r in dt.Rows)
                    {
                        servDescr = ServiceDescricao.loadRow(r);
                        all.Add(servDescr);
                    }
                }
                return all;

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }
        
        public static ServiceDescricao LoadId(long id, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from serv_descricao where id_descricao=" + id, con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach (DataRow r in dt.Rows)
                    {
                        return ServiceDescricao.loadRow(r);
                    }
                }
                return null;

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            return null;
            }
        }

        public static List<ServiceDescricao> GetAll(System.Data.SQLite.SQLiteConnection con)
        {
            List<ServiceDescricao> all = new List<ServiceDescricao>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from serv_descricao", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    ServiceDescricao servDescr;
                    foreach (DataRow r in dt.Rows)
                    {
                        servDescr = ServiceDescricao.loadRow(r);
                        all.Add(servDescr);
                    }
                }
                return all;

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }
        public static ServiceDescricao GetLast(System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from serv_descricao order by id_descricao DESC LIMIT 1", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    
                    return ServiceDescricao.loadRow(dt.Rows[0]);
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }
        

        public static List<ServiceDescricao> GetAllFrom(long service, System.Data.SQLite.SQLiteConnection con)
        {
            List<ServiceDescricao> all = new List<ServiceDescricao>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from serv_descricao WHERE servico=" + service.ToString(), con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                        all.Add(ServiceDescricao.loadRow(row));
                }

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }
        
        public static bool DeleteAll(long service, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "DELETE FROM serv_descricao WHERE servico=@val";
                    command.Parameters.AddWithValue("@val", service);
                    con.Open();
                    int x =command.ExecuteNonQuery();
                    if (x==0)
                    {
                    	con.Close();
                    	return false;
                    }else
                    	{
                    	con.Close();
                    	return true;
                    }
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }

    }
}
