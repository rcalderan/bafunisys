﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BafuniSys
{
    class Cliente
    {
        /*
   Edit    Delete	2	nome TEXT    No None    No
Edit    Delete	3	cep INTEGER No None    No
Edit    Delete	4	endereco TEXT    No None    No
Edit    Delete	5	numero INTEGER No None    No
Edit    Delete	6	bairro TEXT    No None    No
Edit    Delete	7	cidade TEXT    No None    No
Edit    Delete	8	uf TEXT    No None    No
Edit    Delete	9	fone1 TEXT    No None    No
Edit    Delete	10	fone2 TEXT    No None    No
Edit    Delete	11	complemeto TEXT    No None    No
Edit    Delete	12	obs*/

        public long id { get; set; }
        public long tipo {get;set;}
        public string cpf { get; set; }
        public string nome { get; set; }
        public List<Endereco> endereco { get; set; }
        public string fone1 { get; set; }
        public string fone2 { get; set; }
        public string obs { get; set; }

        public static Cliente Load(long id, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                con.Open();
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente where id_cliente='" + id.ToString() + "'", con);
                con.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Cliente cli = new Cliente();
                    cli.id = (long)dt.Rows[0]["id_cliente"];
                    cli.tipo = (long)dt.Rows[0]["tipo"];
                    cli.nome = dt.Rows[0]["nome"].ToString();
                    cli.cpf = dt.Rows[0]["cpf"].ToString();
                    cli.endereco = Endereco.LoadCli(id,con);
                    cli.fone1 = dt.Rows[0]["fone1"].ToString();
                    cli.fone2 = dt.Rows[0]["fone2"].ToString();
                    cli.obs = dt.Rows[0]["obs"].ToString();
                    
                    return cli;
                }
                else
                {
                    return null;
                }
            }
            catch
            {

                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }

        public static List<Cliente> GetByName(string nome, System.Data.SQLite.SQLiteConnection con)
        {
            List<Cliente> ret = new List<Cliente>();
            try
            {
                con.Open();
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente where nome like '%" + nome + "%'", con);
                con.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                
                Cliente cli = new Cliente();
                foreach (DataRow r in dt.Rows)
                    ret.Add(Cliente.loadRow(r,con));
            }
            catch
            {

                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return ret;
        }
        
        public static List<Cliente> GetByNameSensitive(string nome, System.Data.SQLite.SQLiteConnection con)
        {
            List<Cliente> ret = new List<Cliente>();
            try
            {
                con.Open();
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente where nome =" + nome + "'", con);
                con.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                
                Cliente cli = new Cliente();
                foreach (DataRow r in dt.Rows)
                    ret.Add(Cliente.loadRow(r,con));
            }
            catch
            {

                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return ret;
        }

        public static Cliente Load(string cpf, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente where cpf='" + cpf + "'", con);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                    {
                        Cliente cli = new Cliente();
                        cli.id = (long)dt.Rows[0]["id_cliente"];
                        cli.tipo = (long)dt.Rows[0]["tipo"];
                        cli.nome = dt.Rows[0]["nome"].ToString();
                        cli.cpf = dt.Rows[0]["cpf"].ToString();
                        cli.endereco = Endereco.LoadCli(cli.id,con);
                        cli.fone1 = dt.Rows[0]["fone1"].ToString();
                        cli.fone2 = dt.Rows[0]["fone2"].ToString();
                        cli.obs = dt.Rows[0]["obs"].ToString();
                        con.Close();
                        return cli;
                    }
                    else
                    {
                        con.Close();
                        return null;
                    }
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }
        
        public static Cliente LoadFromTel(string phone, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente where fone1='" + phone + "' or fone2='"+ phone +"'", con);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                    {
                        Cliente cli = new Cliente();
                        cli.id = (long)dt.Rows[0]["id_cliente"];
                        cli.tipo = (long)dt.Rows[0]["tipo"];
                        cli.nome = dt.Rows[0]["nome"].ToString();
                        cli.cpf = dt.Rows[0]["cpf"].ToString();
                        cli.endereco = Endereco.LoadCli(cli.id,con);
                        cli.fone1 = dt.Rows[0]["fone1"].ToString();
                        cli.fone2 = dt.Rows[0]["fone2"].ToString();
                        cli.obs = dt.Rows[0]["obs"].ToString();
                        con.Close();
                        return cli;
                    }
                    else
                    {
                        con.Close();
                        return null;
                    }
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }


        public static long NextId(System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                List<Cliente> all = Cliente.GetAll(con);
                long i = 1;
                while(all.Exists(element => element.id == i))
                    i++;
                return i;
            }
            catch
            {
                return 1;
            }
        }
        public static Cliente GetLast( System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente order by id_cliente desc limit 1", con);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                    {
                        Cliente cli = new Cliente();
                        cli.id = (long)dt.Rows[0]["id_cliente"];
                        cli.tipo = (long)dt.Rows[0]["tipo"];
                        cli.nome = dt.Rows[0]["nome"].ToString();
                        cli.cpf = dt.Rows[0]["cpf"].ToString();
                        cli.endereco = Endereco.LoadCli(cli.id,con);
                        cli.fone1 = dt.Rows[0]["fone1"].ToString();
                        cli.fone2 = dt.Rows[0]["fone2"].ToString();
                        cli.obs = dt.Rows[0]["obs"].ToString();
                        con.Close();
                        return cli;
                    }
                    else
                    {
                        con.Close();
                        return null;
                    }
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }

        }

        private static Cliente loadRow(DataRow row,System.Data.SQLite.SQLiteConnection con)
        {
            Cliente cli = new Cliente();
            try
            {
                cli = new Cliente();
                cli.id = (long)row["id_cliente"];
                cli.tipo = (long)row["tipo"];
                cli.nome = row["nome"].ToString();
                cli.cpf = row["cpf"].ToString();
                cli.endereco = Endereco.LoadCli(cli.id,con);
                cli.fone1 = row["fone1"].ToString();
                cli.fone2 = row["fone2"].ToString();
                cli.obs = row["obs"].ToString();

                return cli;
            }
            catch { return null; }
        }

        public static List<Cliente> GetAll(System.Data.SQLite.SQLiteConnection con)
        {
            List<Cliente> all = new List<Cliente>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach(DataRow row in dt.Rows)
                        all.Add(Cliente.loadRow(row,con));
                }

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }

        public static List<Cliente> Find(string name, System.Data.SQLite.SQLiteConnection con)
        {
            List<Cliente> all = new List<Cliente>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    comm.CommandText = "select * from cliente WHERE nome like @name";
                    comm.Parameters.AddWithValue("@name", "%"+name+"%");
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(comm);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                        all.Add(Cliente.loadRow(row,con));
                }

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }


        public static List<string> GetAllNames(System.Data.SQLite.SQLiteConnection con)
        {
            List<string> all = new List<string>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                        all.Add(row["nome"].ToString());
                }

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }


        public static string New(long tipo,string cpf, string nome, string fone1, string fone2, string obs, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {


                    long id = 0;
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente order by id_cliente desc limit 1", con);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                        id = (long)dt.Rows[0]["id_cliente"];
                    id++;

                    command.CommandText = "INSERT INTO cliente (id_cliente,tipo,cpf,nome,fone1,fone2,obs) VALUES (" +
                        "@id,@tipo,@cpf,@nome,@fone1,@fone2,@obs)";
                    //var command = new System.Data.SQLite.SQLiteCommand(query, con);
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@tipo", tipo);
                    command.Parameters.AddWithValue("@cpf", cpf);
                    command.Parameters.AddWithValue("@nome", nome);
                    command.Parameters.AddWithValue("@fone1", fone1);
                    command.Parameters.AddWithValue("@fone2", fone2);
                    command.Parameters.AddWithValue("@obs", obs);
                    con.Open();
                    if (0 < command.ExecuteNonQuery())
                    {
                    	con.Close();
                    	return "";
                    }
                    else{
                    	con.Close();
                    	return "Não foi possível criar novo cadastro.";
                    }
                }
            }
            catch(Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }
        public static string Update(long id,long tipo,string cpf, string nome,string email, string fone1, string fone2, string obs, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {

                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from cliente order by id_cliente desc limit 1", con);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    command.CommandText = "UPDATE cliente set cpf=@cpf,tipo=@tipo,email=@email,nome=@nome,fone1=@fone1,fone2=@fone2,obs=@obs WHERE id_cliente=@id";
                    //var command = new System.Data.SQLite.SQLiteCommand(query, con);
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@tipo", tipo);
                    command.Parameters.AddWithValue("@cpf", cpf);
                    command.Parameters.AddWithValue("@nome", nome);
                    command.Parameters.AddWithValue("@email", email);
                    command.Parameters.AddWithValue("@fone1", fone1);
                    command.Parameters.AddWithValue("@fone2", fone2);
                    command.Parameters.AddWithValue("@obs", obs);
                    con.Open();
                    
                    int x = command.ExecuteNonQuery();
                    if (x > 0){
                    con.Close();
               		return "";
                    }
                    else
                    {
                    con.Close();
                		return "Numero de rows affetatos="+x;
                    }
                }
            }
            catch (Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }
        public static bool Update(long id, string coluna, object valor, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "UPDATE cliente SET " + coluna + "=@valor WHERE id_cliente=" + id.ToString();
                    command.Parameters.AddWithValue("@valor", valor);
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return true;
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
        
        
        public static bool Delete(long id, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "DELETE FROM cliente WHERE id_cliente=@val";
                    command.Parameters.AddWithValue("@val", id);
                    con.Open();
                    int x =command.ExecuteNonQuery();
                    if (x==0)
                    {
                    	con.Close();
                    	return false;
                    }else
                    	{
                    	con.Close();
                    	return true;
                    }
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
        
    }
}
