﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BafuniSys
{
    class Financa
    {

        public long id { set; get; }
        public long parcela { set; get; }
        public long cliente { set; get; }
        public long service { set; get; }
        public DateTime hoje { set; get; }
        public DateTime data { set; get; }
        public TipoDeFormas forma { set; get; }
        public double valor { set; get; }
        public bool pg { set; get; }
        
        public enum TipoDeFormas
        {
        	Pendente,
        	Dinheiro,
        	Cheque,
        	Cartao,
        	Deposito,
        	Outros
        };
        
        public static TipoDeFormas setTipo(long idForma){
        	
        	switch(idForma){
        		case 0:
        			return TipoDeFormas.Pendente;
        		case 1:
        			return TipoDeFormas.Dinheiro;
        		case 2:
        			return TipoDeFormas.Cheque;
        		case 3:
        			return TipoDeFormas.Cartao;
        		case 4:
        			return TipoDeFormas.Deposito;
        		case 5:
        			return TipoDeFormas.Outros;
        		default:
        			return TipoDeFormas.Pendente;
        	}
        }
        public static long TipoFormasStringToId(string tipoForma){
        	
        	switch(tipoForma){
        		case "Pendente":
        			return 0;
        		case "Dinheiro":
        			return 1;
        		case "Cheque":
        			return 2;
        		case "Cartao":
        			return 3;
        		case "Deposito":
        			return 4;
        		case "Outros":
        			return 5;
        		default:
        			return 0;
        	}
        }

        public struct Irow_parcer
        {
        	public long parcela;
        	public DateTime data;
        	public TipoDeFormas forma;
        	public double valor;
        	public Irow_parcer(long par,DateTime dt,TipoDeFormas forma_pag,double val)
        	{
        		parcela = par;
        		data = dt;
        		valor = val;
        		forma = forma_pag;
        	}
        }

        public static Financa Load(long id, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                con.Open();
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from financa where id_financa='" + id.ToString() + "'", con);
                con.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Financa fin = new Financa();
                    fin.id = (long)dt.Rows[0]["id_financa"];
                    fin.parcela = (long)dt.Rows[0]["parcela"];
                    fin.cliente = (long)dt.Rows[0]["cliente"];
                    fin.service = (long)dt.Rows[0]["servico"];
                    fin.hoje = (DateTime)dt.Rows[0]["hoje"];
                    fin.data = (DateTime)dt.Rows[0]["data"];
                    fin.pg = (bool)dt.Rows[0]["pg"];
                    fin.forma = setTipo((long)dt.Rows[0]["forma"]);
                    fin.valor = (double)dt.Rows[0]["status"];
                    return fin;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }

        private static Financa loadRow(DataRow row, System.Data.SQLite.SQLiteConnection con)
        {
            Financa fin = new Financa();
            try
            {
                fin = new Financa();
                fin.id = (long)row["id_financa"];
                fin.parcela = (long)row["parcela"];
                fin.cliente = (long)row["cliente"];
                fin.service = (long)row["servico"];
                fin.hoje = (DateTime)row["hoje"];
                fin.data = (DateTime)row["data"];
                fin.pg = (bool)row["pg"];
                fin.forma = setTipo((long)row["forma"]);
                fin.valor = (double)row["valor"];

                return fin;
            }
            catch { return null; }
        }

        public static List<Financa> GetAll(System.Data.SQLite.SQLiteConnection con)
        {
            List<Financa> all = new List<Financa>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from financa", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                        all.Add(Financa.loadRow(row, con));
                }

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }
        public static List<Financa> GetAllFrom(long service, System.Data.SQLite.SQLiteConnection con)
        {
            List<Financa> all = new List<Financa>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from financa WHERE servico=" + service.ToString() + " ORDER BY parcela", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                        all.Add(Financa.loadRow(row, con));
                }

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }

        public static string NewOrUpdate( long parcela,long cliente,long servico, double valor,TipoDeFormas forma, DateTime hoje, DateTime data, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (System.Data.SQLite.SQLiteCommand command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    long id = 0;
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from financa order by id_financa desc limit 1", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                        id = (long)dt.Rows[0]["id_financa"];
                    id++;
                    command.CommandText = "select * from financa where parcela=@parcela and servico=@servico";
                    command.Parameters.AddWithValue("@parcela", parcela);
                    command.Parameters.AddWithValue("@servico", servico);

                    con.Open();
                    adp = new System.Data.SQLite.SQLiteDataAdapter(command);
                    con.Close();
                    dt.Clear();
                    dt=new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                    {//update
                        if (valor==0)
                        {
                            forma = 0;
                        }
                        command.CommandText = "UPDATE financa set valor=@valor,forma=@forma,data=@data,pg=0 WHERE id_financa=@id";
                        command.Parameters.AddWithValue("@id", (long)dt.Rows[0]["id_financa"]);
                        command.Parameters.AddWithValue("@valor", valor);
                        command.Parameters.AddWithValue("@forma", (long)forma);
                        command.Parameters.AddWithValue("@data", data);
                        con.Open();

                        if (command.ExecuteNonQuery()==0)
                        {
                        	con.Close();
                        	return "Não foi possível atualizar parcela "+parcela.ToString();
                        }
                        else
                        	con.Close();
                    }
                    else
                    {//new
                        if (valor == 0)
                        {
                            forma = 0;
                            if (con.State != ConnectionState.Closed)
                                con.Close();
                            return "";
                        }
                        command.CommandText = "INSERT INTO financa (id_financa,parcela,cliente,servico,valor,forma,hoje,data,pg) VALUES (" +
                            "@id,@parcela,@cliente,@servico,@valor,@forma,@hoje,@data,@pg)";
                        command.Parameters.AddWithValue("@id", id);
                        command.Parameters.AddWithValue("@parcela", parcela);
                        command.Parameters.AddWithValue("@cliente", cliente);
                        command.Parameters.AddWithValue("@servico", servico);
                        command.Parameters.AddWithValue("@valor", valor);
                        command.Parameters.AddWithValue("@forma", (long)forma);
                        command.Parameters.AddWithValue("@hoje", hoje);
                        command.Parameters.AddWithValue("@data", data);
                        command.Parameters.AddWithValue("@pg", false);
                        con.Open();

                        
                        if (command.ExecuteNonQuery()==0)
                        {
                        	con.Close();
                        	return "Não foi criar parcela "+parcela.ToString();
                        }
                        else
                        	con.Close();
                    }
                }
                return "";
            }
            catch (Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }
        
        public static DataTable Pendencias(bool mostrarTodos, System.Data.SQLite.SQLiteConnection con)
        {
        	DataTable dt = new DataTable();
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    if (mostrarTodos)
                    	command.CommandText ="Select s.id_servico as ID, s.codigo as Codigo,c.nome as Cliente, f.valor as Valor,f.parcela as Parcela from financa as f inner join servico as s on s.id_servico=f.servico "+
                    	"inner join cliente as c on s.ccli=c.id_cliente "+
                    	"where f.forma=0 and s.status >= 0 and f.valor!=0";
                    else
                    	command.CommandText = "Select s.id_servico as ID, s.codigo as Codigo,c.nome as Cliente, f.valor as Valor,f.parcela as Parcela from financa as f inner join servico as s on s.id_servico=f.servico "+
                    	"inner join cliente as c on s.ccli=c.id_cliente "+
                    	"where f.forma=0 and s.status > 1 and f.valor!=0";
                    
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(command);
                    con.Close();
                    adp.Fill(dt);
                }
            }
            /*
            Houve uma incompatibilidade entre a arquitetura de processador do projeto em criação "MSIL" e a 
arquitetura de processador da referência "System.Data.SQLite", "x86". Essa incompatibilidade poderá causar falhas de tempo de execução. 
Considere a alteração da arquitetura de processador de destino de seu projeto por meio do Gerenciador de Configuração de forma a alinhar as 
arquiteturas de processador entre seu projeto e as referências ou obtenha uma dependência sobre referências com uma arquitetura de processador
 que corresponda à arquitetura de processador de destino de seu projeto. (MSB3270)
            */catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return dt;
        }

        public static DataTable GetFinanca(Cliente cliente, System.Data.SQLite.SQLiteConnection con)
        {
            DataTable dt = new DataTable();
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    command.CommandText = "select * from financa WHERE cliente=@cli";
                    command.Parameters.AddWithValue("@cli", cliente.id);
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(command);
                    con.Close();
                    adp.Fill(dt);
                    double total = 0,pago=0,aux;
                    if (dt.Rows.Count != 0)
                    {
                        foreach(DataRow r in dt.Rows)
                        {
                            if (double.TryParse(r["valor"].ToString(), out aux))
                            {
                                total += aux;
                                if ((long)r["forma"]!=0)
                                    pago += aux;
                            }
                        }
                    }
                    DataColumn[] cols = { new DataColumn("Descrição"), new DataColumn("valor") };
                    dt = new DataTable();
                    dt.Columns.AddRange(cols);
                    object[] row ={"Total", total.ToString("F2") };
                    dt.Rows.Add(row);
                    row = new object[2];
                    row[0] = "Pagou";
                    row[1] = pago.ToString("F2");
                    dt.Rows.Add(row);
                    row = new object[2];
                    row[0] = "Falta";
                    row[1] = (total - pago).ToString("F2");
                    dt.Rows.Add(row);
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return dt;
        }

        public static bool Update(long id, string coluna, object valor, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "UPDATE financa SET " + coluna + "=@valor WHERE id_financa=" + id.ToString();
                    command.Parameters.AddWithValue("@valor", valor);
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return true;
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }public static bool Update(long servico, long parcela, float valor, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "UPDATE financa SET valor=@valor WHERE servico=@serv and parcela=@par";
                    command.Parameters.AddWithValue("@valor", valor);
                    command.Parameters.AddWithValue("@serv", servico);
                    command.Parameters.AddWithValue("@par", parcela);
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return true;
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
        
        public static bool DeleteAll(long service, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "DELETE FROM financa WHERE servico=@val";
                    command.Parameters.AddWithValue("@val", service);
                    con.Open();
                    int x =command.ExecuteNonQuery();
                    if (x==0)
                    {
                    	con.Close();
                    	return false;
                    }else
                    	{
                    	con.Close();
                    	return true;
                    }
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
        
        public static bool Delete(int parcela, long service, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                	DataTable dt = new DataTable();
                    con.Open();
                	command.CommandText = "select * from financa WHERE servico=@service";
                    command.Parameters.AddWithValue("@service", service);
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(command);
                    adp.Fill(dt);
                    if (dt.Rows.Count-1>=parcela)
                    {
                    	for(int i=0;i< dt.Rows.Count;i++)
                    	{
                    		if(i==parcela)
                    		{
                    			command.CommandText = "DELETE FROM financa WHERE servico=@val and parcela=@par";
                    			command.Parameters.AddWithValue("@val", service);
                    			command.Parameters.AddWithValue("@par", dt.Rows[i]["parcela"]);
                    			int x =command.ExecuteNonQuery();
                    			if (x==0)
                    			{
                    				con.Close();
                    				return false;
                    			}else
                    			{
                    				con.Close();
                    				return true;
                    			}
                    		}
                    	}
                    	con.Close();
                    	return false;
                    	
                    }else{
                    	con.Close();
                    	return false;
                    }
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
    }
}
