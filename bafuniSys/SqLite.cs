﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;


namespace BafuniSys
{
    class SqLite
    {
        private string connectionString;
        private string database;
        
        private Dictionary<string,string> tables = new Dictionary<string, string>(){
        	{"cliente","CREATE TABLE IF NOT EXISTS cliente ('id_cliente' INTEGER NOT NULL, 'cpf' TEXT NOT NULL, 'nome' TEXT, 'fone1' TEXT, 'fone2' TEXT, 'obs' TEXT, 'tipo' INTEGER NOT NULL DEFAULT 0, 'email' TEXT DEFAULT ''"+
        			"PRIMARY KEY ('id_cliente', 'cpf'))"},
        	{"endereco","CREATE TABLE IF NOT EXISTS endereco ('id_endereco' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'cliente' INTEGER, 'cep' INTEGER, 'endereco' TEXT, 'numero' INTEGER, 'bairro' TEXT, 'cidade' TEXT, 'uf' TEXT, 'complemento' TEXT,'tipologr' TEXT, "+
        			"FOREIGN KEY(cliente) REFERENCES cli(id_cliente))"},
        	{"servico","CREATE TABLE IF NOT EXISTS servico ('id_servico' INTEGER PRIMARY KEY NOT NULL,'codigo' INTEGER NOT NULL,'ccli' INTEGER NOT NULL, 'hoje' DATETIME, 'obs' TEXT, 'status' INTEGER, 'data' DATETIME, 'endereco' INTEGER, "+
        			"FOREIGN KEY(ccli) REFERENCES cli(id_cliente), " +
        		"FOREIGN KEY(endereco) REFERENCES end(id_endereco) )"},
        	{"financa",
        		"CREATE TABLE IF NOT EXISTS financa ('id_financa' INTEGER PRIMARY KEY NOT NULL, 'parcela' INTEGER NOT NULL, 'cliente' INTEGER NOT NULL, 'servico' INTEGER NOT NULL, 'valor' REAL, 'pg' BOOLEAN, 'forma' INTEGER NOT NULL, 'hoje' DATETIME,'data' DATETIME, "+
        			"FOREIGN KEY(servico) REFERENCES servico(id_servico) )"},
        	{"serv_descricao","CREATE TABLE IF NOT EXISTS serv_descricao ('id_descricao' INTEGER PRIMARY KEY NOT NULL,'servico' INTEGER NOT NULL,'descricao' TEXT, 'valor' REAL, "+
        			"FOREIGN KEY(servico) REFERENCES servico(id_servico) )"},
        	{"conf","CREATE TABLE 'config' ('id' INTEGER PRIMARY KEY NOT NULL, 'bkp_on_close' BOOLEAN DEFAULT 'false', 'bkp_src' TEXT DEFAULT '', 'conf1' BOOLEAN DEFAULT 'false', 'conf2' BOOLEAN DEFAULT 'false', 'conf3' BOOLEAN DEFAULT 'false')"},
    	};


        public static SQLiteConnection cepConnection = new SQLiteConnection("Data Source=" + Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\cep;Version=3;");
        private SQLiteConnection connection;
        //public static MySql.Data.MySqlClient.MySqlConnection mysqlConect = new MySql.Data.MySqlClient.MySqlConnection("datasource=192.168.0.15;port=3306;username=root;password=33722363;database=cep");
        /*
         Compilar junto os arquivos sqlite à respectiva platafoma x86 ou X64, 
         caso tentar compilar com arquivos errados vai travar...
             
             */
        public SqLite()
        {
            database = "bafuni";
            connectionString = "Data Source="+ Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)+"\\"+database+";Version=3;";
            connection = new SQLiteConnection(connectionString);
        }
        public SQLiteConnection GetConnection()
        {
            return connection;
        }

        public string check_tables()
        {
        	string aux="Checking tables...\n";
            try
            {
            	
            	using (var command = new System.Data.SQLite.SQLiteCommand(connection))
                {
            		
            		
                    connection.Open();
                    
                    if(File.Exists(database))
                    {
                    	aux+="Database ok!\n";
                    }
                    else{
                    	command.CommandText = "CREATE DATABASE "+database;
                    	if (command.ExecuteNonQuery()>0)
                    		aux+="Database criada\n";
                    	else{
                    		aux+="fail! não foi possivel criar database...\n";
                    		return aux;
                    	}
                    }
                    /*SQLiteDataAdapter adp = new SQLiteDataAdapter("SHOW TABLES",connection);
                    System.Data.DataTable dt = new  System.Data.DataTable();
                    adp.Fill(dt);
                    dt.Columns[0].ColumnName = "tables";
                    System.Data.DataColumn[] keys = {dt.Columns["tables"]};
                    dt.PrimaryKey = keys;*/
                    
                    foreach (KeyValuePair<string, string> pair in tables)
                    {
                    	command.CommandText =pair.Value;
                    	command.ExecuteNonQuery();
                    	aux+=pair.Key+" ok!\n";
                    	/*
                    	if (null == dt.Rows.Find(pair.Key))
                    	{
                    	}
                    	else
                    		aux+=pair.Key+" já existe!\n";*/
                    }
                    connection.Close();
                }
                return aux;
            }
            catch(Exception e)
            {
            	if (connection.State!= System.Data.ConnectionState.Closed)
            		connection.Close();
                return aux+e.Message;
            }
        }

    }
}
