﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BafuniSys
{
    class Service
    {

        public long id { set; get; }
        public long codigo { set; get; }
        public Cliente cliente { set; get; }
        public DateTime hoje { set; get; }
        public DateTime data { set; get; }
        public string obs { set; get; }
        public long status { set; get; }
        public Endereco endereco {set; get;}
        
        public List<ServiceDescricao> listaDeDecricao { set; get; }

        public List<Financa> parcelas { set; get; }

        public struct descricao{
            public long id;
            public string desc_str;
            public double valor;
            
        	public descricao(long cod,String descri,double val)
        	{
        		id = cod;
        		desc_str = descri;
        		valor = val;
        	}
        }

        public static Service Load(long id, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                con.Open();
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter(
                	"select s.*,c.* from servico as s inner join cliente as c on c.id_cliente=s.ccli "+
                	"where id_servico='" + id.ToString() + "'", con);
                con.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Service service = new Service();
                    service.id = id;
                    service.codigo = (long)dt.Rows[0]["codigo"];
                    Cliente cli = new Cliente();
                    cli.id = (long)dt.Rows[0]["id_cliente"];
                    cli.tipo = (long)dt.Rows[0]["tipo"];
                    cli.nome = dt.Rows[0]["nome"].ToString();
                    cli.cpf = dt.Rows[0]["cpf"].ToString();
                    cli.endereco = Endereco.LoadCli(cli.id,con);
                    cli.fone1 = dt.Rows[0]["fone1"].ToString();
                    cli.fone2 = dt.Rows[0]["fone2"].ToString();
                    cli.obs = dt.Rows[0]["obs"].ToString();
                    service.cliente = cli;
                    service.endereco = Endereco.LoadId((long)dt.Rows[0]["endereco"],con);
                    service.hoje = (DateTime)dt.Rows[0]["hoje"];
                    service.data = (DateTime)dt.Rows[0]["data"];
                    service.obs = dt.Rows[0]["obs"].ToString();
                    service.status = (long)dt.Rows[0]["status"];
                    service.listaDeDecricao = ServiceDescricao.GetAllFrom(service.id, con);
                    service.parcelas = Financa.GetAllFrom(id, con);
                    return service;
                }
                else
                {
                    return null;
                }
            }
            catch 
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }
        public static Service LoadByBafuniCod(long codigo, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                con.Open();
                System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from servico where codigo='" + codigo.ToString() + "'", con);
                con.Close();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Service service = new Service();
                    service.id = (long)dt.Rows[0]["id_servico"];
                    service.codigo = codigo;
                    service.cliente = Cliente.Load((long)dt.Rows[0]["ccli"], con);
                    service.endereco = Endereco.LoadId((long)dt.Rows[0]["endereco"], con);
                    service.hoje = (DateTime)dt.Rows[0]["hoje"];
                    service.data = (DateTime)dt.Rows[0]["data"];
                    service.obs = dt.Rows[0]["obs"].ToString();
                    service.status = (long)dt.Rows[0]["status"];
                    service.listaDeDecricao = ServiceDescricao.GetAllFrom(service.id, con);
                    service.parcelas = Financa.GetAllFrom(service.id, con);
                    return service;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return null;
            }
        }

        private static Service loadRow(DataRow row, System.Data.SQLite.SQLiteConnection con)
        {
            Service service = new Service();
            try
            {
                service = new Service();
                service.id = (long)row["id_servico"];
                service.codigo = (long)row["codigo"];
                service.cliente = Cliente.Load((long)row["ccli"], con);
                service.hoje = (DateTime)row["hoje"];
                service.data = (DateTime)row["data"];
                service.obs = row["obs"].ToString();
                service.status = (long)row["status"];
                service.listaDeDecricao = ServiceDescricao.GetAllFrom(service.id, con);
                service.parcelas = Financa.GetAllFrom(service.id, con);
                service.endereco = Endereco.LoadId((long)row["endereco"], con);

                return service;
            }
            catch {
            	return null; }
        }

        public static List<Service> GetAll(System.Data.SQLite.SQLiteConnection con)
        {
            List<Service> all = new List<Service>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from servico", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                        all.Add(Service.loadRow(row, con));
                }

            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }
        public static List<Service> GetAllFrom(long cliente, System.Data.SQLite.SQLiteConnection con)
        {
            List<Service> all = new List<Service>();
            try
            {
                using (var comm = new System.Data.SQLite.SQLiteCommand(con))
                {
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from servico WHERE ccli=" + cliente.ToString()+" ORDER BY data DESC", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                        all.Add(Service.loadRow(row, con));
                }

            }
            catch 
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            return all;
        }
        
        public static string New(long codigo, long ccli, long endereco, DateTime hoje, DateTime data, long status, string obs, List<Service.descricao> descricao,
                                 
                                 List<Financa.Irow_parcer> parcelas,System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    long id = 0;
                    con.Open();
                    System.Data.SQLite.SQLiteDataAdapter adp = new System.Data.SQLite.SQLiteDataAdapter("select * from servico order by id_servico desc limit 1", con);
                    con.Close();
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    if (dt.Rows.Count != 0)
                        id = (long)dt.Rows[0]["id_servico"];
                    id++;

                    command.CommandText = "INSERT INTO servico (id_servico,endereco,codigo,ccli,hoje,data,status,obs) VALUES (" +
                        "@id,@endereco,@codigo,@ccli,@hoje,@data,@status,@obs)";
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@endereco", endereco );
                    command.Parameters.AddWithValue("@codigo", codigo);
                    command.Parameters.AddWithValue("@ccli", ccli);
                    command.Parameters.AddWithValue("@hoje", hoje);
                    command.Parameters.AddWithValue("@data", data);
                    command.Parameters.AddWithValue("@status", status);
                    command.Parameters.AddWithValue("@obs", obs);
                    con.Open();
                    
                    if(command.ExecuteNonQuery() > 0)
                    {
                        con.Close();
                        foreach (Service.descricao desc in descricao)
                        {
                            ServiceDescricao.New(id, desc.desc_str, desc.valor, con);
                        }
                        
                        //foreach(Financa.Irow_parcer par in parcelas)
                        	//Financa.NewOrUpdate(par.parcela,ccli, id, par.valor, par.forma, DateTime.Now, par.data, con);
                        //Financa.NewOrUpdate(1,ccli, id, 0, 0, DateTime.Now, DateTime.Now, con);

                        foreach (Financa.Irow_parcer p in parcelas)
                        {
                        	Financa.NewOrUpdate(p.parcela,ccli, id, p.valor, p.forma, DateTime.Now, p.data, con);
                        }
                    }
                    else
                        con.Close();
                }
                return "";
            }
            catch (Exception e)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return e.Message;
            }
        }

        public static bool Update(long id, string coluna, object valor, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "UPDATE servico SET " + coluna + "=@valor WHERE id_servico=" + id.ToString();
                    command.Parameters.AddWithValue("@valor", valor);
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();
                }
                return true;
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
        
        public static bool Delete(long id, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                    command.CommandText = "DELETE FROM servico WHERE id_servico=@val";
                    command.Parameters.AddWithValue("@val", id);
                    con.Open();
                    int x =command.ExecuteNonQuery();
                    if (x==0)
                    {
                    	con.Close();
                    	return false;
                    }else
                    	{
                    	ServiceDescricao.DeleteAll(id,con);
                    	Financa.DeleteAll(id,con);
                    	con.Close();
                    	return true;
                    }
                }
            }
            catch(Exception er)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
        
        public bool AlterarDescricao(int index,string descricao,float valor){
        	try{
        		return true;
        		
        	}catch{
        		return false;
        	}
        	
        }
        
        public static bool DeleteAll(long cliente, System.Data.SQLite.SQLiteConnection con)
        {
            try
            {
                using (var command = new System.Data.SQLite.SQLiteCommand(con))
                {
                	List<Service> all = Service.GetAllFrom(cliente,con);
                    command.CommandText = "DELETE FROM service WHERE cliente=@val";
                    command.Parameters.AddWithValue("@val", cliente);
                    con.Open();
                    int x =command.ExecuteNonQuery();
                    if (x==0)
                    {
                    	con.Close();
                    	return false;
                    }else
                    	{
                    	con.Close();
                    	foreach(Service s in all)
                    	{
                    		ServiceDescricao.DeleteAll(s.id,con);
                    		Financa.DeleteAll(s.id,con);
                    	}
                    	return true;
                    }
                }
            }
            catch
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();
                return false;
            }
        }
        
        
    }

}
